Databox::Application.routes.draw do
   get "facility/control/main" => "facilities#main"
   post "facility/control/EditFacility" => "facilities#edit"
   post "facility/control/UpdateFacility" => "facilities#update"
   get "facility/control/EditFacilityInventoryItems" => "facilities#edit_facility_inventory_item"
   #resources :facilities
   
  #devise_for :users, :controllers => { :sessions => "users/sessions" }
  match "users/sign_up" => "pages#error"

  devise_for :users,
  :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", :invitations => 'users/invitations', :sessions => "users/sessions"},
  :path => "users", :path_names => { :sign_in => 'login', :sign_out => 'logout', :password => 'secret', :confirmation => 'verification', :unlock => 'unblock'}
  devise_scope :user do
    match "party/" => "users/users#index"
    match "party/adduser" => "users/users#adduser"
    post "party/adduser"
    match "party/status" => "users/users#status"
    match "party/update_user" => "users/users#update_user"
    match "party/assign_role" => "users/users#assign_role"
    match "/users/check_email" => "users/users#check_email"
  end
  root :to => 'products#index'



  #get "admins/accounts"
  put "admins/update_user"
  put "admins/assign_role"


  get "pages/error"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controllers and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controllers actions automatically):
  #   resources :products

  resources :products do
    collection do
      post :update_image
      post :update_product
      post :update_factory
      get :overview
      get :photos
      get :copy
      get :color
      match "weight_size/:sku" => "products#weight_size", as: :weight_size
      match "pricing/:sku" => "products#pricing", as: :pricing
      get :moq_cost
      get :child
      get :find
      post  :results
    end
  end


  resources :product_promotions do
    collection do
      post :promotions_per_page
      post :delete_promotions
      get :stores
      post :stores_per_page
      post :delete_stores
      post :update_stores
      post :insert_stores


    end
  end

  resources :promotion_codes do
    collection do
      post :promotion_codes_per_page
      post :delete_promotion_codes
      get :mass_insert
      post :mass_insert
      post :add_email
      post :add_party
      post :delete_email
      post :delete_party
      post :check_party_id
    end
  end

  resources :ship_rates do
    collection do
      post :get_for_country
      post :get_rates
      post :get_for_city_autocomplete
      post :shipment_per_page
      post :delete_shipment
      post :update_shipment
      post :get_quote
      post :get_for_country_autocomplete
    end
  end

  match "all_customers" => "customers#index", as: :all_customers

  # resources :factories do
  #   collection do
  #     match "factory_detail/:party_id" => "factories#factory_detail", as: :factory_detail
  #   end
  # end
  resources :factories
  
  match "/factories/factory_detail/:party_id" => "factories#factory_detail", as: :factory_detail
  match "all_factories" => "factories#all_factories", as: :all_factories



  # Customized URLs for Orders controlelr.
  #match "orders" => "orders#index"
  #match "orders/order-entry" => "orders#new"
  #match "orders/find-return" => "orders#return"
  #match "orders/order-view" => "orders#view"
  resources :orders do
    collection do
      get :find_return
    end
  end

  resources :promotion_rules do
    collection do
      match 'manage/:id' =>  "promotion_rules#manage"
      post '/update_promotion_rule'
      post '/update_rule_condition'
      post 'delete_rule_condition'
      post 'add_rule_condition_category'
      post 'update_rule_condition_category'
      post 'add_rule_condition_product'
      post 'update_rule_condition_product'
      post :delete_rule_action_category
      post :add_rule_action_category
      post :delete_rule_action
      post :update_rule_action
      post :delete_rule_action_product
      post :add_rule_action_product
      post :insert_rule_action
      post :delete_rule_condition_category
      post :delete_rule_condition_product
      post :add_rule_condition
      post :add_promotion_rule
      post :add_promotion_category
      post :delete_promotion_category
      post :add_promotion_product
      post :delete_promotion_product
    end
  end

  resources :promotion_rule_conditions

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  #root :to => 'users/users#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controllers route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controllers accessible via GET requests.
  # match ':controllers(/:action(/:id))(.:format)'
end
