#require 'test_helper'
#
#class BillingAccountsControllerTest < ActionController::TestCase
#  setup do
#    @billing_account = billing_accounts(:one)
#    @controller = BillingAccountsController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:billing_accounts)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create billing_account" do
#    assert_difference('BillingAccount.count') do
#      post :create, billing_account: { account_limit: @billing_account.account_limit, account_limit_uom_id: @billing_account.account_limit_uom_id, bill_from_id: @billing_account.bill_from_id, bill_to_party_id: @billing_account.bill_to_party_id, billing_account_id: @billing_account.billing_account_id, description: @billing_account.description, external_account_id: @billing_account.external_account_id, from_date: @billing_account.from_date, postal_contact_mech_id: @billing_account.postal_contact_mech_id, thru_date: @billing_account.thru_date }
#    end
#
#    assert_redirected_to billing_account_path(assigns(:billing_account))
#  end
#
#  test "should show billing_account" do
#    get :show, id: @billing_account
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @billing_account
#    assert_response :success
#  end
#
#  test "should update billing_account" do
#    put :update, id: @billing_account, billing_account: { account_limit: @billing_account.account_limit, account_limit_uom_id: @billing_account.account_limit_uom_id, bill_from_id: @billing_account.bill_from_id, bill_to_party_id: @billing_account.bill_to_party_id, billing_account_id: @billing_account.billing_account_id, description: @billing_account.description, external_account_id: @billing_account.external_account_id, from_date: @billing_account.from_date, postal_contact_mech_id: @billing_account.postal_contact_mech_id, thru_date: @billing_account.thru_date }
#    assert_redirected_to billing_account_path(assigns(:billing_account))
#  end
#
#  test "should destroy billing_account" do
#    assert_difference('BillingAccount.count', -1) do
#      delete :destroy, id: @billing_account
#    end
#
#    assert_redirected_to billing_accounts_path
#  end
#end
