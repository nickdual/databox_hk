#require 'test_helper'
#
#class MarketInterestsControllerTest < ActionController::TestCase
#  setup do
#    @market_interest = market_interests(:one)
#    @controller = MarketInterestsController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:market_interests)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create market_interest" do
#    assert_difference('MarketInterest.count') do
#      post :create, market_interest: { from_date: @market_interest.from_date, market_segment_id: @market_interest.market_segment_id, product_category_id: @market_interest.product_category_id, thru_date: @market_interest.thru_date }
#    end
#
#    assert_redirected_to market_interest_path(assigns(:market_interest))
#  end
#
#  test "should show market_interest" do
#    get :show, id: @market_interest
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @market_interest
#    assert_response :success
#  end
#
#  test "should update market_interest" do
#    put :update, id: @market_interest, market_interest: { from_date: @market_interest.from_date, market_segment_id: @market_interest.market_segment_id, product_category_id: @market_interest.product_category_id, thru_date: @market_interest.thru_date }
#    assert_redirected_to market_interest_path(assigns(:market_interest))
#  end
#
#  test "should destroy market_interest" do
#    assert_difference('MarketInterest.count', -1) do
#      delete :destroy, id: @market_interest
#    end
#
#    assert_redirected_to market_interests_path
#  end
#end
