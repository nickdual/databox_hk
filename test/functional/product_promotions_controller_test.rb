#require 'test_helper'
#
#class ProductPromotionsControllerTest < ActionController::TestCase
#  setup do
#    @product_promotion = product_promotions(:one)
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:product_promotions)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create product_promotion" do
#    assert_difference('ProductPromotion.count') do
#      post :create, product_promotion: { billback_factor: @product_promotion.billback_factor, override_org_party_id: @product_promotion.override_org_party_id, promotion_id: @product_promotion.promotion_id, promotion_name: @product_promotion.promotion_name, promotion_show_to_customer: @product_promotion.promotion_show_to_customer, promotion_text: @product_promotion.promotion_text, require_code: @product_promotion.require_code, use_limit_per_customer: @product_promotion.use_limit_per_customer, use_limit_per_order: @product_promotion.use_limit_per_order, use_limit_per_promotion: @product_promotion.use_limit_per_promotion, user_entered: @product_promotion.user_entered }
#    end
#
#    assert_redirected_to product_promotion_path(assigns(:product_promotion))
#  end
#
#  test "should show product_promotion" do
#    get :show, id: @product_promotion
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @product_promotion
#    assert_response :success
#  end
#
#  test "should update product_promotion" do
#    put :update, id: @product_promotion, product_promotion: { billback_factor: @product_promotion.billback_factor, override_org_party_id: @product_promotion.override_org_party_id, promotion_id: @product_promotion.promotion_id, promotion_name: @product_promotion.promotion_name, promotion_show_to_customer: @product_promotion.promotion_show_to_customer, promotion_text: @product_promotion.promotion_text, require_code: @product_promotion.require_code, use_limit_per_customer: @product_promotion.use_limit_per_customer, use_limit_per_order: @product_promotion.use_limit_per_order, use_limit_per_promotion: @product_promotion.use_limit_per_promotion, user_entered: @product_promotion.user_entered }
#    assert_redirected_to product_promotion_path(assigns(:product_promotion))
#  end
#
#  test "should destroy product_promotion" do
#    assert_difference('ProductPromotion.count', -1) do
#      delete :destroy, id: @product_promotion
#    end
#
#    assert_redirected_to product_promotions_path
#  end
#end
