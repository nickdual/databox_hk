#require 'test_helper'
#
#class MarketSegmentClassificationsControllerTest < ActionController::TestCase
#  setup do
#    @market_segment_classification = market_segment_classifications(:one)
#    @controller = MarketSegmentClassificationsController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:market_segment_classifications)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create market_segment_classification" do
#    assert_difference('MarketSegmentClassification.count') do
#      post :create, market_segment_classification: { market_segment_id: @market_segment_classification.market_segment_id, party_classification_id: @market_segment_classification.party_classification_id }
#    end
#
#    assert_redirected_to market_segment_classification_path(assigns(:market_segment_classification))
#  end
#
#  test "should show market_segment_classification" do
#    get :show, id: @market_segment_classification
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @market_segment_classification
#    assert_response :success
#  end
#
#  test "should update market_segment_classification" do
#    put :update, id: @market_segment_classification, market_segment_classification: { market_segment_id: @market_segment_classification.market_segment_id, party_classification_id: @market_segment_classification.party_classification_id }
#    assert_redirected_to market_segment_classification_path(assigns(:market_segment_classification))
#  end
#
#  test "should destroy market_segment_classification" do
#    assert_difference('MarketSegmentClassification.count', -1) do
#      delete :destroy, id: @market_segment_classification
#    end
#
#    assert_redirected_to market_segment_classifications_path
#  end
#end
