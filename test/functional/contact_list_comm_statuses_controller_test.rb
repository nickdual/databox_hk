#require 'test_helper'
#
#class ContactListCommStatusesControllerTest < ActionController::TestCase
#  setup do
#    @contact_list_comm_status = contact_list_comm_statuses(:one)
#    @controller = ContactListCommStatusesController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:contact_list_comm_statuses)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create contact_list_comm_status" do
#    assert_difference('ContactListCommStatus.count') do
#      post :create, contact_list_comm_status: { communication_event_id: @contact_list_comm_status.communication_event_id, contact_list_id: @contact_list_comm_status.contact_list_id, contact_mech_id: @contact_list_comm_status.contact_mech_id, message_id: @contact_list_comm_status.message_id, party_id: @contact_list_comm_status.party_id, status_id: @contact_list_comm_status.status_id }
#    end
#
#    assert_redirected_to contact_list_comm_status_path(assigns(:contact_list_comm_status))
#  end
#
#  test "should show contact_list_comm_status" do
#    get :show, id: @contact_list_comm_status
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @contact_list_comm_status
#    assert_response :success
#  end
#
#  test "should update contact_list_comm_status" do
#    put :update, id: @contact_list_comm_status, contact_list_comm_status: { communication_event_id: @contact_list_comm_status.communication_event_id, contact_list_id: @contact_list_comm_status.contact_list_id, contact_mech_id: @contact_list_comm_status.contact_mech_id, message_id: @contact_list_comm_status.message_id, party_id: @contact_list_comm_status.party_id, status_id: @contact_list_comm_status.status_id }
#    assert_redirected_to contact_list_comm_status_path(assigns(:contact_list_comm_status))
#  end
#
#  test "should destroy contact_list_comm_status" do
#    assert_difference('ContactListCommStatus.count', -1) do
#      delete :destroy, id: @contact_list_comm_status
#    end
#
#    assert_redirected_to contact_list_comm_statuses_path
#  end
#end
