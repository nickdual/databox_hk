#require 'test_helper'
#
#class MarketSegmentPartiesControllerTest < ActionController::TestCase
#  setup do
#    @market_segment_party = market_segment_parties(:one)
#    @controller = MarketSegmentPartiesController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:market_segment_parties)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create market_segment_party" do
#    assert_difference('MarketSegmentParty.count') do
#      post :create, market_segment_party: { market_segment_id: @market_segment_party.market_segment_id, party_id: @market_segment_party.party_id, role_type_id: @market_segment_party.role_type_id }
#    end
#
#    assert_redirected_to market_segment_party_path(assigns(:market_segment_party))
#  end
#
#  test "should show market_segment_party" do
#    get :show, id: @market_segment_party
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @market_segment_party
#    assert_response :success
#  end
#
#  test "should update market_segment_party" do
#    put :update, id: @market_segment_party, market_segment_party: { market_segment_id: @market_segment_party.market_segment_id, party_id: @market_segment_party.party_id, role_type_id: @market_segment_party.role_type_id }
#    assert_redirected_to market_segment_party_path(assigns(:market_segment_party))
#  end
#
#  test "should destroy market_segment_party" do
#    assert_difference('MarketSegmentParty.count', -1) do
#      delete :destroy, id: @market_segment_party
#    end
#
#    assert_redirected_to market_segment_parties_path
#  end
#end
