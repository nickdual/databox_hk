#require 'test_helper'
#
#class TrackingCodeVisitsControllerTest < ActionController::TestCase
#  setup do
#    @controller = TrackingCodeVisitsController.new
#    @tracking_code_visit = tracking_code_visits(:one)
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:tracking_code_visits)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create tracking_code_visit" do
#    assert_difference('TrackingCodeVisit.count') do
#      post :create, tracking_code_visit: { from_date: @tracking_code_visit.from_date, source_enum_id: @tracking_code_visit.source_enum_id, tracking_code_id: @tracking_code_visit.tracking_code_id, visit_id: @tracking_code_visit.visit_id }
#    end
#
#    assert_redirected_to tracking_code_visit_path(assigns(:tracking_code_visit))
#  end
#
#  test "should show tracking_code_visit" do
#    get :show, id: @tracking_code_visit
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @tracking_code_visit
#    assert_response :success
#  end
#
#  test "should update tracking_code_visit" do
#    put :update, id: @tracking_code_visit, tracking_code_visit: { from_date: @tracking_code_visit.from_date, source_enum_id: @tracking_code_visit.source_enum_id, tracking_code_id: @tracking_code_visit.tracking_code_id, visit_id: @tracking_code_visit.visit_id }
#    assert_redirected_to tracking_code_visit_path(assigns(:tracking_code_visit))
#  end
#
#  test "should destroy tracking_code_visit" do
#    assert_difference('TrackingCodeVisit.count', -1) do
#      delete :destroy, id: @tracking_code_visit
#    end
#
#    assert_redirected_to tracking_code_visits_path
#  end
#end
