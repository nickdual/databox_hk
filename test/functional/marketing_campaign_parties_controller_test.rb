#require 'test_helper'
#
#class MarketingCampaignPartiesControllerTest < ActionController::TestCase
#  setup do
#    @marketing_campaign_party = marketing_campaign_parties(:one)
#    @controller = MarketingCampaignPartiesController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:marketing_campaign_parties)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create marketing_campaign_party" do
#    assert_difference('MarketingCampaignParty.count') do
#      post :create, marketing_campaign_party: { from_date: @marketing_campaign_party.from_date, marketing_campaign_id: @marketing_campaign_party.marketing_campaign_id, party_id: @marketing_campaign_party.party_id, role_type_id: @marketing_campaign_party.role_type_id, thru_date: @marketing_campaign_party.thru_date }
#    end
#
#    assert_redirected_to marketing_campaign_party_path(assigns(:marketing_campaign_party))
#  end
#
#  test "should show marketing_campaign_party" do
#    get :show, id: @marketing_campaign_party
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @marketing_campaign_party
#    assert_response :success
#  end
#
#  test "should update marketing_campaign_party" do
#    put :update, id: @marketing_campaign_party, marketing_campaign_party: { from_date: @marketing_campaign_party.from_date, marketing_campaign_id: @marketing_campaign_party.marketing_campaign_id, party_id: @marketing_campaign_party.party_id, role_type_id: @marketing_campaign_party.role_type_id, thru_date: @marketing_campaign_party.thru_date }
#    assert_redirected_to marketing_campaign_party_path(assigns(:marketing_campaign_party))
#  end
#
#  test "should destroy marketing_campaign_party" do
#    assert_difference('MarketingCampaignParty.count', -1) do
#      delete :destroy, id: @marketing_campaign_party
#    end
#
#    assert_redirected_to marketing_campaign_parties_path
#  end
#end
