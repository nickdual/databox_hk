#require 'test_helper'
#
#class RequestContentsControllerTest < ActionController::TestCase
#  setup do
#    @request_content = request_contents(:one)
#    @controller = RequestContentsController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:request_contents)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create request_content" do
#    assert_difference('RequestContent.count') do
#      post :create, request_content: { content_location: @request_content.content_location, from_date: @request_content.from_date, request_id: @request_content.request_id, thrudate: @request_content.thrudate }
#    end
#
#    assert_redirected_to request_content_path(assigns(:request_content))
#  end
#
#  test "should show request_content" do
#    get :show, id: @request_content
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @request_content
#    assert_response :success
#  end
#
#  test "should update request_content" do
#    put :update, id: @request_content, request_content: { content_location: @request_content.content_location, from_date: @request_content.from_date, request_id: @request_content.request_id, thrudate: @request_content.thrudate }
#    assert_redirected_to request_content_path(assigns(:request_content))
#  end
#
#  test "should destroy request_content" do
#    assert_difference('RequestContent.count', -1) do
#      delete :destroy, id: @request_content
#    end
#
#    assert_redirected_to request_contents_path
#  end
#end
