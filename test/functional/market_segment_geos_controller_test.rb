#require 'test_helper'
#
#class MarketSegmentGeosControllerTest < ActionController::TestCase
#  setup do
#    @market_segment_geo = market_segment_geos(:one)
#    @controller = MarketSegmentGeosController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:market_segment_geos)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create market_segment_geo" do
#    assert_difference('MarketSegmentGeo.count') do
#      post :create, market_segment_geo: { geo_id: @market_segment_geo.geo_id, market_segment_id: @market_segment_geo.market_segment_id }
#    end
#
#    assert_redirected_to market_segment_geo_path(assigns(:market_segment_geo))
#  end
#
#  test "should show market_segment_geo" do
#    get :show, id: @market_segment_geo
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @market_segment_geo
#    assert_response :success
#  end
#
#  test "should update market_segment_geo" do
#    put :update, id: @market_segment_geo, market_segment_geo: { geo_id: @market_segment_geo.geo_id, market_segment_id: @market_segment_geo.market_segment_id }
#    assert_redirected_to market_segment_geo_path(assigns(:market_segment_geo))
#  end
#
#  test "should destroy market_segment_geo" do
#    assert_difference('MarketSegmentGeo.count', -1) do
#      delete :destroy, id: @market_segment_geo
#    end
#
#    assert_redirected_to market_segment_geos_path
#  end
#end
