#require 'test_helper'
#
#class MarketSegmentsControllerTest < ActionController::TestCase
#  setup do
#    @market_segment = market_segments(:one)
#    @controller = MarketSegmentsController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:market_segments)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create market_segment" do
#    assert_difference('MarketSegment.count') do
#      post :create, market_segment: { description: @market_segment.description, market_segment_id: @market_segment.market_segment_id, market_segment_type_enum_id: @market_segment.market_segment_type_enum_id, product_store_id: @market_segment.product_store_id }
#    end
#
#    assert_redirected_to market_segment_path(assigns(:market_segment))
#  end
#
#  test "should show market_segment" do
#    get :show, id: @market_segment
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @market_segment
#    assert_response :success
#  end
#
#  test "should update market_segment" do
#    put :update, id: @market_segment, market_segment: { description: @market_segment.description, market_segment_id: @market_segment.market_segment_id, market_segment_type_enum_id: @market_segment.market_segment_type_enum_id, product_store_id: @market_segment.product_store_id }
#    assert_redirected_to market_segment_path(assigns(:market_segment))
#  end
#
#  test "should destroy market_segment" do
#    assert_difference('MarketSegment.count', -1) do
#      delete :destroy, id: @market_segment
#    end
#
#    assert_redirected_to market_segments_path
#  end
#end
