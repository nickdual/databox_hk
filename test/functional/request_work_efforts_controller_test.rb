#require 'test_helper'
#
#class RequestWorkEffortsControllerTest < ActionController::TestCase
#  setup do
#    @request_work_effort = request_work_efforts(:one)
#    @controller = RequestWorkEffortsController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:request_work_efforts)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create request_work_effort" do
#    assert_difference('RequestWorkEffort.count') do
#      post :create, request_work_effort: { request_id: @request_work_effort.request_id, work_effort_id: @request_work_effort.work_effort_id }
#    end
#
#    assert_redirected_to request_work_effort_path(assigns(:request_work_effort))
#  end
#
#  test "should show request_work_effort" do
#    get :show, id: @request_work_effort
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @request_work_effort
#    assert_response :success
#  end
#
#  test "should update request_work_effort" do
#    put :update, id: @request_work_effort, request_work_effort: { request_id: @request_work_effort.request_id, work_effort_id: @request_work_effort.work_effort_id }
#    assert_redirected_to request_work_effort_path(assigns(:request_work_effort))
#  end
#
#  test "should destroy request_work_effort" do
#    assert_difference('RequestWorkEffort.count', -1) do
#      delete :destroy, id: @request_work_effort
#    end
#
#    assert_redirected_to request_work_efforts_path
#  end
#end
