require 'spec_helper'

describe PromotionRulesController do
  def promotion_rule_valid
    {
      :name => "MyString" ,
      :product_promotion_id => 1
    }
    end

  def promotion_rule_invalid
    {
      :name => "" ,
      :product_promotion_id => 1
    }
  end
  def promotion_rule_condition_valid
    {
        :operator_id => 1       ,
        :value =>1               ,
        :other =>1                ,
        :shipment_method_id => 1   ,
        :promotion_rule_condition_name => 'PPIP_ORDER_TOTAL'
    }
  end
  def promotion_rule_condition_invalid
    {
        :operator_id => 1       ,
        :value =>''               ,
        :other =>''                ,
        :shipment_method_id => 1   ,
        :promotion_rule_condition_name => 'PPIP_ORDER_TOTAL'
    }
    end
  def rule_condition_update_valid
    {
        :operator_id => 1       ,
        :value =>2               ,
        :other =>2                ,
        :shipment_method_id => 1   ,
        :promotion_rule_condition_name => 'PPIP_ORDER_TOTAL'
    }
  end
  def promotion_rule_condition_category_valid
    {
      :product_category_id => "MyString" ,
      :sub_category_status => ""          ,
      :group_id  => 1                      ,
      :promotion_rule_condition_id => 1
    }
  end
  def promotion_rule_condition_category_invalid
    {
      :product_category_id => "MyString" ,
      :sub_category_status => ""          ,
      :group_id  => ''                      ,
      :promotion_rule_condition_id => ''
        }
  end
  def promotion_rule_condition_product_valid
    {
      :product_id  => 1,
      :sub_category_status => "",
      :promotion_rule_condition_id => 1
    }
  end
  def promotion_rule_condition_product_invalid
    {
      :product_id  => '',
      :sub_category_status => "",
      :promotion_rule_condition_id => 1
    }
  end
def promotion_product_valid
    {
      :product_id  => 1,
      :sub_category_status => "",
      :product_promotion_id => 1
    }
  end

  #before :each do
  #  @request.env["devise.mapping"] = Devise.mappings[:user]
  #  @current_user = FactoryGirl.create(:user)
  #  sign_in @current_user
  #  controller.stub(:authenticate_user!).and_return true
  #end


  describe "GET new" do
    it 'request to new promotion rule' do

      @product_promotion = FactoryGirl.create(:product_promotion)
      session[:product_promotion_id] = @product_promotion.id
      get :new ,{:product_promotion_id => @product_promotion.id}
      response.code.should eq('200')
    end
    it 'render to new promotion rule page' do
      @product_promotion = FactoryGirl.create(:product_promotion)
      session[:product_promotion_id] = @product_promotion
      get :new, {:promotion_id => @product_promotion.id}
      response.should render_template("new")
    end

  end

  describe "GET edit" do
    it 'request to edit promotion rule' do
      @promotion_rule = FactoryGirl.create(:promotion_rule)
      @promotion_rule_conditions = FactoryGirl.create(:promotion_rule_condition)
      @promotion_rule_condition_category = PromotionRuleConditionCategory.new()
      @promotion_rule_condition_product = PromotionRuleConditionProduct.new()
      get :edit, {:id => @promotion_rule.id}
      response.code.should eq('200')
    end
    it 'render to edit promotion rule template' do
      @promotion_rule = FactoryGirl.create(:promotion_rule)
      @promotion_rule_conditions = FactoryGirl.create(:promotion_rule_condition)
      @promotion_rule_condition_category = PromotionRuleConditionCategory.new()
      @promotion_rule_condition_product = PromotionRuleConditionProduct.new()
      get :edit, {:id => @promotion_rule.id}
      response.should render_template("edit")
    end

  end
  describe 'PUT update_promotion_rule' do
    it 'render to status true if update successfully' do
      @promotion_rule = FactoryGirl.create(:promotion_rule)
      put :update_promotion_rule, {:promotion_rule => {:id => @promotion_rule.id,:name => 'myname'}}
      JSON.parse(response.body)["success"] == "true"
    end
    it 'render to status false if update error' do
      @promotion_rule = FactoryGirl.create(:promotion_rule)
      put :update_promotion_rule, {:promotion_rule => {:id => @promotion_rule.id,:name => ''}}
      JSON.parse(response.body)["success"] == "false"
    end
  end


  describe 'POST add_promotion_rule' do
    it 'render to status true if add promotion rule successfully' do
      post :add_promotion_rule, {:promotion_rule => promotion_rule_valid}
      JSON.parse(response.body)["success"] == "true"
    end
    it 'render to status false if add promotion rule error' do
      post :add_promotion_rule, {:promotion_rule => promotion_rule_invalid}
      JSON.parse(response.body)["success"] == "false"
    end

  end
  describe 'POST add_rule_condition' do
    it 'render to status true if add rule condition successfully' do
      post :add_rule_condition, {:promotion_rule_condition => promotion_rule_condition_valid}
      JSON.parse(response.body)["success"] == "true"
    end
    it 'render to status false if add rule condition error' do
      post :add_rule_condition, {:promotion_rule_condition => promotion_rule_condition_invalid}
      JSON.parse(response.body)["success"] == "false"
    end
  end

  describe 'PUT update_rule_condition' do
    it 'render to status true if update rule condition successfully' do
      @promotion_rule_condition = FactoryGirl.create(:promotion_rule_condition)
      post :update_rule_condition, {:id => @promotion_rule_condition.id, :promotion_rule_condition => rule_condition_update_valid }
      JSON.parse(response.body)["success"] == "true"
    end
    it 'render to status false if update rule condition error' do
      @promotion_rule_condition = FactoryGirl.create(:promotion_rule_condition)
      post :update_rule_condition, {:id => @promotion_rule_condition.id, :promotion_rule_condition => promotion_rule_condition_invalid }
      JSON.parse(response.body)["success"] == "false"
    end
  end

  describe 'POST create' do
    it 'create a new promotion rule' do
      post :create, {:promotion_rule =>promotion_rule_valid}
      assigns(:promotion_rule).should be_a(PromotionRule)
      assigns(:promotion_rule).should be_persisted
    end
    it 'redirect to manager promotion rule page' do
      post :create, {:promotion_rule =>promotion_rule_valid}
      response.should be_redirect
    end

  end
  describe 'DELETE delete_rule_condition' do
    it 'delete a promotion rule' do
      @promotion_rule_condition = FactoryGirl.create(:promotion_rule_condition)
      delete :delete_rule_condition, {:id =>@promotion_rule_condition.id}
      JSON.parse(response.body)["status"] == "ok"
    end
  end

  describe 'POST add_rule_condition_category' do
    it 'render to status true if add rule condition category successfully' do
      post :add_rule_condition_category, {:promotion_rule_condition_category => promotion_rule_condition_category_valid}
      JSON.parse(response.body)["success"] == "true"
    end
    it 'render to status false if add rule condition category error' do
      post :add_rule_condition_category, {:promotion_rule_condition_category => promotion_rule_condition_category_invalid}
      JSON.parse(response.body)["success"] == "false"
    end
  end

  describe 'update_rule_condition_category' do
    it 'render to status true if update rule condition category successfully' do
      @promotion_rule_condition_category = PromotionRuleConditionCategory.create(promotion_rule_condition_category_valid)
      post :update_rule_condition_category, {:id => @promotion_rule_condition_category.id, :promotion_rule_condition_category => promotion_rule_condition_category_valid }
      JSON.parse(response.body)["status"] == "ok"
    end
    it 'render to status false if update rule condition category error' do
      @promotion_rule_condition_category = PromotionRuleConditionCategory.create(promotion_rule_condition_category_valid)
      post :update_rule_condition_category, {:id => @promotion_rule_condition_category.id, :promotion_rule_condition_category => promotion_rule_condition_category_invalid }
      JSON.parse(response.body)["status"] == "403"
    end
    end

  describe 'DELETE delete rule condition category' do
    it 'delete rule condition category' do
      @promotion_rule_condition_category = PromotionRuleConditionCategory.create(promotion_rule_condition_category_valid)
      delete :delete_rule_condition_category, {:id =>@promotion_rule_condition_category.id}
      JSON.parse(response.body)["status"] == "ok"
    end
  end
  describe 'DELETE delete rule condition product' do
    it 'delete rule condition product' do
      @promotion_rule_condition_product = FactoryGirl.create(:promotion_rule_condition_product)
      delete :delete_rule_condition_product, {:id =>@promotion_rule_condition_product.id}
      JSON.parse(response.body)["status"] == "ok"
    end
  end


  describe 'add_rule_condition_product' do
    it 'add rule condition product success'  do
      post :add_rule_action_product,{ :promotion_rule_condition_product => promotion_rule_condition_product_valid}
      JSON.parse(response.body)["status"] == "true"
      end
    it 'add rule condition product error'  do
      post :add_rule_action_product,{ :promotion_rule_condition_product => promotion_rule_condition_product_invalid}
      JSON.parse(response.body)["status"] == "false"
    end
  end

  describe 'update_rule_condition_product' do
    it 'render to status true if update rule condition product successfully' do
      @promotion_rule_condition_product = PromotionRuleConditionProduct.create(promotion_rule_condition_product_valid)
      post :update_rule_condition_product, {:id => @promotion_rule_condition_product.id, :promotion_rule_condition_product => promotion_rule_condition_product_valid }
      JSON.parse(response.body)["status"] == "ok"
    end
    it 'render to status false if update rule condition product error' do
      @promotion_rule_condition_product = PromotionRuleConditionProduct.create(promotion_rule_condition_product_valid)
      post :update_rule_condition_product, {:id => @promotion_rule_condition_product.id, :promotion_rule_condition_product => promotion_rule_condition_product_invalid }
      JSON.parse(response.body)["status"] == 403
    end
  end


  describe "POST add_rule_action_category" do
    context "with vaild attributes" do
      it "render true when add new rule action category" do
        post :add_rule_action_category, {:promotion_rule_action_category => {:product_category_id => 1, :sub_category_status => "include", :group_id => 10, :promotion_rule_action_id => 1}}
        JSON.parse(response.body)["success"] == "true"
      end
    end
    context "with invaild attributes" do
      it "render false when add new rule action category" do
        post :add_rule_action_category, {:promotion_rule_action_category => {:product_category_id => '', :sub_category_status => "include", :group_id => '', :promotion_rule_action_id => 1}}
        JSON.parse(response.body)["success"] == "false"
      end
    end
  end

  describe "POST delete_rule_action_category" do
    it "delete rule action category" do
      rule = PromotionRuleActionCategory.create({:product_category_id => 1, :sub_category_status => "include", :group_id => 10, :promotion_rule_action_id => 1})
      post :delete_rule_action_category, {:id => rule.id}

    end
  end

  describe 'update rule action' do
    it 'render to status true if update rule action successfully' do
      @promotion_rule_action = FactoryGirl.create(:promotion_rule_action)
      post :update_rule_action, {:id => @promotion_rule_action.id, :promotion_rule_action => {:promotion_rule_action_name => "PPRA_ORDER_PERCENT_DISCOUNT",:quantity =>1,:amount => 1,:party_id => "MyString",:service_name => "MyString"} }
      JSON.parse(response.body)["success"] == "true"
    end
    it 'render to status false if update rule action error' do
      @promotion_rule_action = FactoryGirl.create(:promotion_rule_action)
      post :update_rule_action, {:id => @promotion_rule_action.id, :promotion_rule_action => {:promotion_rule_action_name => "",:quantity => '',:amount => 1,:party_id => "",:service_name => "MyString"} }
      JSON.parse(response.body)["success"] == 'false'
    end
  end

  describe 'delete rule action' do
    it 'delete rule action' do
      @promotion_rule_action = FactoryGirl.create(:promotion_rule_action)
      delete :delete_rule_action, {:id =>@promotion_rule_action.id}
      JSON.parse(response.body)["status"] == "ok"
    end
  end

  describe 'delete rule action product' do
    it 'delete rule action product' do
      @promotion_rule_action_product = FactoryGirl.create(:promotion_rule_action_product)
      delete :delete_rule_action_product, {:id =>@promotion_rule_action_product.id}
      JSON.parse(response.body)["status"] == "ok"
    end
  end

  describe 'add rule action product' do
    it 'add rule action product success' do
      post :add_rule_action_product,{:promotion_rule_action_product => {:product_id => 1,:sub_category_status =>'',:promotion_rule_action_id => 1 }}
      JSON.parse(response.body)["success"] == "true"
    end
    it 'add rule action product error' do
      post :add_rule_action_product,{:promotion_rule_action_product => {:product_id => '',:sub_category_status =>'',:promotion_rule_action_id => 1 }}
      JSON.parse(response.body)["success"] == "false"
    end
  end

  describe 'insert rule action' do
    it 'insert rule action success' do
      post :insert_rule_action,{:promotion_rule_action => {:promotion_rule_id => 1,:promotion_rule_action_name => "",:quantity => 1,:amount => 1,:item_id => 1,:party_id => "MyString",:service_name => "MyString",:use_cart_quantity => false }}
      JSON.parse(response.body)["success"] == "true"
    end
    it 'insert rule action error' do
      post :insert_rule_action,{:promotion_rule_action => {:promotion_rule_id => 1,:promotion_rule_action_name => "",:quantity => '',:amount => 1,:item_id => 1,:party_id => '',:service_name => '',:use_cart_quantity => false }}
      JSON.parse(response.body)["success"] == "false"
    end
  end

  describe 'add promotion category' do
    it 'add promotion category success' do
      @product_promotion =  FactoryGirl.create(:product_promotion)
      @product =  FactoryGirl.create(:product)
      session[:product_promotion_id] = @product_promotion.id
      post :add_promotion_category,{:promotion_category => {:group_id  => 1,:product_promotion_id => 'asdfsasdfsdfv1234',:product_category_id => 1,:sub_category_status =>'' }}
      JSON.parse(response.body)["status"] == "ok"
    end
    it 'add promotion category error' do
      @product_promotion =  FactoryGirl.create(:product_promotion)
      session[:product_promotion_id] = @product_promotion.id
      @product =  FactoryGirl.create(:product)
      post :add_promotion_category,{:promotion_category => {:group_id  => 1,:product_promotion_id => 1,:product_category_id => 1,:sub_category_status =>'' }}
      JSON.parse(response.body)["status"] == 403
    end
  end
  #
  describe 'add promotion product' do
    it 'add promotion product success'  do
      @product_promotion =  FactoryGirl.create(:product_promotion)
      @product =  FactoryGirl.create(:product)
      session[:product_promotion_id] = @product_promotion.id
      post :add_promotion_product,{:promotion_product => {:product_id  => 1,  :sub_category_status => "", :product_promotion_id => 1}}
      JSON.parse(response.body)["status"] == "ok"
    end
    it 'add promotion product error'  do
      @product_promotion =  FactoryGirl.create(:product_promotion)
      @product =  FactoryGirl.create(:product)
      session[:product_promotion_id] = @product_promotion.id
      post :add_promotion_product,{:promotion_product => {:product_id  => '',  :sub_category_status => "", :product_promotion_id => 1}}
      JSON.parse(response.body)["status"] == 403
    end
  end

  describe 'DELETE delete promotion category' do
    it 'delete promotion category' do
      @promotion_category = FactoryGirl.create(:promotion_category)
      delete :delete_promotion_category, {:id =>@promotion_category.id}
      JSON.parse(response.body)["status"] == "ok"
    end
  end
  describe 'DELETE delete promotion product' do
    it 'delete promotion product' do
      @promotion_product = FactoryGirl.create(:promotion_product)
      delete :delete_promotion_product, {:id =>@promotion_product.id}
      JSON.parse(response.body)["status"] == "ok"
    end
  end

end