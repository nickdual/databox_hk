require 'spec_helper'

describe ShipRatesController do

  # This should return the minimal set of attributes required to create a valid
  # User. As you add validations to User, be sure to
  # update the return value of this method accordingly.


  describe "GET index " do
    it "list ship rates" do
      get :index
      response.should render_template :index
    end
  end

  describe "POST get_for_country" do
    it "get_for_country" do
      @city = FactoryGirl.create(:city)
      @city2 = FactoryGirl.create(:city2)
      post :get_for_country ,{:type => 'country', :id => 'US'}
      JSON.parse(response.body)["data"] != 'nil'
    end
    end

  describe "POST get_for_city_autocomplete" do
    it "get_for_city_autocomplete" do
      @city = FactoryGirl.create(:city)
      @city2 = FactoryGirl.create(:city2)
      post :get_for_city_autocomplete ,{:type => 'US', :content => 'Honesda'}
      JSON.parse(response.body)["data"] != 'nil'
    end
  end

  describe "POST update_shipment" do
    it "update_shipment" do
      @shipment = FactoryGirl.create(:shipment)
      @shipment_package = FactoryGirl.create(:shipment_package)
      @origin_contact_mech = FactoryGirl.create(:postal_address)
      @destination_contact_mech = FactoryGirl.create(:postal_address2)
          post :update_shipment ,{:id => 1, :weight => 1, :length => 100, :width => 100, :height => 100, :starting_point => { :country_code => "US", :city_name => "Beverly Hills", :postcode => "90210" }, :ending_point =>{ :country_code => "CA", :city_name => "Ottawa", :postcode => "12345" }}
      JSON.parse(response.body)["status"] != 'ok'
    end
  end

  describe "POST get_rates" do
    it "get_rates" do
      session[:promo_page] = 10
      post :get_rates, {:weight => 1, :length => 100, :width => 100, :height => 100, :starting_point => { :country_code => "US", :city_name => "Beverly Hills", :postcode => "90210" }, :ending_point =>{ :country_code => "CA", :city_name => "Ottawa", :postcode => "12345" }}
      JSON.parse(response.body)["data"] != 'nil'
    end
  end

  describe "POST get_quote" do
    it "get_quote" do
      @shipment = FactoryGirl.create(:shipment)
      @shipment_package = FactoryGirl.create(:shipment_package)
      @origin_contact_mech = FactoryGirl.create(:postal_address)
      @destination_contact_mech = FactoryGirl.create(:postal_address2)
      post :get_quote, {:id => 1}
      JSON.parse(response.body)["data"] != 'nil'
    end
  end
end
