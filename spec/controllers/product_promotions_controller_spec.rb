require 'spec_helper'

describe ProductPromotionsController do

  def valid_attributes
    {
        :billback_factor => 3 ,
        :override_org_party_id =>4 ,
        :promotion_name =>'MyString' ,
        :promotion_show_to_customer  => false ,
        :promotion_text => 'MyText',
        :require_code => false  ,
        :use_limit_per_customer => 3,
        :use_limit_per_order => 2,
        :use_limit_per_promotion => 2 ,
        :user_entered =>true
    }
  end

  def invalid_attributes
    {
        :promotion_name =>'' ,
        :promotion_text => 'MyText',
    }
  end
  def valid_attributes_update_all
    {
        :billback_factor => 2 ,
        :override_org_party_id =>2 ,
        :promotion_name =>'MyString' ,
        :promotion_show_to_customer  => false ,
        :promotion_text => 'MyText',
        :require_code => false  ,
        :use_limit_per_customer => 2,
        :use_limit_per_order => 2,
        :use_limit_per_promotion => 2 ,
        :user_entered =>true,

    }
  end


  before :each do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @current_user = FactoryGirl.create(:user)
    sign_in @current_user
    controller.stub(:authenticate_user!).and_return true
  end



  #
  describe "GET new" do
    it "a new product promotion as @product_promotion" do
      get :new
      response.should render_template("new")
    end
  end

  describe "GET edit" do
    it "render to edit template" do
      @product_promotion = ProductPromotion.create(valid_attributes)
      get :edit,{:id => @product_promotion.id}
      response.should render_template("edit")
    end
    it "requested edit product_promotion " do
      @product_promotion = ProductPromotion.create(valid_attributes)
      get :edit, {:id => @product_promotion.id}
      response.code.should eq('200')
    end
  end
  #
  describe "POST create" do
    describe "with valid params" do
      it "creates a new Promotion" do
        post :create, {:product_promotion => valid_attributes}
        assigns(:product_promotion).should be_a(ProductPromotion)
        assigns(:product_promotion).should be_persisted
      end


      it "redirects to the created product_promotion" do
        post :create, {:product_promotion => valid_attributes }
        response.should be_redirect
        end

    end

    #describe 'with invalid params' do
      #it "does not save the new product promotion" do
      #  expect{
      #    post :create, :product_promotion => invalid_attributes
      #  }.to_not change(ProductPromotion,:count)
      #  end
  #    it "does not save the new product promotion" do
  #      expect{
  #        post :create, :product_promotion => invalid_attributes
  #      }.should have(1).error_on(:promotion_name)
  #    end
  #
  #    it "re-renders the new method" do
  #      post :create, product_promotion: FactoryGirl.attributes_for(:invalid_product_promotion_1)
  #      response.should render_template :new
  #    end
  #
  #  end
  ##
  end
  ##
  describe "PUT update" do
    describe "with valid params" do
      before(:each) do
        @product_promotion = FactoryGirl.create(:product_promotion)
      end


      it "product_promotion as @product_promotion" do
        put :update, {:id => @product_promotion.to_param, :product_promotion => valid_attributes_update_all}
        assigns(:product_promotion).should ==(@product_promotion)
      end

      it "redirects to the product_promotion" do
        put :update, {:id => @product_promotion.to_param, :product_promotion => valid_attributes_update_all}
        response.should redirect_to '/product_promotions'
      end
    end
  end
  #
  describe "DELETE destroy" do
    before(:each) do
      @product_promotion = FactoryGirl.create(:product_promotion)
    end
    it "destroys the requested @product_promotion" do

      expect {
        delete :destroy, {:id => @product_promotion.to_param}
      }.to change(ProductPromotion, :count).by(-1)
    end
  end
  #
  describe "GET index" do
    it "renders the :index view" do
      get :index
      response.should render_template :index
    end
  end

  describe "POST promotion_per_page" do
    context "with valid attributes" do
      it "params with per" do
        post :promotions_per_page, {:per => 10, :page => 1}
      end
    end

    context "with invalid attributes" do
      it "params without per" do
        session[:promo_page] = 10
        post :promotions_per_page
      end
    end
  end

  describe "POST delete_promotions" do
    context "with valid attributes" do
      it "params delete" do
        post :delete_promotions, {:per => 10, :list => [1, 2]}
      end
    end
    context "with invaild attributes" do
      it "params without per" do
        session[:promo_page] = 10
        post :delete_promotions, {:list => [1, 2]}
      end
      it "params without per" do
        post :delete_promotions, {:per => 10, :list => [1, 2]}
      end
    end
  end

  describe "GET stores" do
    it "renders the :stores view" do
      get :stores
      response.should render_template :stores
    end
  end

  describe "POST stores_per_page" do
    context "with vaild attributes" do
      it "params with per" do
        post :stores_per_page, {:per => 10}
      end
    end
    context "with invaild attributes" do
      it "params without per" do
        session[:promo_page] = 10
        post :stores_per_page
      end
    end
  end

  describe "POST update_stores" do
    context "with vaild attributes" do
      it "update store with id" do
        store = FactoryGirl.create(:promotion_store)
        post :update_stores, {:id => store.id, :thru_date => DateTime.now, :sequence_num => 1}
      end
    end

    context "with invaild attributes" do
      it "update stores without id" do
        store = FactoryGirl.create(:promotion_store)
        post :update_stores,  {:id => store.id}
      end
    end
  end

  describe "POST insert_stores" do
    context "with vaild attributes" do
      it "insert store" do
        post :insert_stores, {:product_store_id => 1, :from_date => DateTime.now, :thru_date => DateTime.now, :per => 10}
      end
    end
    context "with invaild attributes" do
      it "insert store fail" do
        session[:promo_page] = 10
        post :insert_stores
      end
    end
  end

  describe "POST delete_stores" do
    context "with vaild attributes" do
      it "delete stores" do
        post :delete_stores, {:per => 10, :list => [1, 2]}
      end
    end
    context "with invaild attribures" do
      it "params without per" do
        expect {
          post :delete_stores
        }.to raise_error
      end
      it "params without per" do
        session[:promo_page] = 10
        post :delete_stores, {:list => [1, 2], :page => 1}
      end
    end
     it "list product promotion" do
      get :index
      response.should render_template :index
     end
  end
end
