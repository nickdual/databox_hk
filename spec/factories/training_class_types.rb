# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :training_class_type do
    description "MyString"
    name "MyString"
  end
end
