# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :rate_time_period_type do
    description "MyString"
    time_period_type_id "MyString"
    period_length 1
    length_uom_id "MyString"
  end
end
