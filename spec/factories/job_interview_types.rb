# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :job_interview_type do
    description "MyString"
    name "MyString"
  end
end
