# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :payroll_deduction_type do
    name "MyString"
    description "MyString"
  end
end
