# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :promotion_rule do
    id 1
    product_promotion_id 1
    name "MyString"
  end
end
