# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :job_posting_type do
    description "MyString"
    name "MyString"
  end
end
