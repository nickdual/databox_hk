# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :qualification_type do
    description "MyString"
    name "MyString"
  end
end
