# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invoice_sequence do
    name "MyString"
    description "MyString"
    sequence_num 1
  end
end
