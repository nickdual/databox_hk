FactoryGirl.define do
  factory :promotion_store do
    from_date DateTime.now
    product_store_id 1
    sequence_num 1
    thru_date DateTime.now
    product_promotion_id 1
  end
end
