# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :employment_application_referred_by do
    description "MyString"
    name "MyString"
  end
end
