# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :party_qualification_verification do
    description "MyString"
    sequence_num 1
    status_id "MyString"
  end
end
