# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :party_qualification_status do
    description "MyString"
    sequence_num 1
    status_id "MyString"
  end
end
