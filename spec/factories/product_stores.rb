# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_store do
    id 1
    store_name "Store Name"
  end
end
