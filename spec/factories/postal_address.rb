FactoryGirl.define do
  factory :postal_address do
    id 1
    contact_mech_id  1
    city "Beverly Hills"
    postal_code "90210"
    access_code "US"
    created_at '2013-01-05 14:58:14.005616'
    updated_at '2013-01-05 14:58:14.005616'
  end

  factory :postal_address2 , :class => 'PostalAddress' do
    id 2
    contact_mech_id  2
    city "Ottawa"
    postal_code "K1P 1J1"
    access_code "CA"
    created_at '2013-01-05 14:58:14.005616'
    updated_at '2013-01-05 14:58:14.005616'
    end

end
