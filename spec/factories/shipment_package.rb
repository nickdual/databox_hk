FactoryGirl.define do
  factory :shipment_package  do
    id 1
    shipment_id 1
    box_length 1.0
    box_height 1.0
    box_width 1.0
    weight 1.0
    created_at '2013-01-05 14:58:14.005616'
    updated_at '2013-01-05 14:58:14.005616'
  end
end
