# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :promotion_rule_condition_category do
    id 1
    product_category_id "MyString"
    sub_category_status "Include"
    group_id 1
    promotion_rule_condition_id 1
  end
end
