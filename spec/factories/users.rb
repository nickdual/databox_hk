FactoryGirl.define do
  factory :user do
    id 1
    email "abc@gmail.com"
    password "123456"
    sign_in_count "0"
    created_at '2012-11-21 10:24:28 '
    confirmed_at '2012-11-22 10:01:19'
    approval true
  end
end
