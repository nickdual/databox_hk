FactoryGirl.define do
  factory :city do
    id 1
    country_id 1
    division_id 3581
    geonames_id 5194099
    name "Honesdale"
    ascii_name "Honesdale"
    alternate_name ''
    latitude 41.57676000
    longitude -75.25879000
    country_iso_code_two_letters "US"
    admin_1_code "PA"
    admin_2_code '127'
    admin_3_code ''
    admin_4_code ''
    population 4480
    geonames_timezone_id 0

  end

  factory :city2 , :class => 'City' do
    id 2
    country_id 234
    division_id 3574
    geonames_id 5228623
    name "Howard"
    ascii_name "Howard"
    alternate_name ''
    latitude 44.01081000
    longitude -97.52674000
    country_iso_code_two_letters "US"
    admin_1_code "SD"
    admin_2_code '097'
    admin_3_code ''
    admin_4_code ''
    population 858
    geonames_timezone_id 0

  end
end
