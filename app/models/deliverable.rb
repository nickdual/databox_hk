class Deliverable < ActiveRecord::Base
  attr_accessible :deliverable_name, :deliverable_type_id, :description
  # Association
  has_many :work_effort_deliverable_prods
end
