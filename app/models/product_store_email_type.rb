class ProductStoreEmailType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :product_store_emails, :class_name => 'ProductStoreEmail', :foreign_key => :email_type_id
end
