class AcctgTransType < ActiveRecord::Base
  attr_accessible :description, :name, :parent_id
  belongs_to :parent, :class_name => 'AcctgTransType', :foreign_key => :parent_id
  has_many :children, :class_name => 'AcctgTransType', :foreign_key => :parent_id
  has_many :acctg_trans
end
