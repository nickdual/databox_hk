class ProductFeatureAppl < ActiveRecord::Base
  attr_accessible :amount, :appl_type_id, :from_date, :product_feature_id, :product_id, :recurring_amount, :sequence_num, :thru_date
  # Composite primary keys
  self.primary_keys = :product_id, :product_feature_id, :from_date
  # Association
  belongs_to :product
  belongs_to :appl_type, :class_name => 'ProductFeatureApplType', :foreign_key => :appl_type_id
  belongs_to :product_feature
end
