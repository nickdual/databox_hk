class QualificationType < ActiveRecord::Base
  attr_accessible :description, :name, :parent_id
  belongs_to :parent, :class_name => 'QualificationType', :foreign_key => :parent_id
  has_many :children, :class_name => 'QualificationType', :foreign_key => :parent_id
  has_many :party_qualifications
end
