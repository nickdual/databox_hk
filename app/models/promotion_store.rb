class PromotionStore < ActiveRecord::Base
  attr_accessible :from_date, :product_store_id, :sequence_num, :thru_date, :product_promotion_id
  # Association
  belongs_to :product_store
  belongs_to :product_promotion, :foreign_key => :product_promotion_id
  def self.simple_search(params)
    if (params['content'].blank?)
      product_promotions = PromotionStore.joins(:product_store).select("product_stores.store_name, promotion_stores.*").where(:product_promotion_id => params[:product_promotion_id])
    else
      type = params[:type]
      content = params[:content]
      if (type == 'store_name')
        product_promotions = PromotionStore.joins(:product_store).select("product_stores.store_name, promotion_stores.*").where([type + ' like ? AND product_promotion_id = ?', '%' + content + '%', params[:product_promotion_id]])
      else
        product_promotions = PromotionStore.joins(:product_store).select("product_stores.store_name, promotion_stores.*").where({type => content, :product_promotion_id => params[:product_promotion_id]})
      end
    end

    return product_promotions
  end

  def self.standard_data(items)
    result = []
    if (items.blank?)
      return nil
    end
    items.each do |item|
      thru_date = ''
      from_date = ''
      if (!item['thru_date'].blank?)
        thru_date = item['thru_date'].strftime("%Y-%m-%d %H:%M:%S:%L")
      end
      if (!item['from_date'].blank?)
        from_date = item['from_date'].strftime("%Y-%m-%d %H:%M:%S:%L")
      end
      pr = ActiveSupport::JSON.decode(item.to_json)
      pr['thru_date'] = thru_date
      pr['from_date'] = from_date
      result.push(pr)
    end
    return result
  end
end
