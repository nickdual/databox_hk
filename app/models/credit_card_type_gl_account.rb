class CreditCardTypeGlAccount < ActiveRecord::Base
  attr_accessible :credit_card_type_enum_id, :organization_party_id, :gl_account_id

  belongs_to :credit_card_type
  belongs_to :party
  belongs_to :gl_account
end
