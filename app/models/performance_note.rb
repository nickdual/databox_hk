class PerformanceNote < ActiveRecord::Base
  attr_accessible :employee_party_id, :note_date, :note_text
end
