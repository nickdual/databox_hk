class FacilityLocationType < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :facility_locations
end
