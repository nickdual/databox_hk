class CreditCard < ActiveRecord::Base
  attr_accessible :card_number, :card_number_lookup_hash, :company_name_on_card, :consecutive_failed_auths, :consecutive_failed_nsf, :credit_card_type_enum_id, :expire_date, :first_name_on_card, :issue_number, :last_failed_auth_date, :last_failed_nsf_date, :last_name_on_cad, :middle_name_on_card, :payment_method_id, :suffix_on_card, :title_on_card, :valid_from_date
  
  belongs_to :payment_method
  belongs_to :credit_card_type_enum, :class_name => "CreditCardType", :foreign_key => "credit_card_type_enum_id"
end
