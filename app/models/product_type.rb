class ProductType < ActiveRecord::Base
  attr_accessible :description, :name, :parent_id
  # Association
  belongs_to :parent, :class_name => 'ProductType', :foreign_key => :parent_id
  has_many :children, :class_name => 'ProductType', :foreign_key => :parent_id
  has_many :products
end
