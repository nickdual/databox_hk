# require 'composite_primary_keys'
class CommunicationEventProduct < ActiveRecord::Base
  attr_accessible :communication_event_id, :product_id
  # Composite primary key
  self.primary_keys = :communication_event_id, :product_id
  # Association
  belongs_to :communication_event
  belongs_to :product
end
