class ContactListCommStatus < ActiveRecord::Base
  attr_accessible :communication_event_id, :contact_list_id, :contact_mech_id, :message_id, :party_id, :status_id
  
  belongs_to :contact_list
  belongs_to :communication_event
  belongs_to :contact_mech
  belongs_to :party
  belongs_to :status, :class_name => "StatusItem"
end
