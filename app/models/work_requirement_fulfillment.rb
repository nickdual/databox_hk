class WorkRequirementFulfillment < ActiveRecord::Base
  attr_accessible :fulfillment_type_enum_id, :requirement_id, :work_effort_id
end
