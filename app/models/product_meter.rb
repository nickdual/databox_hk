class ProductMeter < ActiveRecord::Base
  attr_accessible :meter_name, :meter_uom_id, :product_id, :product_meter_type_id
  # Composite primary keys
  self.primary_keys = :product_id, :product_meter_type_id
  # Association
  belongs_to :product
  belongs_to :product_meter_type
  has_many :interval_product_maintenances, :class_name => 'ProductMaintenance', :foreign_key => [:product_id, :interval_meter_type_id]
end
