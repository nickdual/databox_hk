class SupplierPreferredOrder < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
  # Association
  has_many :supplier_products, :class_name => 'SupplierProduct', :foreign_key => :preferred_order_id
end
