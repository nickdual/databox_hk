class Person < ActiveRecord::Base
  attr_accessible :birth_date, :comments, :deceased_date, :employment_status_id, :first_name, :gender, :height, :last_name, :marital_status_id, :middle_name, :mothers_maiden_name, :nickname, :occupation, :party_id, :personal_title, :residence_status_id, :salutation, :suffix, :weight
  # self.primary_key :party_id
  # Association
  belongs_to :party
  belongs_to :marital_status
  belongs_to :employment_status
  belongs_to :residence_status
  has_many :order_headers


  def full_name
  	"#{first_name} #{last_name}"
  end
end
