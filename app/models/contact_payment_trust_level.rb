class ContactPaymentTrustLevel < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
  # Association
  has_many :contact_meches
end
