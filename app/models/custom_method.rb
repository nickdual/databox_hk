class CustomMethod < ActiveRecord::Base
  attr_accessible :custom_method_id, :custom_method_name, :custom_method_type_id, :description
end
