# require 'composite_primary_keys'
class CommunicationEventPurpose < ActiveRecord::Base
  attr_accessible :communication_event_id, :description, :purpose_id
  # Composite primary key
  self.primary_keys = :communication_event_id, :purpose_id
  # Association
  belongs_to :communication_event
  belongs_to :communication_purpose, :class_name => 'CommunicationPurpose', :foreign_key => 'purpose_id'
end
