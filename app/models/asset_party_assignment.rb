class AssetPartyAssignment < ActiveRecord::Base
  attr_accessible :allocated_date, :asset_id, :comments, :from_date, :party_id, :role_type_id, :status_id, :thru_date
  # Composite primary keys
  self.primary_keys = :asset_id, :party_id, :role_type_id, :from_date
  # Association
  belongs_to :party
  belongs_to :role_type
  belongs_to :asset
end
