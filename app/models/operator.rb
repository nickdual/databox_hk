class Operator < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  validates :name, :uniqueness => true
  has_many :promotion_rule_conditions
  def self.operator_select
     self.all.map{ |item| [item.name,item.id] }
  end

end
