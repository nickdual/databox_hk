class RequestWorkEffort < ActiveRecord::Base
  attr_accessible :request_id, :work_effort_id
  
  belongs_to :request
  belongs_to :work_effort
end
