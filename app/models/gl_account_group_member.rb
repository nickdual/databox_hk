class GlAccountGroupMember < ActiveRecord::Base
  attr_accessible :gl_account_id, :gl_account_group_type_enum_id, :gl_account_group_id

  belongs_to :gl_account
  belongs_to :gl_account_group_type
  belongs_to :gl_account_group
end
