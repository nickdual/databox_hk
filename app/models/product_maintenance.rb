class ProductMaintenance < ActiveRecord::Base
  attr_accessible :description, :interval_meter_type_id, :interval_quantity, :interval_uom_id, :maintenance_type_id, :product_id, :repeat_count, :template_work_effort_id
  # Association
  belongs_to :product
  belongs_to :maintenance_type
  belongs_to :template_work_effort, :class_name => 'WorkEffort', :foreign_key => :template_work_effort_id
  belongs_to :interval_meter_type, :class_name => 'ProductMeterType', :foreign_key => :interval_meter_type_id
  belongs_to :interval_product_meter, :class_name => 'ProductMeter', :foreign_key => [:product_id, :interval_meter_type_id]
  has_many :asset_maintenances
end
