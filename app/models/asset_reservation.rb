class AssetReservation < ActiveRecord::Base
  attr_accessible :asset_id, :current_promised_date, :order_id, :order_item_seq_id, :priority, :promised_datetime, :quantity, :quantity_not_available, :reserve_order_id, :reserved_datetime, :sequence_num
  # Association
  belongs_to :order_header ,:class_name => 'OrderHeader', :foreign_key => :order_id
  belongs_to :order_item_seq, :class_name => 'OrderItem', :foreign_key => :order_item_seq_id
  belongs_to :asset
  has_one :item_issuance
  has_many :asset_details
end
