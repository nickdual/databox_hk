class AcctgTransEntry < ActiveRecord::Base
	attr_accessible :acctg_trans_id, :acctg_trans_entry_seq_id, :description, :voucher_ref, :party_id, :role_type_id,
	:their_party_id, :product_id, :their_product_id, :asset_id, :gl_account_type_enum_id, :gl_account_id, :organization_party_id,
	:amount, :amoun_uom_id, :orig_currency_amount, :orig_currency_uom_id, :debit_credit_flag, :due_date, :group_id, :tax_id,
	:reconcile_status_id, :settlement_term_id, :is_summary

	belongs_to :amount
	belongs_to :orig_currency
	belongs_to :acctg_trans
	belongs_to :asset
	belongs_to :party
	belongs_to :role_type
	belongs_to :gl_account_type
	belongs_to :status_item
	belongs_to :settlement_term
  belongs_to :acctg_trans_entry_reconcile_status, :foreign_key => :reconcile_status_id
	
end
