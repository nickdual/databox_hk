class BenefitType < ActiveRecord::Base
  attr_accessible :description, :employer_paid_percentage, :parent_id, :name
  # Association
  belongs_to :parent, :class_name => 'BenefitType', :foreign_key => :parent_id
  has_many :children, :class_name => 'BenefitType', :foreign_key => :parent_id
end
