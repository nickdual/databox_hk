class PaymentMethodTypeGlAccount < ActiveRecord::Base
  attr_accessible :payment_method_type_enum_id, :organization_party_id, :gl_account_id

  belongs_to :gl_account
  belongs_to :party
  belongs_to :payment_method_type
end
