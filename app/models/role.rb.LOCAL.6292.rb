class Role < ActiveRecord::Base
  attr_accessible :name
  
  has_many :roles_users, foreign_key: "role_id", dependent: :destroy
end
