class ProductDimensionType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :product_dimensions, :class_name => 'ProductDimension', :foreign_key => :dimension_type_id
end
