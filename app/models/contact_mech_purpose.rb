class ContactMechPurpose < ActiveRecord::Base
  attr_accessible :contact_mech_type_id, :description
  # Association
  has_one	:shipment_contact_mech
  has_many :return_contact_meches
  has_many :order_contact_meches
  has_many :work_effort_contact_meches
end
