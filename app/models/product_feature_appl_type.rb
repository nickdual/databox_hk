class ProductFeatureApplType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :product_feature_appls, :class_name => 'ProductFeatureAppl', :foreign_key => :appl_type_id
end
