class AssetIdentificationType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :asset_identifications, :class_name => 'AssetIdentification', :foreign_key => :identification_type_id
end
