class  PromotionCode < ActiveRecord::Base
  attr_accessible :created_by_user_login, :from_date, :last_modified_by_user_login, :product_promotion_code_id, :product_promotion_id, :require_email_or_party, :thru_date, :use_limit_per_code, :use_limit_per_customer, :user , :user_entered
  # Association
  belongs_to :product_promotion, :foreign_key => :product_promotion_id
  belongs_to :created_by_user_login, :class_name => 'User', :foreign_key =>  :created_by_user_login
  belongs_to :last_modified_by_user_login, :class_name => 'User', :foreign_key => :last_modified_by_user_login
  has_many :promotion_code_emails, :foreign_key =>  :product_promotion_code_id
  has_many :promotion_code_parties, :foreign_key => :product_promotion_code_id

  def self.simple_search(params)
    if (params['content'].blank?)
      promotion_codes = PromotionCode.where(:product_promotion_id => params[:product_promotion_id])
    else
      type = params[:type]
      content = params[:content]
      if (type == 'promotion_name')
        promotion_codes = PromotionCode.where([type + ' like ?' + ' AND product_promotion_id = ? ', '%' + content + '%', params[:product_promotion_id]])
      else
        promotion_codes = PromotionCode.where({type => content, :product_promotion_id => params[:product_promotion_id]})
      end
    end
    return promotion_codes
  end

  def self.check_id(previous=nil)
    str = ''
    str = previous.to_s
    if PromotionCode.where(:product_promotion_code_id => str).count != 0
      str = ''
    end

    return str
  end

  def self.random_string(type, length, previous=nil)
    str = ''
    if (type == "smart")
      begin
        str = ApplicationHelper.random_friendly_string('abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789', length)
      end while PromotionCode.where(:product_promotion_code_id => str).count != 0
    elsif (type == "sequence")
      if (previous == nil || previous == '')
        previous = ApplicationHelper.random_friendly_string('0123456789', length)
      end
      str = previous.to_i
      while PromotionCode.where(:product_promotion_code_id => str.to_s).count != 0
        str += 1
      end
    else
      begin
        str = ApplicationHelper.random_friendly_string('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', length)
      end while PromotionCode.where(:product_promotion_code_id => str).count != 0
    end
    return str
  end


end
