class WorkEffortPartyAvailability < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :availability_work_effort_parties, :class_name => 'WorkEffortParty', :foreign_key => :availability_id
end
