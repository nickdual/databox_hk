class TimePeriod < ActiveRecord::Base
  attr_accessible :period_name, :period_num, :period_name, :from_date,
                  :thru_date, :is_closed
  # Association
  has_one :parent, class_name: TimePeriod, primary_key: :parent_period_id, foreign_key: :id

  belongs_to :time_period_type, primary_key: :time_period_type_id
  belongs_to :party
  has_many :sales_forecasts
end
