class ProductReview < ActiveRecord::Base
  attr_accessible :posted_anonymous, :posted_datetime, :product_id, :product_rating, :product_review, :product_store_id, :status_id, :user_id
  # Association
  belongs_to :product_store
  belongs_to :product
  belongs_to :user, :class_name => 'UserAccount', :foreign_key => :user_id
  belongs_to :status, :class_name => 'ProductReviewStatus', :foreign_key => :status_id
end
