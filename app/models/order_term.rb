class OrderTerm < ActiveRecord::Base
  attr_accessible :order_id, :order_item_seq_id, :settlement_term_id
  # Composite primary keys
  # self.primary_keys = :order_id, :order_item_seq_id, :settlement_term_id
  # Association
  belongs_to :order_header, :class_name => 'OrderHeader', :foreign_key => :order_id
  belongs_to :order_item_seq, :class_name => 'OrderItem', :foreign_key => :order_item_seq_id
end
