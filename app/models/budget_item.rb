class BudgetItem < ActiveRecord::Base
	attr_accessible :budget_id, :budget_item_seq_id, :budget_item_type_enum_id, :amount, :purpose, :justification

	enum_attr :budget_item_type_enum_id, %w(BIT_REQUIRED BIT_DISCRETIONARY)

	belongs_to :budget
	belongs_to :budget_item_type, :foreign_key => :budget_item_type_enum_id
	has_one :budget_revision_impact
	has_one :budget_scenario_application
end
