class PromotionRuleConditionCategory < ActiveRecord::Base
  attr_accessible :group_id, :product_category_id, :promotion_rule_condition_id, :sub_category_status
  enum_attr :sub_category_status, %w(include exclude always_include) do
    label :include => 'Include'
    label :exclude => 'Exclude'
    label :always_include => 'Always Include'
  end
  # Association
  belongs_to :product_category, :foreign_key => :product_category_id
  belongs_to :promotion_rule_condition
  def self.product_category_id_select
    self.all.map{ |item| [item.category_name, item.id] }
  end
end
