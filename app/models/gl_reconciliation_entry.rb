class GlReconciliationEntry < ActiveRecord::Base
  attr_accessible :gl_reconciliation_id, :acctg_trans_id, :acctg_trans_entry_seq_id, :reconciled_amount

  belongs_to :gl_reconciliation
  belongs_to :acctg_trans
  belongs_to :acctg_trans_entry

end
