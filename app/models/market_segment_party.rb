class MarketSegmentParty < ActiveRecord::Base
  attr_accessible :market_segment_id, :party_id, :role_type_id
  
  belongs_to :market_segment
  belongs_to :party
  belongs_to :role_type
end
