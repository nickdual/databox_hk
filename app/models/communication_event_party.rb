# require 'composite_primary_keys'
class CommunicationEventParty < ActiveRecord::Base
  attr_accessible :communication_event_id, :contact_mech_id, :party_id, :role_type_id, :status_id
  # Composite primary keys
  self.primary_keys = :communication_event_id, :party_id, :role_type_id
  # Association 
  belongs_to :communication_event
  belongs_to :party
  belongs_to :role_type
  belongs_to :contact_mech
  belongs_to :status ,:class_name => 'CommunicationEventPartyStatus', :foreign_key => 'status_id'
end
