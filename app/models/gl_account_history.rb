class GlAccountHistory < ActiveRecord::Base
	attr_accessible :gl_account_id, :organization_party_id, :time_period_id, :posted_debits, :posted_credits, :ending_balance

	belongs_to :gl_account
	belongs_to :party
	belongs_to :time_period

end
