class SalesOpportunity < ActiveRecord::Base
  attr_accessible :currency_uom_id, :data_source_id, :description, :estimated_amount, :estimated_close_date, :estimated_probability, :marketing_campaign_id, :next_step, :opportunity_name, :opportunity_stage_id, :type_id
  # Association
  belongs_to :opportunity_stage, :class_name => 'SalesOpportunityStage', :foreign_key => :opportunity_stage_id
  belongs_to :marketing_campaign
  has_many :order_items
  has_many :sales_opportunity_parties
  has_many :sales_opportunity_work_efforts
  has_many :sales_opportunity_quotes
  has_many :sales_opportunity_competitors
  has_many :sales_opportunity_trackings
end
