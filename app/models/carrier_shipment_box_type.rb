class CarrierShipmentBoxType < ActiveRecord::Base
  attr_accessible :carrier_party_id, :oversize_code, :packaging_type_code, :shipment_box_type_id

  belongs_to :shipment_box_type
  belongs_to :party
end
