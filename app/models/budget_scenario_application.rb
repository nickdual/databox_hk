class BudgetScenarioApplication < ActiveRecord::Base
  attr_accessible :budget_scenario_id, :budget_id, :budget_item_seq_id, :amount_change, :percentage_change

  has_one :budget
  has_one :budget_scenario
  has_one :budget_item
end
