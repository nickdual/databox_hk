class Country < ActiveRecord::Base
  attr_accessible :country_code, :geonames_id, :phone, :currency_name, :currency_code, :continent, :population, :capital, :name, :iso_number, :iso_code_three_letter, :iso_code_two_letter

  has_many :cities
  has_many :divisions
end