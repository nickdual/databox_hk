class Budget < ActiveRecord::Base
	attr_accessible :budget_type_enum_id, :time_period_id, :status_id, :comments

	enum_attr :rate_type_enum_id, %w(BUDGET_CAPITAL BUDGET_OPERATING)	

	belongs_to :budget_type, :foreign_key => :budget_type_enum_id
	belongs_to :time_period
  belongs_to :budget_status
	has_one :budget_item
	has_one :budget_review
	has_one :budget_revision
	has_one :budget_revision_impact
	has_one :budget_party
	has_one :budget_scenario_application
end
