class SalesOpportunityStage < ActiveRecord::Base
  attr_accessible :default_probability, :description, :opportunity_stage_id, :sequence_num
  # Association
  has_many :sales_opportunities
end
