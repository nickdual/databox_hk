class PayrollDeductionType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :payroll_preferences
end
