class ContactList < ActiveRecord::Base
  attr_accessible :comments, :contact_list_id, :contact_list_name, :contact_list_type_enum_id, :contact_mesh_type_enum_id, :description, :is_public, :marketing_campaign_id, :opt_out_screen, :owner_party_id, :single_use, :verfity_email_from, :verify_email_screen, :verify_email_subject, :verify_email_website_id
  
  belongs_to :marketing_campaign
  belongs_to :contact_list_type_enum, :class_name => "ContactListType", :foreign_key => "contact_list_type_enum_id"
  belongs_to :owner_party, :class_name => "Party", :foreign_key => "owner_party_id"

  # Association
  has_many :communication_events

#  attr_accessible :contact_list_type_id, :contact_mech_type_id
end
