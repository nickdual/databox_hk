class WorkEffortPartyStatus < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
  # Association
  has_many :work_effort_parties
end
