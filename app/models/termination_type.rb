class TerminationType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :employments
end
