class FinancialAccountType < ActiveRecord::Base
  attr_accessible :account_code_length, :account_valid_days, :allow_auth_to_negative, :auth_valid_days, :description, :fin_account_type_id, :is_refundable, :parent_type_id, :pin_code_length, :replenish_method_enum_id, :replenish_min_balance, :replenish_threshold, :replenish_type_enum_id, :require_pin_code
  
  belongs_to :parent_type, :class_name => "FinancialAccountType", :foreign_key => "parent_type_id" 
  belongs_to :replenish_method_enum, :class_name => "FinancialAccountReplenishMethod", :foreign_key => "replenish_method_enum_id"
  belongs_to :replenish_type_enum, :class_name => "FinancialAccountReplenishType", :foreign_key => "replenish_type_enum_id"
end
