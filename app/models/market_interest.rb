class MarketInterest < ActiveRecord::Base
  attr_accessible :from_date, :market_segment_id, :product_category_id, :thru_date
  
  belongs_to :product_category
  belongs_to :market_segment
end
