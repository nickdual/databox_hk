class AssetClass < ActiveRecord::Base
  attr_accessible :description, :name, :parent_id
  # Association
  belongs_to :parent, :class_name => 'AssetClass', :foreign_key => :parent_id
  has_many :children, :class_name => 'AssetClass', :foreign_key => :parent_id
  has_many :assets, :class_name => 'Asset', :foreign_key => :class_id
end
