class ShipmentStatus < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
end
