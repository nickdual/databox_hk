class ReturnHeader < ActiveRecord::Base
  attr_accessible :billing_account_id, :currency_uom_id, :destination_facility_id, :entry_date, :fin_account_id, :from_party_id, :needs_inventory_receive, :origin_contact_mech_id, :payment_method_id, :return_id, :status_id, :supplier_rma_id, :to_party_id
  # Composite primary keys
  # self.primary_keys = :return_id, :status_id
  # Association
  has_one :shipment
  belongs_to :status ,:class_name => 'ReturnHeaderStatus', :foreign_key => :status_id
  belongs_to :from_party ,:class_name => 'Party', :foreign_key => :from_party_id
  belongs_to :to_party ,:class_name => 'Party', :foreign_key => :to_party_id
  belongs_to :destination_facility ,:class_name => 'Facility', :foreign_key => :destination_facility_id
  belongs_to :origin_contact_mech, :class_name => 'ContactMech', :foreign_key => :origin_contact_mech_id
  has_many :header_return_contact_meches , :class_name => 'ReturnContactMech', :foreign_key => :return_id
  has_many :return_items, :class_name => 'ReturnItem', :foreign_key => :return_id
end
