class SalesForecastDetail < ActiveRecord::Base
  attr_accessible :amount, :product_category_id, :product_id, :quantity, :quantity_uom_id, :sales_forecast_detail_seq_id, :sales_forecast_id
  # Composite primary keys
  # self.primary_keys = :sales_forecast_id, :sales_forecast_detail_seq_id
  # Association
  belongs_to :sales_forecast
  belongs_to :product
  belongs_to :product_category
  
end
