class GlAccountParty < ActiveRecord::Base
  attr_accessible :gl_account_id, :party_id, :role_type_id, :from_date, :thru_date

	belongs_to :gl_account
	belongs_to :party
	belongs_to :role_type
end
