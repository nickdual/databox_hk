class ProductGeoPurpose < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :product_geos
end
