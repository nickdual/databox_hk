class Payment < ActiveRecord::Base
  attr_accessible :amount, :amount_uom_id, :comments, :effective_data, :fin_account_auth_id, :fin_account_trans_id, :from_party_id, :needs_nsf_retry, :order_id, :original_currency_amount, :original_currency_uom_id, :override_gl_account_id, :payment_auth_code, :payment_id, :payment_method_id, :payment_method_type_enum_id, :payment_ref_num, :payment_type_enum_id, :present_flag, :process_attempt, :status_id, :swiped_flag, :to_party_id
  
  belongs_to :payment_type_enum, :class_name => "PaymentMethodType", :foreign_key => "payment_method_type_enum_id"
  belongs_to :payment_method
  belongs_to :order_header
  belongs_to :from, :class_name => "Party", :foreign_key => "from_party_id"
  belongs_to :to, :class_name => "Party", :foreign_key => "to_party_id"
  belongs_to :payment_status 
  belongs_to :financial_account_auth
  belongs_to :financial_account_trans
  belongs_to :override, :class_name => "GlAccount", :foreign_key => "override_gl_account_id"
end
