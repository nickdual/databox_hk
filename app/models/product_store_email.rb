class ProductStoreEmail < ActiveRecord::Base
  attr_accessible :email_template_id, :email_type_id, :from_date, :product_store_id, :thru_date
  # Composite primary keys
  self.primary_keys = :product_store_id, :email_type_id, :from_date
  # Association
  belongs_to :product_store
  belongs_to :email_type, :class_name => 'ProductStoreEmailType', :foreign_key => :email_type_id
end
