class WorkEffortContactMech < ActiveRecord::Base
  attr_accessible :comments, :contact_mech_id, :contact_mech_purpose_id, :extension, :from_date, :thru_date, :work_effort_id
  # Composite primary keys
  # self.primary_keys = :work_effort_id, :contact_mech_id, :contact_mech_purpose_id, :from_date
  # Association
  belongs_to :work_effort 
  belongs_to :contact_mech
  belongs_to :contact_mech_purpose
end
