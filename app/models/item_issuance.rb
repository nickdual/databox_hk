class ItemIssuance < ActiveRecord::Base
  attr_accessible :asset_id, :asset_maintenance_id, :asset_reservation_id, :cancel_quantity, :issued_datetime, :order_id, :order_item_seq_id, :quantity, :shipment_id, :shipment_item_seq_id
  # Association
  belongs_to :asset
  belongs_to :asset_reservation
  belongs_to :order_item
  belongs_to :shipment_item
  belongs_to :asset_maintenance

  has_one :item_issuance_party
  has_many :order_item_billings
  has_many :asset_details
end
