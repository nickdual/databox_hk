class EmplPositionStatus < ActiveRecord::Base
  attr_accessible :description, :status_id
  # Association
  has_many :empl_positions
end
