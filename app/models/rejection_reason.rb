class RejectionReason < ActiveRecord::Base
  attr_accessible :description, :name

  has_one :shipment_receipt
end
