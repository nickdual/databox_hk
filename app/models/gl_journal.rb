class GlJournal < ActiveRecord::Base
  attr_accessible :gl_journal_name, :organization_party_id, :is_posted, :posted_date

  belongs_to :party
end
