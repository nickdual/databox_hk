class  PromotionRuleCondition < ActiveRecord::Base
  attr_accessible :operator_id, :other, :promotion_rule_condition_name, :promotion_rule_id, :shipment_method_id, :value
  enum_attr :promotion_rule_condition_name, %w(PPIP_ORDER_TOTAL PPIP_PRODUCT_TOTAL PPIP_PRODUCT_AMOUNT PPIP_PRODUCT_QUANT PPIP_NEW_ACCT PPIP_PARTY_ID PPIP_PARTY_GRP_MEM PPIP_PARTY_CLASS PPIP_ROLE_TYPE PPIP_ORST_HIST PPIP_RECURRENCE PPIP_ORST_YEAR PPIP_ORST_LAST_YEAR PPIP_LPMUP_AMT PPIP_LPMUP_PER PPIP_ORDER_SHIPTOTAL PPIP_SERVICE) do
    label :PPIP_ORDER_TOTAL => 'Cart Sub-total'
    label :PPIP_PRODUCT_TOTAL => 'Total Amount of Product'
    label :PPIP_PRODUCT_AMOUNT =>  'X Amount of Product'
    label :PPIP_PRODUCT_QUANT => 'X Quantity of Product'
    label :PPIP_NEW_ACCT => 'Account Days Since Created'
    label :PPIP_PARTY_ID =>  'Party'
    label :PPIP_PARTY_GRP_MEM =>  'Party Group Member'
    label :PPIP_PARTY_CLASS =>  'Party Classification'
    label :PPIP_ROLE_TYPE => 'Role Type'
    label :PPIP_ORST_HIST =>  'Order sub-total X in last Y Months'
    label :PPIP_RECURRENCE => 'Promotion Recurrence'
    label :PPIP_ORST_YEAR => 'Order sub-total X since beginning of current year'
    label :PPIP_ORST_LAST_YEAR => 'Order sub-total X last year'
    label :PPIP_LPMUP_AMT => 'List Price minus Unit Price (Amount)'
    label :PPIP_LPMUP_PER => 'List Price minus Unit Price (Percent)'
    label :PPIP_ORDER_SHIPTOTAL => 'Shipping Total'
    label :PPIP_SERVICE =>  'Call Service'

  end
  validates :value, :numericality => true
  validates :value, :presence => true


  validates :other, :numericality => true
  validates :other, :presence => true

  # Association
  belongs_to :operator
  belongs_to :promotion_rule
  belongs_to :shipment_method
  has_many :promotion_rule_condition_categories
  has_many :promotion_rule_condition_products

  def self.product_category_id_select
    self.all.map{ |item| [item.category_name, item.product_category_id] }
  end
end
