class CostComponent < ActiveRecord::Base
  attr_accessible :asset_id, :cost, :cost_component_calc_id, :cost_component_type_id, :cost_uom_id, :from_date, :geo_id, :party_id, :product_id, :thru_date, :work_effort_id
  # Association
  belongs_to :cost_component_type
  belongs_to :product
  belongs_to :party
  belongs_to :work_effort
  belongs_to :asset
  belongs_to :cost_component_calc
end
