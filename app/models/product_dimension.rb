class ProductDimension < ActiveRecord::Base
  attr_accessible :dimension_type_id, :product_id, :value, :value_uom_id
  # set_primary_key :product_id
  # Association
  belongs_to :dimension_type, :class_name => 'ProductDimensionType', :foreign_key => :dimension_type_id
end
