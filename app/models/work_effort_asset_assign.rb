class WorkEffortAssetAssign < ActiveRecord::Base
  attr_accessible :allocated_cost, :asset_id, :comments, :from_date, :status_id, :thru_date, :work_effort_id
  # Composite primary keys
  # self.primary_keys = :work_effort_id, :asset_id, :from_date
  # Association
  belongs_to :work_effort
  belongs_to :asset
  belongs_to :status, :class_name => 'WorkEffortAssetAssignStatus', :foreign_key => :status_id
end
