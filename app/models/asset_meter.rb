class AssetMeter < ActiveRecord::Base
  attr_accessible :asset_id, :asset_maintenance_id, :meter_value, :product_meter_type_id, :reading_date, :reading_reason_id, :work_effort_id
  # Composite primary keys
  self.primary_keys = :asset_id, :product_meter_type_id, :reading_date
  # Association
  belongs_to :asset
  belongs_to :asset_maintenance
  belongs_to :product_meter_type
end
