class PerformanceReviewItemType < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :performance_review_item
end
