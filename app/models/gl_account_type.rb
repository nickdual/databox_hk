class GlAccountType < ActiveRecord::Base
  attr_accessible :description, :name, :parent_id
  belongs_to :parent, :class_name => 'GlAccountType', :foreign_key => :parent_id
  has_many :children, :class_name => 'GlAccountType', :foreign_key => :parent_id
  has_many :gl_accounts
end
