class AcctgTransEntryReconcileStatus < ActiveRecord::Base
  attr_accessible :description, :sequence_num, :status_id
  has_many :acctg_trans_entries
end
