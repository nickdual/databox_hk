class AccommodationMap < ActiveRecord::Base
  attr_accessible :accommodation_class_id, :accommodation_map_type_id, :asset_id, :number_of_spaces
  # Association
  belongs_to :asset
  belongs_to :accommodation_class
  belongs_to :accommodation_map_type
end
