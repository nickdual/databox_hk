class RequestCommEvent < ActiveRecord::Base
  attr_accessible :communication_event_id, :request_id
  
  belongs_to :request
  belongs_to :communication_event
end
