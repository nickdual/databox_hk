class PartyAcctgPreference < ActiveRecord::Base
	attr_accessible :party_id, :fiscal_year_start_month, :fiscal_year_start_day, :tax_form_enum_id, :cogs_method_enum_id, 
	:base_currency_uom_id, :invoice_sequence_enumId, :invoice_id_prefix, :last_invoice_number, :last_invoice_restart_date,
	:use_invoice_id_for_returns, :quote_sequence_enum_id, :quote_id_prefix, :last_quote_number, :order_sequence_enum_id,
	:refund_payment_method_id, :error_gl_journal_id, :last_order_number, :order_id_prefix

	enum_attr :tax_form_enum_id, %w(US_IRS_1120 US_IRS_1120S US_IRS_1065 US_IRS_990 US_IRS_990PF US_IRS_990T US_IRS_1040)
	enum_attr :cogs_method_enum_id, %w(COGS_LIFO COGS_FIFO COGS_AVG_COST COGS_INV_COST)
	enum_attr :invoice_sequence_enumId, %w(INVSQ_STANDARD INVSQ_ENF_SEQ INVSQ_RESTARTYR QTESQ_STANDARD QTESQ_ENF_SEQ ODRSQ_STANDARD ODRSQ_ENF_SEQ)

	belongs_to :party
	belongs_to :tax_form, :foreign_key => :tax_form_enum_id
	belongs_to :cogs_method, :foreign_key => :cogs_method_enum_id
	belongs_to :payment_method
	belongs_to :gl_journal
	belongs_to :invoice_sequence, :foreign_key => :invoice_sequence_enumId
	belongs_to :quote_sequence, :foreign_key => :quote_sequence_enum_id
	belongs_to :order_sequence, :foreign_key => :order_sequence_enum_id
end
