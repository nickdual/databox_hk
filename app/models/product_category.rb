class ProductCategory < ActiveRecord::Base
  attr_accessible :category_name, :description, :product_category_type_id,:product_category_id
  # Association
  has_one :product_category_gl_account
  belongs_to :product_category_type
  has_many :party_needs
  has_many :sales_forecast_details
  has_many :product_store_categories
  has_many :product_category_feat_grp_appls
  has_many :product_category_contents
  has_many :product_category_parties
  has_many :product_category_members
  has_many :product_category_rollups
  has_many :children_product_category_rollups, :class_name => 'ProductCategoryRollup', :foreign_key => :parent_product_category_id
  has_many :promotion_categories, :foreign_key => :product_category_id
  has_many :promotion_rule_condition_categories, :foreign_key => :product_category_id
  has_many :promotion_rule_action_categories, :foreign_key => :product_category_id

  def self.product_category_id_select
    self.all.map{ |item| [item.category_name, item.id] }
  end

end
