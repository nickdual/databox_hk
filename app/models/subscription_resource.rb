class SubscriptionResource < ActiveRecord::Base
  attr_accessible :content_location, :description, :parent_resource_id
  # Association
  belongs_to :parent, :class_name => 'SubscriptionResource', :foreign_key => :parent_resource_id
  has_many :children, :class_name => 'SubscriptionResource', :foreign_key => :parent_resource_id
  has_many :product_subscription_resources
  has_many :subscriptions
end
