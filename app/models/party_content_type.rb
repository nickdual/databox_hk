class PartyContentType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :party_contents
end
