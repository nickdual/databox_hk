class RequestContent < ActiveRecord::Base
  attr_accessible :content_location, :from_date, :request_id, :thru_date
  
  belongs_to :request
end
