class ProductConfigItemContentType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association 
  has_many :product_config_item_contents, :class_name => 'ProductConfigItemContent', :foreign_key => :item_content_type_id
end
