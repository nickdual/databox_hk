class InventoryVarianceReason < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_one :variance_reason_gl_account
  has_many :asset_details, :class_name => 'AssetDetail', :foreign_key => :variance_reason_id
end
