class InvoiceTerm < ActiveRecord::Base
  attr_accessible :invoice_id, :invoice_item_seq_id, :settlement_term_id
  
  belongs_to :invoice, :class_name => "InvoiceItem", :foreign_key => "invoice_id"
  belongs_to :invoice_item_seq, :class_name => "InvoiceItem", :foreign_key => "invoice_item_seq_id"
  belongs_to :settlement_term
end
