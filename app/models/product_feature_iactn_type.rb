class ProductFeatureIactnType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :product_feature_iactns, :class_name => 'ProductFeatureIactn', :foreign_key => :iactn_type_id
  
end
