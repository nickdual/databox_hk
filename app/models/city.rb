class City < ActiveRecord::Base
  attr_accessible :country_id, :division_id, :geonames_id, :name, :ascii_name, :alternate_name, :latitude, :longitude, :country_iso_code_two_letters, :admin_1_code, :admin_2_code, :admin_3_code, :admin_4_code, :population, :geonames_timezone_id

  belongs_to :country
  belongs_to :division
end