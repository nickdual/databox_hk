class RequestItem < ActiveRecord::Base
  attr_accessible :description, :maximum_amount, :priority, :product_id, :quantity, :request_id, :request_item_seq_id, :request_resolution_enum_id, :required_by_date, :selected_amount, :sequence_num, :status_id, :story
  # Composite primary keys 
  self.primary_keys = :request_id, :request_item_seq_id
  belongs_to :request
  # belongs_to :status
  belongs_to :product
  has_many :request_item_work_efforts, :foreign_key => [:request_id, :request_item_seq_id]
end
