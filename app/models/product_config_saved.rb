class ProductConfigSaved < ActiveRecord::Base
  attr_accessible :configurable_product_id
  # Association
  belongs_to :configurable, :class_name => 'Product', :foreign_key => :configurable_product_id
  has_many :order_items
end
