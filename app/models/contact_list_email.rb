class ContactListEmail < ActiveRecord::Base
  attr_accessible :contact_list_id, :email_templated_id, :email_type_enum_id, :from_date, :thru_date
  
  belongs_to :contact_list
  belongs_to :email_type_enum, :class_name => "ContactListEmailType", :foreign_key => "email_type_enum_id"
  
#  attr_accessible :contact_list_id, :email_template_id, :email_type_id

end
