class WorkEffortNote < ActiveRecord::Base
  attr_accessible :internal_note, :note_date, :note_text, :work_effort_id
  # Composite primary keys
  self.primary_keys = :work_effort_id, :note_date
  # Association
  belongs_to :work_effort
end
