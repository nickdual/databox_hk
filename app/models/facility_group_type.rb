class FacilityGroupType < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :facility_groups
end
