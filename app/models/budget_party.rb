class BudgetParty < ActiveRecord::Base
  attr_accessible :budget_id, :party_id, :role_type_id
  
  belongs_to :budget
  belongs_to :party
  belongs_to :role_type
end
