class FacilityType < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :facilities
end
