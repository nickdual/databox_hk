class ContactListEmailType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :contact_list_emails
end
