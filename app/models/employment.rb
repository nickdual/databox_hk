class Employment < ActiveRecord::Base
  attr_accessible :empl_position_id, :employee_party_id, :employer_party_id, :from_date, :termination_reason_enum_id, :termination_type_enum_id, :thru_date
  # Association
  belongs_to :termination_type, :foreign_key => :termination_type_enum_id
  has_many :agreement_item_employments
  
end
