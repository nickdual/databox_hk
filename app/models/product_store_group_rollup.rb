class ProductStoreGroupRollup < ActiveRecord::Base
  attr_accessible :from_date, :parent_group_id, :product_store_group_id, :sequence_num, :thru_date
  # Composite primary keys
  # self.primary_keys = :product_store_group_id, :parent_group_id, :from_date
  # Association
  belongs_to :product_store_group
  belongs_to :parent_group, :class_name => 'ProductStoreGroup', :foreign_key => :parent_group_id
  
end
