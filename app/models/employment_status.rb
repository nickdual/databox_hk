class EmploymentStatus < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :people
end
