class InvoiceParty < ActiveRecord::Base
  attr_accessible :datetime_performed, :invoice_id, :party_id, :percentage, :role_type_id
  
  belongs_to :invoice
  belongs_to :party
  belongs_to :role_type
end
