class ProductCategoryParty < ActiveRecord::Base
  attr_accessible :comments, :from_date, :party_id, :product_category_id, :role_type_id, :thru_date
  # Composite primary keys
  self.primary_keys = :product_category_id, :party_id, :role_type_id, :from_date
  # Association
  belongs_to :product_category
  belongs_to :party
  belongs_to :role_type
end
