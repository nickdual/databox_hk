class WorkEffortDeliverableProd < ActiveRecord::Base
  attr_accessible :deliverable_id, :work_effort_id
  # Composite primary keys
  # self.primary_keys = :work_effort_id, :deliverable_id
  # Association
  belongs_to :work_effort
  belongs_to :deliverable
end
