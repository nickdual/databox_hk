class CommunicationEventType < ActiveRecord::Base
  attr_accessible :contact_mech_type_id, :description, :parent_type_id, :name
  # Association
  has_many :children, :class_name => 'CommunicationEventType', :foreign_key => 'parent_type_id'
  belongs_to :parent, :class_name => 'CommunicationEventType', :foreign_key => 'parent_type_id'
  belongs_to :contact_mech_type, :class_name => 'ContactMechanismType', :foreign_key => 'contact_mech_type_id'
end
