class Product < ActiveRecord::Base
  #has_paper_trail
  attr_accessible :amount_uom_type_id, :bill_of_material_level, :charge_shipping, :comments, :default_shipment_box_type_id, :description, :fixed_amount, :in_shipping_box, :origin_geo_id, :product_name, :product_type_id, :require_amount, :require_inventory, :requirement_method_id, :returnable, :sales_disc_when_not_avail, :sales_discontinuation_date, :sales_introduction_date, :support_discontinuation_date, :supplier_id, :sku
  # Association
  has_one :product_gl_account
  has_one :shipment_receipt
  has_one :shipment_item
  has_one :shipment_package_content
  
  belongs_to :product_type
  belongs_to :requirement_methods
  belongs_to :default_shipment_box_type, :class_name => 'ShipmentBoxType', :foreign_key => :default_shipment_box_type_id
  
  has_many :communication_event_products
  has_many :return_items
  has_many :order_items
  has_many :asset_work_effort_asset_neededs, :class_name => 'WorkEffortAssetNeeded', :foreign_key => :asset_product_id
  has_many :work_effort_products
  has_many :party_needs
  has_many :sales_forecast_details
  has_many :supplier_products
  has_many :product_subscription_resources
  has_many :subscriptions
  has_many :product_assocs
  has_many :to_product_assocs, :class_name => 'ProductAssoc', :foreign_key => :to_product_id
  has_many :product_calculated_infos
  has_many :product_contents
  has_many :product_geos
  has_many :product_identifications
  has_many :product_parties
  has_many :product_prices
  has_many :product_reviews
  has_many :product_meters
  has_many :product_maintenances
  has_many :cost_components
  has_many :product_cost_component_calcs
  has_many :product_average_costs
  has_many :product_feature_appls
  has_many :product_config_item_appls
  has_many :product_config_option_products
  has_many :configurable_product_config_saveds, :class_name => 'ProductConfigSaved', :foreign_key => :configurable_product_id
  has_many :product_config_saved_options
  has_many :product_category_members
  has_many :instance_of_assets, :class_name => 'Asset', :foreign_key => :instance_of_product_id
  has_many :asset_products
  has_many :promotion_products
  has_many :promotion_rule_condition_products
  has_many :promotion_rule_action_products
end
