class ReturnItemBilling < ActiveRecord::Base
  attr_accessible :amount, :invoice_id, :invoice_item_seq_id, :quantity, :return_id, :return_item_seq_id, :shipment_receipt_id
  # Composite primary key 
  # self.primary_keys = :return_id, :return_item_seq_id, :invoice_id, :invoice_item_seq_id
  # Association
  belongs_to :return_item , :class_name => 'ReturnItem', :foreign_key => :return_id
  belongs_to :return_item_seq, :class_name => 'ReturnItem', :foreign_key => :return_item_seq_id
  belongs_to :shipment_receipt
end
