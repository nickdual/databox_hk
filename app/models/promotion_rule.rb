class PromotionRule < ActiveRecord::Base
  attr_accessible :name, :product_promotion_id
  # Association
  validates :name, :presence => true
  validates :name, :uniqueness => true
  belongs_to :product_promotion
  has_many :promotion_rule_conditions
  has_many :promotion_rule_actions
end
