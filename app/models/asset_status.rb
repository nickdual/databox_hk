class AssetStatus < ActiveRecord::Base
  attr_accessible :description, :sequence_num, :status_id
  # Association
  has_many :assets, :class_name => 'Asset', :foreign_key => :status_id
  has_many :status_valids, :class_name => 'AssetStatusValid', :foreign_key => :status_id
  has_many :to_status_valids, :class_name => 'AssetStatusValid', :foreign_key => :to_status_id
end
