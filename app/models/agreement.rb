class Agreement < ActiveRecord::Base
  attr_accessible :agreement_date, :agreement_type_id, :description, :from_date, :from_party_id, :from_role_type_id, :text_data, :thru_date, :to_party_id, :to_role_type_id
  # Association
  belongs_to :from_party, :class_name => 'Party', :foreign_key => :from_party_id
  belongs_to :to_party, :class_name => 'Party', :foreign_key => :to_party_id
  belongs_to :from_role_type, :class_name => 'RoleType', :foreign_key => :from_role_type_id
  belongs_to :to_role_type, :class_name => 'RoleType', :foreign_key => :to_role_type_id
  belongs_to :agreement_type
  belongs_to :agreement_addendums
  has_many :agreement_parties
  has_many :agreement_items
  has_many :agreement_terms
end
