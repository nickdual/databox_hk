class TimesheetStatus < ActiveRecord::Base
  attr_accessible :description, :sequence_num, :status_id
  # Association
  has_many :timesheets, :class_name => 'Timesheet', :foreign_key => :status_id
  has_many :status_valids, :class_name => 'TimesheetStatusValid', :foreign_key => :status_id
  has_many :to_status_valids, :class_name => 'TimesheetStatusValid', :foreign_key => :to_status_id
end
