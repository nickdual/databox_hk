class CarrierShipmentMethod < ActiveRecord::Base
  attr_accessible :carrier_party_id, :carrier_service_code, :sequence_num, :shipment_method_id

  # enum_attr :shipment_method_id, %w(NO_SHIPPING)

  belongs_to :party
  belongs_to :shipment_method
end
