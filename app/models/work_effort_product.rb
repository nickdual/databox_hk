class WorkEffortProduct < ActiveRecord::Base
  attr_accessible :estimated_cost, :estimated_quantity, :from_date, :product_id, :status_id, :thru_date, :type_id, :work_effort_id
  # Composite primary keys
  # self.primary_keys = :work_effort_id, :product_id, :from_date
  # Association
  belongs_to :work_effort
  belongs_to :product
  belongs_to :type, :class_name => 'WorkEffortProductType', :foreign_key => :type_id
  belongs_to :status ,:class_name => 'WorkEffortProductStatus', :foreign_key => :status_id
end
