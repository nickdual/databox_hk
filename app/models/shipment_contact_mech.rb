class ShipmentContactMech < ActiveRecord::Base
  attr_accessible :contact_mech_id, :contact_mech_purpose_id, :shipment_id

  belongs_to :shipment
  belongs_to :contact_mech_purpose
  belongs_to :contact_mech
end
