class WorkEffortPartyDelegateReason < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :delegate_reason_work_effort_parties, :class_name => 'WorkEffortParty', :foreign_key => :delegate_reason_id
end
