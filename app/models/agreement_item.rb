class AgreementItem < ActiveRecord::Base
  attr_accessible :agreement_id, :agreement_item_seq_id, :agreement_item_type_id, :currency_uom_id, :from_date, :item_text, :thru_date
  # Composite primary keys
  self.primary_keys = :agreement_id, :agreement_item_seq_id
  # Association
  belongs_to :agreement
  belongs_to :agreement_item_type
  has_many :agreement_item_employments, :foreign_key => [:agreement_id, :agreement_item_seq_id]
  has_many :agreement_addendums, :foreign_key => [:agreement_id, :agreement_item_seq_id]
  has_many :agreement_item_geos,:foreign_key => [:agreement_id, :agreement_item_seq_id]
  has_many :agreement_item_parties, :foreign_key => [:agreement_id, :agreement_item_seq_id]
  has_many :agreement_item_work_efforts, :foreign_key => [:agreement_id, :agreement_item_seq_id]
  has_many :agreement_terms, :foreign_key => [:agreement_id, :agreement_item_seq_id]
end
