class ProductCategoryContent < ActiveRecord::Base
  attr_accessible :category_content_type_id, :content_location, :from_date, :product_category_id, :thru_date
  # Composite primary keys
  self.primary_keys = :product_category_id, :content_location, :category_content_type_id, :from_date
  # Association
  belongs_to :product_category
  belongs_to :category_content_type, :class_name => 'ProductCategoryContentType', :foreign_key => :category_content_type_id
end
