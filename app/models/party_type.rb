class PartyType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association 
  has_many :parties
end
