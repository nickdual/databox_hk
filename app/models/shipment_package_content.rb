class ShipmentPackageContent < ActiveRecord::Base
  attr_accessible :quantity, :shipment_id, :shipment_item_seq_id, :shipment_package_seq_id, :sub_product_id, :sub_product_quantity

  belongs_to :shipment_package
  belongs_to :shipment_item
  belongs_to :product
end
