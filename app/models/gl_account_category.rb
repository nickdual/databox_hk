class GlAccountCategory < ActiveRecord::Base
	attr_accessible :category_type_enum_id, :description

	enum_attr :category_type_enum_id, %w(COST_CENTER)

	belongs_to :gl_account_category_type, :foregin_key => :category_type_enum_id

	has_one :gl_zacount_category_member
end
