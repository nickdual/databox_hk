class OrderNote < ActiveRecord::Base
  attr_accessible :internal_note, :note_date, :note_text, :order_id
  # Composite primary keys
  # self.primary_keys = :order_id, :note_date
  # Association
  belongs_to :order_header, :class_name => 'OrderHeader', :foreign_key => :order_id
end
