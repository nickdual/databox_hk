class ProductAverageCost < ActiveRecord::Base
  attr_accessible :average_cost, :average_cost_type_id, :facility_id, :from_date, :organization_party_id, :product_id, :thru_date
  # Association
  belongs_to :organization, :class_name => 'Party', :foreign_key => :organization_party_id
  belongs_to :product
  belongs_to :facility
end
