class FinancialAccount < ActiveRecord::Base
  attr_accessible :actual_balance, :availabe_balance, :balance_uom_id, :fin_account_code, :fin_account_id, :fin_account_name, :fin_account_pin, :fin_account_type_id, :from_date, :is_refundable, :organization_party_id, :owner_party_id, :post_to_gl_account_id, :replenish_level, :replenish_payment_id, :status_id, :thru_date
  
  belongs_to :status, :class_name => "FinancialAccountStatus"
  belongs_to :organization, :class_name => "Party", :foreign_key => "organization_party_id"
  belongs_to :owner, :class_name => "Party", :foreign_key => "owner_party_id"
  belongs_to :post_to, :class_name => "GlAccount", :foreign_key => "post_to_gl_account_id"
  belongs_to :replenish, :class_name => "PaymentMethod", :foreign_key => "replenish_payment_id"
end
