class RequestCategory < ActiveRecord::Base
  attr_accessible :description, :parent_category_id, :request_category_id, :responsible_party_id
  
  belongs_to :parent_category, :class_name => "RequestCategory", :foreign_key => "parent_category_id"
  belongs_to :responsible_party, :class_name => "Party", :foreign_key => "responsible_party_id"
end
