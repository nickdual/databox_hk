class AgreementItemType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  belongs_to :agreement_items
end
