class Lot < ActiveRecord::Base
  attr_accessible :creation_date, :expiration_date, :quantity
  # Association
  has_many :assets
end
