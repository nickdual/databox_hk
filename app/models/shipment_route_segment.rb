class ShipmentRouteSegment < ActiveRecord::Base
  attr_accessible :actual_arrival_date, :actual_cost, :actual_other_cost, :actual_service_cost, :actual_start_date, :actual_transport_cost, :billing_weight, :billing_weight_uom_id, :carrier_delivery_zone, :carrier_party_id, :carrier_restriction_codes, :carrier_restriction_desc, :cost_uom_id, :delivery_id, :dest_contact_mech_id, :dest_facility_id, :dest_telecom_number_id, :estimated_arrival_date, :estimated_start_date, :home_delivery_date, :home_delivery_type, :origin_contact_mech_id, :origin_facility_id, :origin_telecom_number_id, :shipment_id, :shipment_method_id, :status_id, :third_party_account_number, :third_party_country_geo_code, :third_party_postal_code, :tracking_digest, :tracking_id_number, :ups_high_value_report, :billing_weight_id

  enum_attr :shipment_method_id, %w(SHRSCS_NOT_STARTED SHRSCS_CONFIRMED SHRSCS_ACCEPTED SHRSCS_VOIDED)

  has_one :shipment_package_route_seg

  belongs_to :shipment
  belongs_to :delivery
  belongs_to :party
  belongs_to :shipment_method
  belongs_to :facility
  belongs_to :postal_address
  belongs_to :telecom_number
  belongs_to :billing_weight
end
