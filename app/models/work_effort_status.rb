class WorkEffortStatus < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
  # Association
  has_many :work_efforts, :class_name => 'WorkEffort', :foreign_key => :status_id
  has_many :status_valids, :class_name => 'WorkEffortStatusValid', :foreign_key => :status_id
  has_many :to_status_valids, :class_name => 'WorkEffortStatusValid', :foreign_key => :to_status_id
end
