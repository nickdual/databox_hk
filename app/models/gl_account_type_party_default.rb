class GlAccountTypePartyDefault < ActiveRecord::Base
  attr_accessible :organization_party_id, :party_id, :role_type_id, :gl_account_type_enum_id, :gl_account_id

  belongs_to :party
  belongs_to :role_type
  belongs_to :gl_account_type
  belongs_to :gl_account

end
