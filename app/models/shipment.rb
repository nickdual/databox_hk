class Shipment < ActiveRecord::Base
  attr_accessible :additional_shipping_charge, :addtl_shipping_charge_desc, :cost_uom_id, :destination_contact_mech_id, :destination_facility_id, :destination_telecom_number_id, :estimated_arrival_date, :estimated_arrival_work_eff_id, :estimated_ready_date, :estimated_ship_cost, :estimated_ship_date, :estimated_ship_work_eff_id, :from_party_id, :handling_instructions, :latest_cancel_date, :origin_contact_mech_id, :origin_facility_id, :origin_telecom_number_id, :picklist_bin_id, :primary_order_id, :primary_order_part_seq_id, :primary_return_id, :shipment_type_id, :status_id, :to_party_id

  enum_attr :shipment_type_id, %w(INCOMING_SHIPMENT OUTGOING_SHIPMENT SALES_RETURN SALES_SHIPMENT PURCHASE_SHIPMENT PURCHASE_RETURN DROP_SHIPMENT TRANSFER)
  enum_attr :status_id, %w(SHIPMENT_INPUT SHIPMENT_SCHEDULED SHIPMENT_PICKED SHIPMENT_PACKED SHIPMENT_SHIPPED SHIPMENT_DELIVERED SHIPMENT_CANCELLED)

  has_one :acctg_trans
  has_one	:shipment_contact_mech
  has_one :shipment_item
  has_one :shipment_package, :dependent => :destroy
  has_one :shipment_route_segment
  belongs_to :shipment_type
  belongs_to :picklist_bin
  belongs_to :work_effort
  belongs_to :cost
  belongs_to :facility
  belongs_to :origin_contact_mech, :class_name => 'PostalAddress', :foreign_key => :origin_contact_mech_id
  belongs_to :destination_contact_mech, :class_name => 'PostalAddress', :foreign_key => :destination_contact_mech_id
  belongs_to :telecom_number
  belongs_to :order_header
  belongs_to :return_header
  belongs_to :order_part
  belongs_to :party


  # Association
  has_one :acctg_trans
  has_many :order_shipments

  def self.simple_search(params)
    if (params['content'].blank?)
      sum = Shipment.count
      shipments = Shipment.paginate(:page => params['page'], :per_page => params[:promo_page])
      results = []
      i = 0
      shipments.each do |shipment|
        results[i] = {:id => shipment.id}
        package = shipment.shipment_package
        results[i]['box_length'] = package['box_length']
        results[i]['box_width'] = package['box_width']
        results[i]['box_height'] = package['box_height']
        results[i]['weight'] = package['weight']
        starting_point = shipment.origin_contact_mech
        results[i]['starting_point'] = {:country_code => starting_point.access_code, :city_name => starting_point.city, :postcode => starting_point.postal_code}
        endding_point = shipment.destination_contact_mech
        results[i]['ending_point'] = {:country_code => endding_point.access_code, :city_name => endding_point.city, :postcode => endding_point.postal_code}
        i += 1
      end
    else
      type = params[:type]
      content = params[:content]
      sum = Shipment.where({type => content}).count
      shipments = Shipment.where({type => content}).paginate(:page => params['page'], :per_page => params[:promo_page])
      results = []
      i = 0
      shipments.each do |shipment|
        results[i] = {:id => shipment.id}
        package = shipment.shipment_package
        results[i]['box_length'] = package['box_length']
        results[i]['box_width'] = package['box_width']
        results[i]['box_height'] = package['box_height']
        results[i]['weight'] = package['weight']
        starting_point = shipment.origin_contact_mech
        results[i]['starting_point'] = {:country_code => starting_point.access_code, :city_name => starting_point.city, :postcode => starting_point.postal_code}
        endding_point = shipment.destination_contact_mech
        results[i]['ending_point'] = {:country_code => endding_point.access_code, :city_name => endding_point.city, :postcode => endding_point.postal_code}
        i += 1
      end
    end
    return {:count => sum, :data => results}
  end
end
