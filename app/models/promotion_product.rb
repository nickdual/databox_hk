class PromotionProduct < ActiveRecord::Base
  attr_accessible :product_id, :sub_category_status ,:product_promotion_id
  enum_attr :sub_category_status, %w(include exclude always_include) do
    label :include => 'Include'
    label :exclude => 'Exclude'
    label :always_include => 'Always Include'
  end
  # Association
  belongs_to :product
  belongs_to :product_promotion, :foreign_key => :product_promotion_id
  validates :product_id, :presence => true
end
