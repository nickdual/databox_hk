class ProductConfigOption < ActiveRecord::Base
  attr_accessible :config_item_id, :config_option_name, :config_option_seq_id, :description, :sequence_num
  # Composite primary keys
  self.primary_keys = :config_item_id, :config_option_seq_id
  # Association
  belongs_to :config_item, :class_name => 'ProductConfigItem', :foreign_key => :config_item_id
  has_many :product_config_option_iactns, :foreign_key => [:config_item_id, :config_option_seq_id]
  has_many :to_product_config_option_iactns, :class_name => 'ProductConfigOptionIactn', :foreign_key => [:to_config_item_id, :to_config_option_seq_id]
  has_many :product_config_option_products, :foreign_key => [:config_item_id, :config_option_seq_id]
  has_many :product_config_saved_options, :foreign_key => [:config_item_id, :config_option_seq_id]
end
