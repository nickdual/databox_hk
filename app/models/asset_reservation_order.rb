class AssetReservationOrder < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :product_stores, :class_name => 'ProductStore', :foreign_key => :reservation_order_id
end
