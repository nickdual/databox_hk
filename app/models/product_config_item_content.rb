class ProductConfigItemContent < ActiveRecord::Base
  attr_accessible :config_item_id, :content_location, :from_date, :item_content_type_id, :thru_date
  # Composite primary keys
  self.primary_keys = :config_item_id, :content_location, :from_date
  # Association
  belongs_to :config_item, :class_name => 'ProductConfigItem', :foreign_key => :config_item_id
  belongs_to :item_content_type, :class_name => 'ProductConfigItemContentType', :foreign_key => :item_content_type_id
end
