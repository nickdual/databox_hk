class PerformanceReviewItem < ActiveRecord::Base
  attr_accessible :comments, :performance_review_id, :performance_review_item_seq_id, :review_item_type_enum_id, :review_rating_enum_id
  # Association
  belongs_to :performance_review_item_type, :foreign_key => :review_item_type_enum_id
  belongs_to :performance_review_rating, :foreign_key => :review_rating_enum_id
end
