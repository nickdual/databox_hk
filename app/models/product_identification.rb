class ProductIdentification < ActiveRecord::Base
  attr_accessible :id_value, :product_id, :product_id_type_id
  # Composite primary keys
  # self.primary_keys = :product_id, :product_id_type_id
  # Association
  belongs_to :product_id_type, :class_name => 'ProductIdentificationType', :foreign_key => :product_id_type_id
  belongs_to :product
end
