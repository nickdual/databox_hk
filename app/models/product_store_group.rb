class ProductStoreGroup < ActiveRecord::Base
  attr_accessible :description, :product_store_group_type_id
  # Association
  has_many :product_store_group_members
  has_many :product_store_group_parties
  has_many :product_store_group_rollups
  has_many :children_rollups, :class_name => 'ProductStoreGroupRollup', :foreign_key => :parent_group_id
end
