class SalesOpportunityCompetitor < ActiveRecord::Base
  attr_accessible :competitor_party_id, :position_id, :sales_opportunity_id, :strengths, :weaknesses
  # Composite primary keys
  # self.primary_keys = :sales_opportunity_id, :competitor_party_id
  # Association
  belongs_to :sales_opportunity
  
end
