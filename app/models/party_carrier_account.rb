class PartyCarrierAccount < ActiveRecord::Base
  attr_accessible :account_number, :carrier_party_id, :from_date, :party_id, :thru_date

  belongs_to :party
  
end
