class RateType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :rate_amounts
end
