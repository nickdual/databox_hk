class SubscriptionType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :subscriptions
end
