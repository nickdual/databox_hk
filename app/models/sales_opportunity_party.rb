class SalesOpportunityParty < ActiveRecord::Base
  attr_accessible :from_date, :party_id, :role_type_id, :sales_opportunity_id, :thru_date
  # Composite primary keys
  # self.primary_keys = :sales_opportunity_id, :party_id, :role_type_id, :from_date
  # Association
  belongs_to :sales_opportunity
  belongs_to :party
  belongs_to :role_type
end
