class CostComponentType < ActiveRecord::Base
  attr_accessible :description, :name, :parent_id
  # Association
  belongs_to :parent, :class_name => 'CostComponentType', :foreign_key => :parent_id
  has_many :children, :class_name => 'CostComponentType', :foreign_key => :parent_id
  has_many :cost_components
  has_many :product_cost_component_calcs
end
