class AcctgTrans < ActiveRecord::Base
	attr_accessible :acctg_trans_type_enum_id, :description, :transaction_date, :is_posted, :posted_date, :scheduled_posting_date,
	:gl_journal_id, :gl_fiscal_type_enum_id, :voucher_ref, :voucher_date, :group_status_id, :asset_id, :physical_inventory_id,
	:party_id, :role_type_id, :invoice_id, :payment_id, :fin_account_trans_id, :shipment_id, :receipt_id, :work_effort_id, :their_acctg_trans_id

	enum_attr :acctg_trans_type_enum_id, %w(INTERNAL_ACCTG_TRANS AMORTIZATION DEPRECIATION CAPITALIZATION INVENTORY ITEM_VARIANCE OTHER_INTERNAL 
	PERIOD_CLOSING EXTERNAL_ACCTG_TRANS OBLIGATION_ACCTG_TRA CREDIT_LINE CREDIT_MEMO NOTE OTHER_OBLIGATION SALES TAX_DUE PAYMENT_ACCTG_TRANS DISBURSEMENT 
	RECEIPT INVENTORY_RETURN SALES_INVOICE PURCHASE_INVOICE CUST_RTN_INVOICE SALES_SHIPMENT SHIPMENT_RECEIPT MANUFACTURING INCOMING_PAYMENT OUTGOING_PAYMENT PAYMENT_APPL) 

	enum_attr :gl_fiscal_type_enum_id, %w(GLFT_ACTUAL GLFT_BUDGET GLFT_FORECAST GLFT_PLAN GLFT_SCENARIO)

	belongs_to :acctg_trans_type, :foreign_key => :acctg_trans_type_enum_id
	belongs_to :gl_journal
	belongs_to :gl_fiscal_type, :foreign_key => :gl_fiscal_type_enum_id
	belongs_to :status_item
	belongs_to :asset
	belongs_to :physical_inventory
	belongs_to :party
	belongs_to :role_type
	belongs_to :invoice
	belongs_to :payment
	belongs_to :financial_account_trans
	belongs_to :shipment
	belongs_to :work_effort
end
