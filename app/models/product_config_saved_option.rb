class ProductConfigSavedOption < ActiveRecord::Base
  attr_accessible :config_item_id, :config_option_seq_id, :product_config_saved_id, :product_id, :quantity
  # Composite primary keys
  self.primary_keys = :product_config_saved_id, :config_item_id
  # Association
  belongs_to :product
  belongs_to :product_config_option, :foreign_key => [:config_item_id, :config_option_seq_id]
end
