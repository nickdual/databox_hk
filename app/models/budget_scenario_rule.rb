class BudgetScenarioRule < ActiveRecord::Base
	attr_accessible :budget_scenario_id, :budget_item_type_enum_id, :amount_change, :percentage_change

	enum_attr :budget_item_type_enum_id, %w(DATA_1 DATA_2)

	belongs_to :budget_scenario
	belongs_to :budget_item_type
end
