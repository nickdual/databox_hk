class AccommodationSpot < ActiveRecord::Base
  attr_accessible :accommodation_class_id, :asset_id, :description, :number_of_spaces
  # Association
  belongs_to :accommodation_class
  belongs_to :asset
end
