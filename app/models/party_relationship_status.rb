class PartyRelationshipStatus < ActiveRecord::Base
  # Association
  has_many :status_party_relationships, :class_name => 'PartyRelationship', :foreign_key => :status_id
end
