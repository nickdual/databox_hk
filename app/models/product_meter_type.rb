class ProductMeterType < ActiveRecord::Base
  attr_accessible :description, :name, :default_uom_id
  # Association
  has_many :interval_asset_maintenances, :class_name => 'AssetMaintenance', :foreign_key => :interval_meter_type_id
  has_many :asset_maintenance_meters
  has_many :asset_meters
  has_many :product_meters
  has_many :interval_product_maintenances, :class_name => 'ProductMaintenance', :foreign_key => :interval_meter_type_id
end
