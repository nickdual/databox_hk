class TrainingClass < ActiveRecord::Base
  attr_accessible :description, :training_class_type_enum_id
  # Association
  belongs_to :training_class_type, :foreign_key => :training_class_type_enum_id
end
