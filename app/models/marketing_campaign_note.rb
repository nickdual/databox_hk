class MarketingCampaignNote < ActiveRecord::Base
  attr_accessible :marketing_campaign_id, :note_date, :note_text
  
  belongs_to :marketing_campaign
end
