class AgreementParty < ActiveRecord::Base
  attr_accessible :agreement_id, :party_id, :role_type_id
  # Composite primary keys
  self.primary_keys = :agreement_id, :party_id, :role_type_id
  # Association
  belongs_to :agreement
  belongs_to :party
  belongs_to :role_type
end
