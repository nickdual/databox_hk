class SettlementTerm < ActiveRecord::Base
  attr_accessible :descriptin, :settlement_term_id, :term_type_enum_id, :term_value, :term_value_uom_id
  # Association
  belongs_to :term_type_enum, :class_name => "SettlementTermType", :foreign_key => "term_type_enum_id"
  belongs_to :term_value_uom, :class_name => "SettlementTermValue", :foreign_key => "term_value_uom_id"
  has_many :agreement_terms
end
