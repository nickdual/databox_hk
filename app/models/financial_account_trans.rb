class FinancialAccountTrans < ActiveRecord::Base
  attr_accessible :amount, :comments, :entry_date, :fin_account_id, :fin_account_trans_id, :gl_reconciliation_id, :order_id, :order_item_seq_id, :party_id, :payment_id, :performed_by_party_id, :reason_enum_id, :status_id, :transaction_date
  
  belongs_to :fin_account_trans_type_enum, :class_name => "FinancialAccountTransType", :foreign_key => "fin_account_trans_type_enum_id"
  belongs_to :financial_account
  belongs_to :status, :class_name => "FinancialAccountTransStatus"
  belongs_to :party
  belongs_to :payment
  belongs_to :order_id, :class_name => "OrderItem", :foreign_key => "order_id"
  belongs_to :order_item_seq_id, :class_name => "OrderItem", :foreign_key => "order_item_seq_id"
  belongs_to :performed_by_party, :class_name => "Party", :foreign_key => "performed_by_party_id"
  belongs_to :reason_enum, :class_name => "FinancialAccountTransReason", :foreign_key => 'reason_enum_id'
  belongs_to :gl_reconciliation
end
