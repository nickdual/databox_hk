class ProductGlAccount < ActiveRecord::Base
	attr_accessible :product_id, :organization_party_id, :gl_account_type_enum_id, :gl_account_id

	belongs_to :party
	belongs_to :product
	belongs_to :gl_account_type
	belongs_to :gl_account
end
