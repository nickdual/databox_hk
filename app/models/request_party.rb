class RequestParty < ActiveRecord::Base
  attr_accessible :from_date, :party_id, :request_id, :role_type_id, :thru_date
  
  belongs_to :request
  belongs_to :party
  belongs_to :role_type
end
