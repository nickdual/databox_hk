class RequestNote < ActiveRecord::Base
  attr_accessible :note_date, :note_text, :request_id, :request_item_seq_id
  
  belongs_to :request_item_seq, :class_name => "RequestItem", :foreign_key => "request_item_seq_id"
end
