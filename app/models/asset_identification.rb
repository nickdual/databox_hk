class AssetIdentification < ActiveRecord::Base
  attr_accessible :asset_id, :id_value, :identification_type_id
  # Composite primary keys
  self.primary_keys = :asset_id, :identification_type_id
  # Association
  belongs_to :asset
  belongs_to :identification_type, :class_name => 'AssetIdentificationType', :foreign_key => :identification_type_id 
end
