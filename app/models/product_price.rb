class ProductPrice < ActiveRecord::Base
  attr_accessible :agreement_id, :agreement_item_seq_id, :customer_party_id, :from_date, :min_quantity, :price, :price_purpose_id, :price_type_id, :price_uom_id, :product_id, :product_store_id, :tax_amount, :tax_auth_geo_id, :tax_auth_party_id, :tax_in_price, :tax_percentage, :term_uom_id, :thru_date, :vendor_party_id
  # Association
  belongs_to :product
  belongs_to :price_type, :class_name => 'ProductPriceType', :foreign_key => :price_type_id
  belongs_to :price_purpose, :class_name => 'ProductPricePurpose', :foreign_key => :price_purpose_id
  belongs_to :product_store
  belongs_to :tax_auth_party, :class_name => 'TaxAuthority', :foreign_key => :tax_auth_party_id
  belongs_to :tax_auth_geo, :class_name => 'TaxAuthority', :foreign_key => :tax_auth_geo_id
end
