class PartyIdentification < ActiveRecord::Base
  attr_accessible :expire_date, :id_value, :party_id, :party_id_type_id
  # Composite primary key
  # self.primary_keys = :party_id, :party_id_type_id
  # Association
  belongs_to :party
  belongs_to :party_id_type
end
