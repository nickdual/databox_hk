class PromotionCodeParty < ActiveRecord::Base
  attr_accessible :party_id, :product_promotion_code_id
  # Association
  belongs_to :promotion_code, :foreign_key => :product_promotion_code_id
end
