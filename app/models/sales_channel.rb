class SalesChannel < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :order_headers
end
