class ProductSubscriptionResource < ActiveRecord::Base
  attr_accessible :available_time, :available_time_uom_id, :from_date, :product_id, :purchase_from_date, :purchase_thru_date, :subscription_resource_id, :thru_date, :use_count_limit, :use_role_type_id, :use_time, :use_time_uom_id
  # Composite primary keys
  # self.primary_keys = :product_id, :subscription_resource_id, :from_date
  # Association
  belongs_to :product
  belongs_to :subscription_resource
  belongs_to :use, :class_name => 'RoleType', :foreign_key => :use_role_type_id
end
