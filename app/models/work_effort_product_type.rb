class WorkEffortProductType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :work_effort_products, :class_name => 'WorkEffortProduct', :foreign_key => :type_id
end
