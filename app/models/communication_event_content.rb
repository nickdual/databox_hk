require 'composite_primary_keys'
class CommunicationEventContent < ActiveRecord::Base
  attr_accessible :assoc_type_id, :communication_event_id, :content_location, :from_date, :sequence_num, :thru_date
  # Composite primary keys
  # self.primary_keys = :communication_event_id, :content_location, :from_date
  # Association 
  belongs_to :communication_event
end
