class TrackingCodeVisit < ActiveRecord::Base
  attr_accessible :from_date, :source_enum_id, :tracking_code_id, :visit_id
  
  belongs_to :tracking_code
  belongs_to :source_enum, :class_name => "TrackingCodeSource", :foreign_key => "source_enum_id"
end
