class ShipmentPackageRouteSeg < ActiveRecord::Base
  attr_accessible :amount_uom_id, :box_number, :cod_amount, :insured_amount, :international_invoice, :label_html, :label_image, :label_intl_sign_image, :label_printed, :package_other_cost, :package_service_cost, :package_transport_cost, :shipment_id, :shipment_package_seq_id, :shipment_route_segment_id, :tracking_code

  belongs_to :shipment_package
  belongs_to :shipment_route_segment
  belongs_to :amount
end
