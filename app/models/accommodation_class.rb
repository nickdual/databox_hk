class AccommodationClass < ActiveRecord::Base
  # Association
  has_many :accommodation_maps
  has_many :accommodation_spots
end
