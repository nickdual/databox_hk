class Delivery < ActiveRecord::Base
  attr_accessible :actual_arrival_date, :actual_start_date, :asset_id, :dest_facility_id, :end_mileage, :estimated_arrival_date, :estimated_start_date, :fuel_used, :origin_facility_id, :start_mileage

  has_one :shipment_route_segment
  
  belongs_to :asset
  belongs_to :facility
  
end
