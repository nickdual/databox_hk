class EmploymentResponsibility < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :empl_position_responsibilities
end
