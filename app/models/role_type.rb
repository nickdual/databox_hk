class RoleType < ActiveRecord::Base
  attr_accessible :description, :parent_type_id, :name
  # Association 
  has_one :budget_party
  has_one :gl_account_party
  has_one :acctg_trans
  has_one :acctg_trans_entry
  has_one :gl_account_type_party_default
  has_one :item_issuance_party
  has_one :picklist_party
  has_one :shipment_receipt_party
  belongs_to :parent, :class_name => 'RoleType', :foreign_key => :parent_type_id
  has_many :children, :class_name => 'RoleType', :foreign_key => :parent_type_id
  has_many :from_role_type_communication_events, :class_name => "CommunicationEvent", :foreign_key => 'from_role_type_id' 
  has_many :to_role_type_communication_events, :class_name => "CommunicationEvent", :foreign_key => 'to_role_type_id'
  has_many :communication_event_parties
  has_many :from_role_type_party_relationships, :class_name => 'PartyRelationship', :foreign_key => :from_role_type_id
  has_many :to_role_type_party_relationships, :class_name => 'PartyRelationship', :foreign_key => :to_role_type_id
  has_many :party_roles
  has_many :order_part_parties
  has_many :timesheet_parties
  has_many :work_effort_parties
  has_many :sales_opportunity_parties
  has_many :party_needs
  has_many :use_product_subscription_resources, :class_name => 'ProductSubscriptionResource', :foreign_key => :use_role_type_id
  has_many :product_store_parties
  has_many :product_store_group_parties
  has_many :product_parties
  has_many :product_category_parties
  has_many :asset_party_assignments
  has_many :from_agreements, :class_name => 'Agreement', :foreign_key => :from_role_type_id
  has_many :to_agreements, :class_name => 'Agreement', :foreign_key => :to_role_type_id
  has_many :agreement_parties
  has_many :agreement_item_parties
end
