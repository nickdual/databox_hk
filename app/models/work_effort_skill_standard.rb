class WorkEffortSkillStandard < ActiveRecord::Base
  attr_accessible :estimated_cost, :estimated_duration, :estimated_num_people, :skill_type_id, :work_effort_id
  # Composite primary keys
  # self.primary_keys = :work_effort_id, :skill_type_id
  # Association
  belongs_to :work_effort
end
