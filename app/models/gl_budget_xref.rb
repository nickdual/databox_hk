class GlBudgetXref < ActiveRecord::Base
  attr_accessible :gl_account_id, :budget_item_type_enum_id, :from_date, :thru_date, :allocation_percentage

  belongs_to :gl_account
end
