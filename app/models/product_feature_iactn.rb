class ProductFeatureIactn < ActiveRecord::Base
  attr_accessible :from_product_feature_id, :iactn_type_id, :product_id, :to_product_feature_id
  # Composite primary keys
  self.primary_keys = :from_product_feature_id, :to_product_feature_id
  # Association
  belongs_to :iactn_type, :class_name => 'ProductFeatureIactnType', :foreign_key => :iactn_type_id
  belongs_to :from_product_feature, :class_name => 'ProductFeature', :foreign_key => :from_product_feature_id
  belongs_to :to_product_feature, :class_name => 'ProductFeature', :foreign_key => :to_product_feature_id
end
