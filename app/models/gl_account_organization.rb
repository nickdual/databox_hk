class GlAccountOrganization < ActiveRecord::Base
	attr_accessible :gl_account_id, :organization_party_id, :from_date, :thru_date, :posted_balance

	belongs_to :gl_account
	belongs_to :party
end
