class TaxAuthorityAssoc < ActiveRecord::Base
  attr_accessible :tax_auth_geo_id, :tax_auth_party_id, :to_tax_auth_geo_id, 
  :to_tax_auth_party_id, :from_date, :thru_date, :assoc_type_enum_id 

  enum_attr :assoc_type_enum_id, %w(EXEMPT_INHER COLLECT_AGENT)

  belongs_to :tax_authority
  belongs_to :tax_authority_assoc_type, :foreign_key => :assoc_type_enum_id
end
