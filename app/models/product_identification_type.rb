class ProductIdentificationType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :product_identifications, :class_name => 'ProductIdentification', :foreign_key => :product_id_type_id
end
