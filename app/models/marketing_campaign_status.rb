class MarketingCampaignStatus < ActiveRecord::Base
  attr_accessible :description, :sequence_num, :status_id
  # Association
  has_many :marketing_campaigns
  has_many :status_valids, :class_name => 'MarketingCampaignStatusValid', :foreign_key => :status_id
  has_many :to_status_valids, :class_name => 'MarketingCampaignStatusValid', :foreign_key => :to_status_id
  
end
