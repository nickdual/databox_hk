class ProductStoreGroupParty < ActiveRecord::Base
  attr_accessible :from_date, :party_id, :product_store_group_id, :role_type_id, :thru_date
  # Composite primary keys
  # self.primary_keys = :product_store_group_id, :party_id, :role_type_id, :from_date
  # Association
  belongs_to :product_store_group
  belongs_to :party
  belongs_to :role_type
end
