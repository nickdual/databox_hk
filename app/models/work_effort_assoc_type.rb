class WorkEffortAssocType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :work_effort_assocs
end
