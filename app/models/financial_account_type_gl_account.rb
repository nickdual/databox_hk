class FinancialAccountTypeGlAccount < ActiveRecord::Base
	attr_accessible :fin_account_type_id, :organization_party_id, :gl_account_id

	belongs_to :financial_account_type
	belongs_to :party
	belongs_to :gl_account

end
