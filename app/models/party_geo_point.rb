class PartyGeoPoint < ActiveRecord::Base
  attr_accessible :from_date, :geo_point_id, :party_id, :thru_date
  # Composite primary key
  # self.primary_keys = :party_id, :geo_point_id, :from_date
  # Association
  belongs_to :party
end
