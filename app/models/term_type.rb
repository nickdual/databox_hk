class TermType < ActiveRecord::Base
  attr_accessible :parent_id, :description, :name
  # Association
  belongs_to :parent, :class_name => 'TermType', :foreign_key => :parent_id
  has_many :children, :class_name => 'TermType', :foreign_key => :parent_id
  has_many :agreement_terms
  
end
