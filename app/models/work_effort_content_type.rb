class WorkEffortContentType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :work_effort_contents
end
