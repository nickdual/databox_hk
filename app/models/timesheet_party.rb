class TimesheetParty < ActiveRecord::Base
  attr_accessible :party_id, :role_type_id, :timesheet_id
  # Composite primary keys
  # self.primary_keys = :timesheet_id, :party_id, :role_type_id
  # Association
  belongs_to :party
  belongs_to :timesheet
  belongs_to :role_type
end
