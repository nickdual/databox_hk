class ProductCategoryFeatGrpAppl < ActiveRecord::Base
  attr_accessible :from_date, :product_category_id, :product_feature_group_id, :thru_date
  # Composite primary keys
  self.primary_keys = :product_category_id, :product_feature_group_id, :from_date
  # Association
  belongs_to :product_category
  belongs_to :product_feature_group
end
