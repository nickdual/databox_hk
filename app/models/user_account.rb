class UserAccount < ActiveRecord::Base
  attr_accessible :party_id
  # Association
  belongs_to :party
  has_one :shipment_receipt
  has_many :product_reviews, :class_name => 'ProductReview', :foreign_key => :user_id
end
