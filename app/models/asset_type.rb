class AssetType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_one :asset_type_gl_account
  has_many :assets
end
