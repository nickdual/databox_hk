class ShipmentRouteSegmentStatus < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
end
