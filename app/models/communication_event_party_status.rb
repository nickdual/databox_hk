class CommunicationEventPartyStatus < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
  # Association
  has_many :status_communication_event_parties, :class_name => 'CommunicationEventParty', :foreign_key => 'status_id'
  has_many :status_valids, :class_name => 'CommunicationEventPartyStatusValid', :foreign_key => 'status_id'
  has_many :to_status_valids, :class_name => 'CommunicationEventPartyStatusValid', :foreign_key => 'to_status_id'
end
