class Invoice < ActiveRecord::Base
  attr_accessible :billing_account_id, :currency_uom_id, :description, :due_date, :from_party_id, :invoice_date, :invoice_id, :invoice_message, :invoice_type_enum_id, :paid_date, :reference_number, :status_id, :to_party_id
  
  belongs_to :invoice_type_enum, :class_name => "InvoiceType", :foreign_key => "invoice_type_enum_id"
  belongs_to :from_party, :class_name => "Party", :foreign_key => "from_party_id"
  belongs_to :to_party, :class_name => "Party", :foreign_key => "to_party_id"
  belongs_to :status, :class_name => "InvoiceStatus", :foreign_key => "status_id"
  belongs_to :billing_account
end
