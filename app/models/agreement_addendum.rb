class AgreementAddendum < ActiveRecord::Base
  attr_accessible :addendum_creation_date, :addendum_effective_date, :addendum_text, :agreement_id, :agreement_item_seq_id
  # Association
  belongs_to :agreement
  belongs_to :agreement_item, :foreign_key => [:agreement_id, :agreement_item_seq_id]
end
