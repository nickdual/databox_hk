class InterviewGrade < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
  # Association
  has_many :job_interviews
end
