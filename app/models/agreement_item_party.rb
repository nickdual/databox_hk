class AgreementItemParty < ActiveRecord::Base
  attr_accessible :agreement_id, :agreement_item_seq_id, :party_id, :role_type_id
  # Composite primary keys
  self.primary_keys = :agreement_id, :agreement_item_seq_id, :party_id, :role_type_id
  # Association
  belongs_to :agreement_item, :foreign_key => [:agreement_id, :agreement_item_seq_id]
  belongs_to :party
  belongs_to :role_type
end
