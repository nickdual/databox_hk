class ShipmentReceipt < ActiveRecord::Base
  attr_accessible :asset_id, :datetime_received, :item_description, :order_id, :order_item_seq_id, :product_id, :quantity_accepted, :quantity_rejected, :received_by_user_id, :rejection_reason_id, :return_id, :return_item_seq_id, :shipment_id, :shipment_item_seq_id, :shipment_package_seq_id
  
  enum_attr :rejection_reason_id, %w(SRJ_DAMAGED SRJ_NOT_ORDERED SRJ_OVER_SHIPPED)

  # Association
  has_one :shipment_receipt_party

  has_many :return_item_billings

  belongs_to :asset
  belongs_to :product
  belongs_to :shipment_package
  belongs_to :order_item
  belongs_to :rejection_reason
  belongs_to :user_account
  belongs_to :shipment_item
  belongs_to :return_item
  has_many :order_item_billings
end
