class ShipmentMethod < ActiveRecord::Base
  attr_accessible :description, :name

  has_one :carrier_shipment_method
  has_one :picklist
  has_one :shipment_route_segment

  # Association
  has_many :order_parites
  has_many :promotion_rule_conditions
  def self.shipment_method_select
    self.all.map{ |item| [item.name,item.id] }
  end
end
