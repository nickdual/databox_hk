class ContactListPartyStatus < ActiveRecord::Base
  attr_accessible :description, :sequence_num, :status_id
  # Association
  has_many :contact_list_parties
end
