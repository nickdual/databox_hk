/* Author:

*/

$(function(){
	
	var CART = {
		container: $('#cart'),
		link: $('#cart a'),
		loader: $('#cart a .icon-loader-pink').clone(),
		growl: $('#cart #cart-growl').clone(),
		diffType: 'added',
		items: 0,
		itemsEl: $('#items h5'),
		estPrice: 0,
		estPriceElCont: $('#estimated-price'),
		estPriceEl: $('#estimated-price h5'),
		minDue: 0,
		minDueElCont: $('#minimum-due'),
		minDueEl: $('#minimum-due h5')
	};
	$('#cart a .icon-loader-pink').remove();
	$('#cart #cart-growl').remove();
		
	
	$('.pill').prepend('<span class="grad"></span>');
	
	$('.filter').live('mouseenter',function(){
		$(this).find('.dropdown').toggle();
	}).live('mouseleave',function(){
		$(this).find('.dropdown').hide();
	});
	
	$('.fancybox').fancybox({
		closeBtn: false
	});
	$('.icon-overlay-close').click(function(e){
		e.preventDefault();
		$.fancybox.close();
	});

  /*
   * Prevent <= 0 qty
   */
   $('.input-qty').live('blur',function(e){
		if ($(this).val() != '' && $(this).val() < 1) {
			$(this).val('');
		}
	});

  /*
   * Clear input-qty on click
   */

  $('.input-qty').live('click', function(){
    $(this).val('');
    $(this).addClass('border-pink');
  });

  $('.input-qty').live('blur', function(){
    $(this).removeClass('border-pink');
  });

  /*
   * qty exceed
   */

  $('.input-qty').live('change', function(){
    if(!$(this).val() || $(this).val() <= 500) $(this).siblings('.qty-exceeded').hide();
  });

  $('.input-qty').live('keyup', function(){
    if(!$(this).val() || $(this).val() <= 500) $(this).siblings('.qty-exceeded').hide();
  });


	$('.cell-color, .cell-size, .cell-qty, .cell-item').live('mouseenter',function(){
		$(this).find('.dropdown:not(.qty-exceeded)').toggle();
		$(this).addClass('hover');
	}).live('mouseleave',function(){
		$(this).find('.dropdown:not(.qty-exceeded)').hide();
		$(this).removeClass('hover');
	});
	
	/*
	$('.more').live('click',function(){
		$(this).find('.dropdown').toggle();
	});
	*/
	$('.more').live('mouseenter',function(){
    $(this).find('.dropdown').toggle();
  }).live('mouseleave',function(){
    $(this).find('.dropdown').hide();
  });
	
	$('.swatch a').live('click', function(e){
		var swatch = $(this).parents('.swatch');
		swatch.parents('.cell-color').find('.pill img').attr('src',$(this).data('chip'));
		e.preventDefault();
	});
	
	$('.size').find('li').live('click', function(){
		var size = $(this);
		size.parents('.cell-size').find('.size-value').text(size.text());
		rowUpdated($(this).parents('tr'));
	});
	
	$('.cell-qty input').numeric();
	$('.cell-qty input').live('keyup',function(e){
		var qty = $(this);
		qty.next('.qty').hide();
		//rowUpdated($(this).parents('tr'));
	});
	
	$('.qty').find('li').live('click', function(){
		var qty = $(this);
		qty.parents('.cell-qty').find('input').val(qty.text());
		//owUpdated($(this).parents('tr'));
	});
	
	$('.add-button:not(.btn-disabled)').live('click',function(e){
		e.preventDefault();
    // Prevent add if qty exceed max
    var qty = $(this).parents('tr').find('.input-qty');
    if(qty.val() > 500) {qty.siblings('.qty-exceeded').show();  return false;}
    else qty.siblings('.qty-exceeded').hide();

		if(!$(this).hasClass('btn-disabled')){
			var img = $(this).parents('tr').find('.cell-item img').attr('src');
			var color = $(this).parents('tr').find('.cell-color .pill img').attr('src');
			var qty = $(this).parents('tr').find('.input-qty').val();
			var price = $(this).parents('tr').find('.cell-price span').text().replace('$','');
			
			$('.items-added').append('<li class="added hidden"><input type="hidden" class="input-price" value="'+price+'" /><span class="thumb"><img src="'+img+'"></span><span class="chip pill"><img src="'+color+'"></span><span class="qty">('+qty+')<input type="hidden" class="input-qty" value="'+qty+'" /></span><a href="#" class="remove-button"><i class="icon-remove"></i></a></li>');
			
			$('.items-added li.hidden').slideDown().removeClass('hidden');
			
			cartUpdate('added');
			
			resetRow($(this).parents('tr'));
		}
	});

  $(document).click(function(e){
    if($('.info-miss').has(e.target).length == 0)
      $('.info-miss').hide();
  });

  $('.add-button.btn-disabled').live('click', function(e){
    $(this).next().show();
    e.stopPropagation();
    e.preventDefault();
  });
	
	function resetRow(row){
		row.find('.cell-color .pill img').attr('src',row.find('.cell-color .swatch ul li:eq(0) a').data('chip'));
		row.find('.cell-size .size-value').text('');
		row.find('.cell-qty .input-qty').val('');
		row.find('.cell-remove .add-button').addClass('btn-disabled');
	}
	
	$('.remove-button').live('click',function(e){
		e.preventDefault();
		
		$(this).parents('li').slideUp('normal',function(){
			$(this).remove();
			cartUpdate('remove');
		});
	});
	
	
	/*
	 * Cart Update
	 */
	function cartUpdate(type) {
		var newCount = 0;
		var newEstPrice = 0;
		var newMinDue = 0;
		
		$('.added').each(function(){
			newCount += parseInt($(this).find('.input-qty').val());
			log(parseInt($(this).find('.input-qty').val()));
			newEstPrice += parseInt($(this).find('.input-qty').val()) * parseFloat($(this).find('.input-price').val());
		});
		newEstPrice = Math.round(newEstPrice*100)/100;
		newMinDue = Math.round(parseFloat(newEstPrice*.3)*100)/100;
		
		var diffType = 'added';
		var diffCount = 0;
		
		/* if CART is NOT empty */
		if(CART.items > 0) {
			if(CART.items < newCount) {
				/* Added Items */
				diffType = 'added';
			} else {
				/* Removed Items */
				diffType = 'removed';
			}
			diffCount = CART.items - newCount;
		/* if CART IS empty */
		} else {
			diffCount = newCount;
		}
		
		cartUpdateData({
			diffType: diffType,
			diffCount: Math.abs(diffCount),
			oldItems: CART.items,
			oldEstPrice: CART.estPrice,
			oldMinDue: CART.minDue,
			newCount: newCount,
			newEstPrice: newEstPrice,
			newMinDue: newMinDue
		});
	}
	
	function cartUpdateData(options) {
		CART.diffType = options.diffType;
		CART.items = options.newCount;
		CART.estPrice = options.newEstPrice;
		CART.minDue = options.newMinDue;
		
		if(CART.items > 0) {
			if(CART.estPriceElCont.hasClass('hidden')) {
				CART.estPriceElCont.removeClass('hidden').animate({
					width: 121
				},function(){$(this).css('min-width',$(this).width()).width('auto');});
				CART.minDueElCont.removeClass('hidden').animate({
					width: 115
				},function(){$(this).css('min-width',$(this).width()).width('auto');});
			}
		} else {
			CART.estPriceElCont.animate({
				width: 0
			},function(){$(this).addClass('hidden').css('min-width',0)})
			CART.minDueElCont.animate({
				width: 0
			},function(){$(this).addClass('hidden').css('min-width',0)});
		}
		
		CART.itemsEl.fadeOut('fast',function(){
			CART.itemsEl.text(options.newCount);
			CART.itemsEl.fadeIn('fast');
		});
		
		CART.estPriceEl.fadeOut('fast',function(){
			CART.estPriceEl.text(options.newEstPrice);
			CART.estPriceEl.fadeIn('fast');
		});
		
		CART.minDueEl.fadeOut('fast',function(){
			CART.minDueEl.text(options.newMinDue);
			CART.minDueEl.fadeIn('fast');
		});
		
		cartGrowl({
			type: options.diffType,
			count: options.diffCount
		});
	}
	
	function cartGrowl(options) {
		var type = options.type;
		var count = options.count;
		
		/* remove any existing growls */
		CART.container.find('#cart-added').fadeOut('fast',function(){$(this).remove();});
		
		/* create new growl */
		var growl = CART.growl.clone();
		
		growl.html('<em><span>'+count+'</span> units '+type+'.</em><img src="fpo/p_cart-item-added.jpg" /> <i class="icon-cart-added-caret"></i>');
		
		CART.container.append(growl);
		
		if(!CART.link.find('.icon-loader-pink').size()){
			CART.link.append(CART.loader.clone().removeClass('hidden').hide().fadeIn('fast'));
		}
		
		growl.fadeIn('normal',function(){
			setTimeout(function(){
				growl.fadeOut('normal',function(){growl.remove();});
				CART.link.find('.icon-loader-pink').fadeOut('normal',function(){$(this).remove();});
			}, 3000);
		});
	}
	
	/*
	 * Product silder
	 */
	$('.item-carousel').cycle({
		fx: 'scrollLeft', 
		prev:	 '.icon-arrow-carousel-left', 
		next:	 '.icon-arrow-carousel-right',
		speed: 750,
		timeout: 0,
		before: function(){
		}
	});

  /*
   * Back to top
   */

  $('#back-to-top').click(function(e){
    $('html, body').animate({
      scrollTop: 0
    }, 1000);
    e.preventDefault()
  });


  /*
   * turn plus button to pink on hover
   */	

  $('.pill:not(.add-button), .dropdown.qty, .dropdown.size, .dropdown.swatch, .thumb').hover(function(){
    $(this).parents('tr').find('.add-button').removeClass('btn-disabled');
  }, function(){
    if(!$(this).parents('tr').find('.size-value').text().length || !$(this).parents('tr').find('.input-qty').val())
      $(this).parents('tr').find('.add-button').addClass('btn-disabled');
  });

	
	if($('#sidebar').length > 0) {
		
		var jqWindow = $(window);
		var productContext = $('#main-content');
		var sidebar = productContext.find('#sidebar .floaty');
		
		jqWindow.data('height', jqWindow.height());
		sidebar.data('positionedWRT', 'parent');
		// Store some info about the page and the sidebar to make scrolling the sidebar with the page easier
		productContext.data('areaHeight', productContext.outerHeight());
		productContext.data('origOffsetY', productContext.offset().top);
		sidebar.data('origOffsetY', sidebar.offset().top);
		sidebar.data('height', sidebar.outerHeight());
		
		// Handle scrolling for the add-to-cart section, which follows the window
		var sidebarTopSpacing = 0;
		var sidebarScrollHandler = function() {
			var amtScrolledY = jqWindow.scrollTop();
			var encroachmentOnSidebar = amtScrolledY - (sidebar.data('origOffsetY') - sidebarTopSpacing);
			
			if (encroachmentOnSidebar > 0) {
				
				// Unless setNewTop ends up true, there's no reason to update the sidebar's CSS
				var setNewTop = (sidebar.data('positionedWRT') === 'parent') ? true : false;
				
				sidebar.data('positionedWRT', 'viewport');
				
				var sidebarNewTop = sidebarTopSpacing + 10;
			
				// Prevents the sidebar from running off the bottom of the page
				var unscrolledContentHeight = (productContext.data('areaHeight') + productContext.data('origOffsetY')) - amtScrolledY;
				if (unscrolledContentHeight < sidebar.data('height') + sidebarTopSpacing) {
					sidebarNewTop = unscrolledContentHeight - sidebar.data('height') + (sidebarTopSpacing * 0.5);
					setNewTop = true;
				}
				
				if (setNewTop) {
					sidebar.css({
						position:   'fixed',
						top:        sidebarNewTop,
						right:      getSidebarPosition(productContext)
					});
				}
			}
			else if (sidebar.data('positionedWRT') === 'viewport') {
				// Check first that sidebar.data('positionedWRT') is 'viewport' because if it's
				// already 'parent', then there's no reason that the sidebar's CSS would need to change.
				var newTop = 0;
				if(productContext.find('#sidebar .sub-head').length > 0){
					newTop = 50;
				}
				sidebar.data('positionedWRT', 'parent');
				sidebar.css({
					position:   'absolute',
					top:        newTop,
					right:      0
				});
			}
		};
		jqWindow.scroll(sidebarScrollHandler);
		
		// Returns the "right" setting that the sidebar should use. pageWrapper should be a jQuery object.
		function getSidebarPosition(pageWrapper) {
			return document.documentElement.clientWidth - pageWrapper.offset().left - pageWrapper.outerWidth(true);
		};
		
	}
});


