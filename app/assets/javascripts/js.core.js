var Core = (function ($, console) {
    "use strict";
    var initialized = false,
        moduleTypes = {},
        modules = [],
        plugins = [],
        body = $('body'),
        ops = {},
        moduleBase,
      ajax = [];
    
    function ModuleExistsException(selector) {
        this.message = 'Module ' + selector + ' already exists.';
        this.name = 'ModuleExistsException';
    }
    ModuleExistsException.prototype.toString = function () {
        return this.name + ': "' + this.message + '"';
    };
    
    function inArray(val, arr) {
        var i;
        for (i = 0; i < arr.length; i++) {
            if (arr[i] === val) {
                return true;
            }
        }
        return false;
    }
    
    // Base functionality given to every module instance
    moduleBase = {
        // Log (generally to the console)
        log : function (msg) {
            if (console.log) {
                console.log(msg);
            }
        },
        /*
         * 'destroy' method to remove a module
         * Note that this method does not destroy the associated element.
         * That can be done in the 'destroy' method (deconstructor) with
         * 'this.root.detach()'
         */
        del : function () {
            var id = this.context.data('module-index'),
                i;
            console.log('Destroying Module ' + this.name);
            if (this.events.destroy) {
                this.context.trigger('destroy');
            }
            modules.splice(id, 1);
            for (i = id + 1; i < modules.length; i++) {
                modules[i].context.data('module-index', modules[i].context.data('module-index')-1);
            }
        },
        /*
         * Send a signal with a named type and associated data to any
         * module listening for that signal type.
         */
        send : function () {
         console.info('[EVENT]: ' + arguments[0]);
         console.info(arguments[1]);

            var i;
            for (i = 0; i < modules.length; i++) {
                if (modules[i].events && modules[i].events[arguments[0]]) {
                    modules[i].context.trigger.apply(modules[i].context, arguments);
                }
            }
        },
      /*
         * Send ajax request
         */
        ajax : function (url, options) {
         var options = options || {};
         var blocking = options.blocking;
         var caller = options.caller;

         var DEFAULT = {
            contentType: 'application/json',
            dataType: 'json',
            timeout: 10000, // time out in 10 seconds
            success: function(json) {
               if (blocking) {
                  SUPERNOVA.global.hideActivityIndicator();
               }

               // TODO MESSY HERE
               if (caller) {
                  json.data['caller'] = caller
               }

               moduleBase.send(json.event, json.data);
            },
            error: function(xhr, status, error) {
               if (blocking) {
                  SUPERNOVA.global.hideActivityIndicator();
               }

               moduleBase.send(status);
            },
            complete: function() {
               for (var i = ajax.length - 1; i >= 0; i--) {
                  if (ajax[i] === url) {
                     ajax.splice(i, 1);
                     break;
                  }
               }
            }
         }

         // Merge and Override ajax handling
         $.extend(options, DEFAULT);

         // Check any same ajax request in process
         var exist = false;

         for (var n = ajax.length - 1; n >= 0; n--) {
            if (ajax[n] === url) {
               exist = true;
               break;
            }
         }

         if (exist === false) {
            if (blocking) {
               SUPERNOVA.global.showActivityIndicator();
            }

            ajax.push(url)
            $.ajax(url, options);

         } else {
            console.info('Request Cancelled. Reissued Ajax Request.');
         }
        }

    };

   // Handle Rails' Remote Form (thru Ajax) Response
   $(document).on('ajax:success', 'form[data-remote=true]', function(event, json) {
      moduleBase.send(json.event, json.data);
   })

    // Bind Event Hanlder function
    function bindEventHandler(reciever, func) {

        var r = reciever, f = func;
        return function () {
            f.apply(r, arguments);
        };
    }
    
    // Add a module
    function addModule(mod) {
        var i, event;
        // Bind events
        if (mod.events) {
            for (event in mod.events) {
                if (mod.events.hasOwnProperty(event)) {
                    mod.context.bind(event, bindEventHandler(mod, mod.events[event]));
                }
            }
        }
        // Feed the module to plugins with a 'newModule' method
        for (i = 0; i < plugins.length; i++) {
            if (plugins[i].newModule) {
//                plugins[i].newModule(selector, mod);
                plugins[i].newModule(mod);
            }
        }
        // Initialize the module if the Core has already been initialized
        if (initialized && mod.init) {
            mod.init(ops);
         mod.initComponents();
        }
        // Add the module to the array
        mod.context.data('module-index', modules.length);
        modules[modules.length] = mod;
    }

    return {

      sendMsg: function (event, data) {
         moduleBase.send(event, data);
      },

        /*
         * Create a module for every element matching a selector
         * Arguments: Selector string, Module template object
         */
        createModule : function (selector, module) {
            var mod;
         // TEMP Disabled exist module check
//            if (!moduleTypes.hasOwnProperty(selector)) {
                moduleTypes[selector] = $.extend({}, moduleBase, module);
                $(selector).each(function (index) {
                    mod = $.extend({}, moduleTypes[selector]);
                    mod.context = $(this);
                    addModule(mod);
                });
//            } else {
//                throw new ModuleExistsException(selector);
//            }
        },
        /*
         * Create a module based on another existing module type
         * Arguments: Base module type, selector string, module template object
         */
        createModuleFrom : function (base, selector, module) {
            var subMod, mod;
            if (!moduleTypes.hasOwnProperty(selector)) {
                subMod = $.extend(true, {}, moduleTypes[base], module);
                subMod._super = moduleTypes[base];
                moduleTypes[selector] = subMod;
                $(selector).each(function (index) {
                    mod = $.extend({}, subMod);
                    mod.context = $(this);
                    addModule(mod);
                });
            } else {
                throw new ModuleExistsException(selector);
            }
        },
        /*
         * Create a Plugin
         * Arguments: Plugin object
         * Description: Plugin objects can have two optional methods, 'init' and 'addModule.'
         * These are called by the core when it is initialized, and when a module is created,
         * respectively.
         * Plugins are also given a method 'find' which takes a selector and will return
         * all plugins whose root elements that match the selector.
         */
        createPlugin : function (plugin) {
            plugins[plugins.length] = plugin;
            plugin.send = moduleBase.send;
            plugin.find = function (selector) {
                var results = [], i;
                for (i = 0; i < modules.length; i++) {
                    if (modules[i].rootNode.selector === selector
                        || modules[i].rootNode.is(selector)) {
                        results[results.length] = modules[i];
                    }
                }
                return results;
            };
        },
        /*
         * Initialize the Core
         * Arguments: Options object
         * Description: Initializes modules and plugins, passing the options
         * object to them.
         */
        init : function (options) {
            var i;
            ops = options || {};
            initialized = true;
            for (i = 0; i < modules.length; i++) {
            // 1. Init module
                if (modules[i].init) {
                    modules[i].init(ops);
               console.log('Module ' + modules[i].name + ' initialized');
                }
            // 2. Init module components
            if (modules[i].initComponents) {
                    modules[i].initComponents();
                }
            }
            for (i = 0; i < plugins.length; i++) {
                if (plugins[i].init) {
                    plugins[i].init(ops, modules);
                }
            }
        }
    };
}(jQuery, console));