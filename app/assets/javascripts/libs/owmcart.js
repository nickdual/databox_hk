var OWMCART = function() { this.init(); }

OWMCART.CONSTANTS = {
	CHECKOUT_BTN_SELECTOR: '#btn-checkout',
	UPDATE_MSG_SELECTOR: '#update-order',
	UPDATE_ITEM_COUNT_SELECTOR: '#update-item-count',
	UPDATE_BTN_SELECTOR: '#btn-update-order',
	SAME_AS_BILLING_SELECTOR: '#input-same-as-billing-address',
	ADDRESS_SEL_SELECTOR: '#select-shipping-address',
	ADD_ADDRESS_BTN_SELECTOR: '#btn-add-address',
	ADD_ADDRESS_PNL_SELECTOR: '#add-address'
}

OWMCART.prototype = {
	
	checkoutBtn: null,
	updateMsg: null,
	updateItemCount: null,
	updateBtn: null,
	sameAsBilling: null,
	addAddressBtn: null,
	addAddressPanel: null,
	addressSelect: null,
	modifiedItems: new Array(),
	_c: OWMCART.CONSTANTS,
	
	init: function() {
		this.checkoutBtn = $(this._c.CHECKOUT_BTN_SELECTOR);
		this.updateMsg = $(this._c.UPDATE_MSG_SELECTOR);
		this.updateItemCount = $(this._c.UPDATE_ITEM_COUNT_SELECTOR);
		this.updateBtn = $(this._c.UPDATE_BTN_SELECTOR);
		this.sameAsBilling = $(this._c.SAME_AS_BILLING_SELECTOR);
		this.addAddressBtn = $(this._c.ADD_ADDRESS_BTN_SELECTOR);
		this.addAddressPanel = $(this._c.ADD_ADDRESS_PNL_SELECTOR);
		this.addressSelect = $(this._c.ADDRESS_SEL_SELECTOR);
		
		this.attachEvtUpdateBtn();
		this.attachEvtSameAsBilling();
	},
	
	attachEvtUpdateBtn: function(){
		var _self = this;
		
		this.updateBtn.click(function(e){
			_self.updateCart();
		});
	},
	
	attachEvtSameAsBilling: function(){
		var _self = this;
		
		this.sameAsBilling.change(function(e){
			_self.sameAsBillingChange();
		});
	},
	
	itemModified: function(item) {
		log('item modified');
		
		this.modifiedItems.push(item);
		this.updateItemCount.text(this.modifiedItems.length);
		this.updateMsg.slideDown();
		this.checkoutBtn.addClass('btn-disabled');
	},
	
	updateCart: function() {
		log('update cart');
		var items = this.modifiedItems;
		$(items).each(function(i){
			var item = $(this);
			item.find('.save-button').trigger('click');
		});
	},
	
	cartSaved: function() {
		this.modifiedItems.length = 0;
		this.updateItemCount.text(this.modifiedItems.length);
		this.updateMsg.slideUp();
		this.checkoutBtn.removeClass('btn-disabled');
		this.toggleCheckOutButton(true);
	},
	
	toggleCheckOutButton: function(enable) {
		if(enable){
			this.checkoutBtn.removeClass('btn-disabled');
		} else {
			this.checkoutBtn.addClass('btn-disabled');
		}
	},
	
	sameAsBillingChange: function() {
		if(this.sameAsBilling.is(':checked')) {
			this.addAddressPanel.slideUp();
			this.addressSelect.attr('disabled',true);
			this.addAddressBtn.addClass('btn-disabled');
		} else {
			this.addressSelect.attr('disabled',false);
			this.addAddressBtn.removeClass('btn-disabled');
		}
	}
	
}

$(function(){
});