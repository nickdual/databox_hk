module FacilitiesHelper
   
  def get_all_facility_types
    FacilityType.find(:all)
  end
  
  def get_all_weight_types
     WeightUom.find(:all)
  end
  
  def get_all_inventory_types
     InventoryItemType.find(:all)
  end
  
  def get_all_facility_size_uom
     FacilitySizeUom.find(:all)
  end
  
end
