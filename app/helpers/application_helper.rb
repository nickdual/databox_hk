require 'active_shipping'
include ActiveMerchant::Shipping

module ApplicationHelper
  def self.random_friendly_string(scope, length)
    str = ''
    length.times { str << scope[rand(scope.size)] }
    return str
  end

  def self.fedex
    packages = [
        Package.new(  100,                        # 100 grams
                      [93,10],                    # 93 cm long, 10 cm diameter
                      :cylinder => true),         # cylinders have different volume calculations

        Package.new(  (7.5 * 16),                 # 7.5 lbs, times 16 oz/lb.
                      [100, 10, 100.5],              # 15x10x4.5 inches
                      :units => :metric)        # not grams, not centimetres
    ]
    # You live in Beverly Hills, he lives in Ottawa
    origin = Location.new(      :country => 'US',
                                :state => 'CA',
                                :city => 'Beverly Hills',
                                :zip => '90210')

    destination = Location.new( :country => 'CA',
                                :province => 'ON',
                                :city => 'Ottawa',
                                :postal_code => 'K1P 1J1')


    fedex = FedEx.new(:login => ENV['FEDIX_LOGIN'], :password => ENV['FEDIX_PASSWORD'], :key => ENV['FEDIX_KEY'],
                      :account => ENV['FEDIX_ACCOUNT'], :meter => ENV['FEDIX_METER'], :test => true)

    tracking_info = fedex.find_rates(origin, destination, packages)
    usps_rates = tracking_info.rates.sort_by(&:price).collect {|rate| [rate.service_name, rate.price]}

  end
end
