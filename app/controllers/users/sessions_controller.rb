class Users::SessionsController < Devise::SessionsController
  layout "login"
  def create
    self.resource = warden.authenticate!(auth_options)
    # overridden check : if user is activated by admin or not.
    if self.resource.approval?
      set_flash_message(:notice, :signed_in) if is_navigational_format? &&
      sign_in(resource_name, resource)
      respond_with resource, :location => after_sign_in_path_for(resource)
    else
      set_flash_message(:notice, :inactive)
      #Destroy User Session if user is not activated by admin yet.
      destroy
    end
  end

end