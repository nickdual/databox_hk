class CustomersController < ApplicationController

	before_filter :authenticate_user!

	layout "customers"

	def index
		if params[:search_order].blank?
			@orders = OrderHeader.all
		else
			
			@orders = if params[:search_order]
				name = params[:search_order].split(" ")

				person = if(name.size.eql?(1))
					puts name
					Person.where("first_name LIKE (?) OR last_name LIKE (?)", "%#{params[:search_order]}%", "%#{params[:search_order]}%" ).map(&:party_id)
				else
					Person.where("first_name LIKE (?) AND last_name LIKE (?)", "%#{name[0]}%", "%#{name[1]}%").map(&:party_id)
				end

				unless person.empty?
					order_part = OrderPart.where("customer_party_id IN (?)", person).map(&:order_id).uniq
					OrderHeader.where("order_id IN (?)", order_part)
				else
					OrderHeader.where("order_id = ?", params[:search_order])
				end
			end

		end
	end

end
