class CreatePromotionRuleConditionTypes < ActiveRecord::Migration
  def change
    create_table :promotion_rule_condition_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
