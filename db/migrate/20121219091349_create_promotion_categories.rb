class CreatePromotionCategories < ActiveRecord::Migration
  def change
    create_table :promotion_categories do |t|
      t.string :product_category_id
      t.enum :sub_category_status
      t.string :group_id
      t.string :product_promotion_id
      
      t.timestamps
    end
  end
end
