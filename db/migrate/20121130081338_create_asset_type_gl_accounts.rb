class CreateAssetTypeGlAccounts < ActiveRecord::Migration
  def change
    create_table :asset_type_gl_accounts do |t|
    	t.enum :asset_type_enum_id
    	t.string :asset_id
    	t.integer :organization_party_id
    	t.integer :asset_gl_account_id
    	t.integer :acc_dep_gl_account_id
    	t.integer :dep_gl_account_id
    	t.integer :profit_gl_account_id
    	t.integer :loss_gl_account_id

      t.timestamps
    end
  end
end
