class CreatePartyContents < ActiveRecord::Migration
  def change
    create_table :party_contents do |t|
      t.integer :party_id
      t.text :content_location
      t.integer :party_content_type_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
