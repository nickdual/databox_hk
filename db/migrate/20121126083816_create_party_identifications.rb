class CreatePartyIdentifications < ActiveRecord::Migration
  def change
    create_table :party_identifications do |t|
      t.integer :party_id
      t.integer :party_id_type_id
      t.integer :id_value
      t.date :expire_date

      t.timestamps
    end
    add_index :party_identifications, :id_value, :name => 'PARTY_ID_VALUE'
  end
end
