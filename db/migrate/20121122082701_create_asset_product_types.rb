class CreateAssetProductTypes < ActiveRecord::Migration
  def change
    create_table :asset_product_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
