class CreateShipmentItems < ActiveRecord::Migration
  def change
    create_table :shipment_items do |t|
      t.integer :shipment_id
      t.integer :shipment_item_seq_id
      t.integer :product_id
      t.decimal :quantity
      t.text :description

      t.timestamps
    end
  end
end
