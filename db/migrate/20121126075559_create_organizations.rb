class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.integer :party_id
      t.text :organization_name
      t.text :office_site_name
      t.decimal :annual_revenue
      t.decimal :num_employees
      t.text :comments

      t.timestamps
    end
    add_index :organizations, :organization_name, :name => 'PTY_ORG_NAME_IDX'
  end
end
