class CreateReturnItemBillings < ActiveRecord::Migration
  def change
    create_table :return_item_billings do |t|
      t.integer :return_id
      t.integer :return_item_seq_id
      t.integer :invoice_id
      t.integer :invoice_item_seq_id
      t.integer :shipment_receipt_id
      t.decimal :quantity
      t.decimal :amount

      t.timestamps
    end
  end
end
