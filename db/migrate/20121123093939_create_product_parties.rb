class CreateProductParties < ActiveRecord::Migration
  def change
    create_table :product_parties do |t|
      t.integer :product_id
      t.integer :party_id
      t.integer :role_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :sequence_num
      t.text :comments

      t.timestamps
    end
  end
end
