class CreateReturnHeaderStatuses < ActiveRecord::Migration
  def change
    create_table :return_header_statuses do |t|
      t.string :description
      t.integer :sequence_num
      t.string :status_id

      t.timestamps
    end
  end
end
