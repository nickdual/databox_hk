class CreateProductIdentifications < ActiveRecord::Migration
  def change
    create_table :product_identifications do |t|
      t.integer :product_id
      t.integer :product_id_type_id
      t.integer :id_value

      t.timestamps
    end
    add_index :product_identifications, :id_value, :name => 'PRODUCT_ID_VALUE'
  end
end
