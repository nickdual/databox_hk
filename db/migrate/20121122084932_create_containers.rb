class CreateContainers < ActiveRecord::Migration
  def change
    create_table :containers do |t|
      t.integer :container_type_id
      t.integer :facility_id
      t.integer :geo_point_id
      t.text :description

      t.timestamps
    end
  end
end
