class CreatePayrollDeductionTypes < ActiveRecord::Migration
  def change
    create_table :payroll_deduction_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
