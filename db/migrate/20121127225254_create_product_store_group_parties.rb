class CreateProductStoreGroupParties < ActiveRecord::Migration
  def change
    create_table :product_store_group_parties do |t|
      t.integer :product_store_group_id
      t.integer :party_id
      t.integer :role_type_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
