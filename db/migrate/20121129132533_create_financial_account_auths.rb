class CreateFinancialAccountAuths < ActiveRecord::Migration
  def change
    create_table :financial_account_auths do |t|
      t.integer :fin_account_auth_id
      t.integer :fin_account_id
      t.decimal :amount
      t.integer :amount_uom_id
      t.datetime :authorization_date
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
