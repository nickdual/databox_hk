class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :product_type_id
      t.text :product_name
      t.text :description
      t.text :comments
      t.datetime :sales_introduction_date
      t.datetime :sales_discontinuation_date
      t.boolean :sales_disc_when_not_avail
      t.datetime :support_discontinuation_date
      t.boolean :require_inventory
      t.boolean :requirement_method_id
      t.boolean :charge_shipping
      t.boolean :in_shipping_box
      t.integer :default_shipment_box_type_id
      t.boolean :returnable
      t.boolean :require_amount
      t.integer :amount_uom_type_id
      t.decimal :fixed_amount
      t.integer :origin_geo_id
      t.integer :bill_of_material_level

      t.timestamps
    end
  end
end
