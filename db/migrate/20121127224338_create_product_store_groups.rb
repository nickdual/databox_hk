class CreateProductStoreGroups < ActiveRecord::Migration
  def change
    create_table :product_store_groups do |t|
      t.integer :product_store_group_type_id
      t.text :description

      t.timestamps
    end
  end
end
