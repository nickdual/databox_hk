class CreateOrderAdjustments < ActiveRecord::Migration
  def change
    create_table :order_adjustments do |t|
      t.integer :item_type_id
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :order_part_seq_id
      t.text :comments
      t.text :description
      t.decimal :amount
      t.decimal :recurring_amount
      t.decimal :amount_already_included
      t.integer :product_feature_id
      t.integer :corresponding_product_id
      t.integer :tax_authority_rate_id
      t.integer :source_reference_id
      t.decimal :source_percentage
      t.integer :customer_reference_id
      t.integer :primary_geo_id
      t.integer :secondary_geo_id
      t.decimal :exempt_amount
      t.integer :tax_auth_geo_id
      t.integer :tax_auth_party_id
      t.integer :override_gl_account_id
      t.boolean :include_in_tax
      t.boolean :include_in_shipping
      t.integer :original_adjustment_id

      t.timestamps
    end
  end
end
