class CreateAccommodationMaps < ActiveRecord::Migration
  def change
    create_table :accommodation_maps do |t|
      t.integer :accommodation_map_type_id
      t.integer :accommodation_class_id
      t.integer :asset_id
      t.integer :number_of_spaces

      t.timestamps
    end
  end
end
