class CreateProductPriceTypes < ActiveRecord::Migration
  def change
    create_table :product_price_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
