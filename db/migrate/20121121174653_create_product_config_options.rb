class CreateProductConfigOptions < ActiveRecord::Migration
  def change
    create_table :product_config_options do |t|
      t.integer :config_item_id
      t.integer :config_option_seq_id
      t.text :config_option_name
      t.text :description
      t.integer :sequence_num

      t.timestamps
    end
  end
end
