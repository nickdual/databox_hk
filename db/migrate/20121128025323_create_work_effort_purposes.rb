class CreateWorkEffortPurposes < ActiveRecord::Migration
  def change
    create_table :work_effort_purposes do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
