class AddFileToProductContent < ActiveRecord::Migration
  def self.up
      add_attachment :product_contents, :file
  end

  def self.down
    remove_attachment :product_contents, :file
  end
end