class CreateAgreementParties < ActiveRecord::Migration
  def change
    create_table :agreement_parties do |t|
      t.integer :agreement_id
      t.integer :party_id
      t.integer :role_type_id

      t.timestamps
    end
  end
end
