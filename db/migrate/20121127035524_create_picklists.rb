class CreatePicklists < ActiveRecord::Migration
  def change
    create_table :picklists do |t|
      t.text :description
      t.integer :facility_id
      t.integer :shipment_method_id
      t.integer :status_id
      t.datetime :picklist_date

      t.timestamps
    end
  end
end
