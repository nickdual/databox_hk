class CreateBillingAccounts < ActiveRecord::Migration
  def change
    create_table :billing_accounts do |t|
      t.integer :billing_account_id
      t.integer :bill_to_party_id
      t.integer :bill_from_id
      t.integer :account_limit
      t.integer :account_limit_uom_id
      t.integer :postal_contact_mech_id
      t.datetime :from_date
      t.datetime :thru_date
      t.text :description
      t.integer :external_account_id

      t.timestamps
    end
  end
end
