class CreatePartyRole < ActiveRecord::Migration
  def create
  create_table "party_role", :force => true do |t|
    t.integer   "party_id"
    t.integer   "role_type_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end
  end
end
