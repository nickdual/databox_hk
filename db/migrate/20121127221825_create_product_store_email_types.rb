class CreateProductStoreEmailTypes < ActiveRecord::Migration
  def change
    create_table :product_store_email_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
