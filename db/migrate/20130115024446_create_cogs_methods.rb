class CreateCogsMethods < ActiveRecord::Migration
  def change
    create_table :cogs_methods do |t|
      t.string :name
      t.string :description
      t.integer :sequence_num

      t.timestamps
    end
  end
end
