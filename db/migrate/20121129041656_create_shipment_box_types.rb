class CreateShipmentBoxTypes < ActiveRecord::Migration
  def change
    create_table :shipment_box_types do |t|
      t.text :description
      t.integer :dimension_uom_id
      t.decimal :box_length
      t.decimal :box_width
      t.decimal :box_height
      t.integer :weight_uom_id
      t.decimal :box_weight

      t.timestamps
    end
  end
end
