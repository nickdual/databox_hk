class CreateBudgetScenarios < ActiveRecord::Migration
	def change
		create_table :budget_scenarios do |t|
			t.string :description
			
			t.timestamps
		end
	end
end
