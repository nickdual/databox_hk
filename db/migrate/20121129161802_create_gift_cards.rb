class CreateGiftCards < ActiveRecord::Migration
  def change
    create_table :gift_cards do |t|
      t.integer :payment_method_id
      t.text :card_number
      t.text :pin_number
      t.text :expire_date

      t.timestamps
    end
  end
end
