class CreatePartyClassificationAppls < ActiveRecord::Migration
  def change
    create_table :party_classification_appls do |t|
      t.integer :party_id
      t.integer :party_classification_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
