class CreateProductMeters < ActiveRecord::Migration
  def change
    create_table :product_meters do |t|
      t.integer :product_id
      t.integer :product_meter_type_id
      t.integer :meter_uom_id
      t.string :meter_name

      t.timestamps
    end
  end
end
