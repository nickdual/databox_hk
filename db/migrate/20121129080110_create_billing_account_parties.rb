class CreateBillingAccountParties < ActiveRecord::Migration
  def change
    create_table :billing_account_parties do |t|
      t.integer :billing_account_id
      t.integer :party_id
      t.integer :role_type_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
