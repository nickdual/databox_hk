class CreateWorkEffortNotes < ActiveRecord::Migration
  def change
    create_table :work_effort_notes do |t|
      t.integer :work_effort_id
      t.datetime :note_date
      t.text :note_text
      t.boolean :internal_note

      t.timestamps
    end
  end
end
