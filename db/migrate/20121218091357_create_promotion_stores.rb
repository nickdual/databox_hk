class CreatePromotionStores < ActiveRecord::Migration
  def change
    create_table :promotion_stores do |t|
      t.integer :product_store_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :sequence_num
      t.string :product_promotion_id
      t.timestamps
    end
  end
end
