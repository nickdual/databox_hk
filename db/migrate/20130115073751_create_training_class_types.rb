class CreateTrainingClassTypes < ActiveRecord::Migration
  def change
    create_table :training_class_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
