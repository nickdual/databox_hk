class CreateRequest < ActiveRecord::Migration
  def create
	  create_table "request", :force => true do |t|
	    t.integer   "request_type_id"
	    t.integer   "request_category_id"
	    t.integer   "status_id"
	    t.text   "description"
	    t.integer   "from_party_id"
	    t.integer   "priority"
	    t.datetime   "request_date"
	    t.datetime   "response_required_date"
	    t.integer   "product_storeid"
	    t.integer   "sales_channel_id"
	    t.integer   "fulfill_contact_mech_id"
	    t.integer   "maximum_amount_uom_id"
	    t.integer   "currency_uom_id"
	    t.datetime "created_at",                    :null => false
	    t.datetime "updated_at",                    :null => false
	  end
  end
end
