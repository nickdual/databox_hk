class CreateMarketSegmentClassifications < ActiveRecord::Migration
  def change
    create_table :market_segment_classifications do |t|
      t.integer :market_segment_id
      t.integer :party_classification_id

      t.timestamps
    end
  end
end
