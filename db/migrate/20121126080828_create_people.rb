class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.integer :party_id
      t.text :salutation
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :personal_title
      t.string :suffix
      t.string :nickname
      t.boolean :gender
      t.date :birth_date
      t.date :deceased_date
      t.float :height
      t.float :weight
      t.text :mothers_maiden_name
      t.text :comments
      t.integer :marital_status_id
      t.integer :employment_status_id
      t.integer :residence_status_id
      t.string :occupation

      t.timestamps
    end
    add_index :people, :first_name
    add_index :people, :last_name
  end
end
