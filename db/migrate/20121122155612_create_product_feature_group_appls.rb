class CreateProductFeatureGroupAppls < ActiveRecord::Migration
  def change
    create_table :product_feature_group_appls do |t|
      t.integer :product_feature_group_id
      t.integer :product_feature_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :sequence_num

      t.timestamps
    end
  end
end
