class CreateRequirementParties < ActiveRecord::Migration
  def change
    create_table :requirement_parties do |t|
      t.integer :requirement_id
      t.integer :part_id
      t.integer :role_type_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
