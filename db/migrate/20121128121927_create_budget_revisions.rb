class CreateBudgetRevisions < ActiveRecord::Migration
  def change
    create_table :budget_revisions do |t|
    	t.integer :budget_id
    	t.integer :revision_seq_id
    	t.date :date_revision
    	t.string :description

      t.timestamps
    end
  end
end
