class CreateSalesOpportunityTrackings < ActiveRecord::Migration
  def change
    create_table :sales_opportunity_trackings do |t|
      t.integer :sales_opportunity_id
      t.integer :tracking_code_id
      t.datetime :received_date

      t.timestamps
    end
  end
end
