class CreateCustomMethodTypes < ActiveRecord::Migration
  def change
    create_table :custom_method_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
