class CreateBillingAccountTermTypes < ActiveRecord::Migration
  def change
    create_table :billing_account_term_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
