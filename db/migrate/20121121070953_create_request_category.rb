class CreateRequestCategory < ActiveRecord::Migration
  def create
	  create_table "request_category", :force => true do |t|
	    t.integer   "parent_category_id"
	    t.integer   "responsible_party_id"
	    t.datetime "created_at",                    :null => false
	    t.datetime "updated_at",                    :null => false
	  end
  end
end
