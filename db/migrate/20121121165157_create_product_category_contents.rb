class CreateProductCategoryContents < ActiveRecord::Migration
  def change
    create_table :product_category_contents do |t|
      t.integer :product_category_id
      t.integer :content_location
      t.integer :category_content_type_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
    add_index :product_category_contents, :product_category_id, :name => 'PRDCAT_CNT_CTTP'
  end
end
