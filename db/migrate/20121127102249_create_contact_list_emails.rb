class CreateContactListEmails < ActiveRecord::Migration
  def change
    create_table :contact_list_emails do |t|
      t.integer :contact_list_id
      t.integer :email_type_enum_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :email_templated_id

      t.timestamps
    end
  end
end
