class CreateLots < ActiveRecord::Migration
  def change
    create_table :lots do |t|
      t.datetime :creation_date
      t.decimal :quantity
      t.datetime :expiration_date

      t.timestamps
    end
  end
end
