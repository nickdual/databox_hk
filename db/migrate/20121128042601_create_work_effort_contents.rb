class CreateWorkEffortContents < ActiveRecord::Migration
  def change
    create_table :work_effort_contents do |t|
      t.integer :work_effort_id
      t.string :content_location
      t.integer :content_type_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
