class CreateProductReviews < ActiveRecord::Migration
  def change
    create_table :product_reviews do |t|
      t.integer :product_store_id
      t.integer :product_id
      t.integer :user_id
      t.integer :status_id
      t.boolean :posted_anonymous
      t.datetime :posted_datetime
      t.decimal :product_rating
      t.text :product_review

      t.timestamps
    end
  end
end
