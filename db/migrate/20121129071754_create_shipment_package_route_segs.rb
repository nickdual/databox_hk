class CreateShipmentPackageRouteSegs < ActiveRecord::Migration
  def change
    create_table :shipment_package_route_segs do |t|
      t.integer :shipment_id
      t.integer :shipment_package_seq_id
      t.integer :shipment_route_segment_id
      t.string :tracking_code
      t.string :box_number
      t.text :label_image
      t.text :label_intl_sign_image
      t.text :label_html
      t.boolean :label_printed
      t.text :international_invoice
      t.decimal :package_transport_cost
      t.decimal :package_service_cost
      t.decimal :package_other_cost
      t.decimal :cod_amount
      t.decimal :insured_amount
      t.integer :amount_uom_id

      t.timestamps
    end
  end
end
