class CreateFinancialAccountReplenishMethods < ActiveRecord::Migration
  def change
    create_table :financial_account_replenish_methods do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
