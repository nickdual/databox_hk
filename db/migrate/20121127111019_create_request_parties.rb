class CreateRequestParties < ActiveRecord::Migration
  def change
    create_table :request_parties do |t|
      t.integer :request_id
      t.integer :party_id
      t.integer :role_type_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
