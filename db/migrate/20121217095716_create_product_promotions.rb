class CreateProductPromotions < ActiveRecord::Migration
  def change
    create_table :product_promotions do |t|
      t.integer :billback_factor
      t.integer :override_org_party_id
      t.string :promotion_name , :null => false
      t.boolean :promotion_show_to_customer
      t.text :promotion_text
      t.boolean :require_code
      t.integer :use_limit_per_customer
      t.integer :use_limit_per_order
      t.integer :use_limit_per_promotion
      t.boolean :user_entered
      t.integer :created_by_user_login
      t.integer :last_modified_by_user_login
      t.integer :user_id
      t.timestamps
    end
  end
end
