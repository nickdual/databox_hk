class CreatePromotionCodeParties < ActiveRecord::Migration
  def change
    create_table :promotion_code_parties do |t|
      t.integer :party_id
      t.string :product_promotion_code_id

      t.timestamps
    end
  end
end
