class CreateGlReconciliationStatuses < ActiveRecord::Migration
  def change
    create_table :gl_reconciliation_statuses do |t|
      t.string :status_id
      t.integer :sequence_num
      t.string :description

      t.timestamps
    end
  end
end
