class CreatePartyRelationships < ActiveRecord::Migration
  def change
    create_table :party_relationships do |t|
      t.integer :relationship_type_id
      t.integer :from_party_id
      t.integer :from_role_type_id
      t.integer :to_party_id
      t.integer :to_role_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :status_id
      t.integer :priority_type_id
      t.integer :permissions_id
      t.text :comments
      t.string :relationship_name

      t.timestamps
    end
  end
end
