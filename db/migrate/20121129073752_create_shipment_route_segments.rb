class CreateShipmentRouteSegments < ActiveRecord::Migration
  def change
    create_table :shipment_route_segments do |t|
      t.integer :shipment_id
      t.integer :delivery_id
      t.integer :origin_facility_id
      t.integer :origin_contact_mech_id
      t.integer :origin_telecom_number_id
      t.integer :dest_facility_id
      t.integer :dest_contact_mech_id
      t.integer :dest_telecom_number_id
      t.integer :carrier_party_id
      t.integer :shipment_method_id
      t.integer :status_id
      t.string :carrier_delivery_zone
      t.string :carrier_restriction_codes
      t.text :carrier_restriction_desc
      t.decimal :billing_weight
      t.integer :billing_weight_uom_id
      t.decimal :actual_transport_cost
      t.decimal :actual_service_cost
      t.decimal :actual_other_cost
      t.decimal :actual_cost
      t.integer :cost_uom_id
      t.datetime :actual_start_date
      t.datetime :actual_arrival_date
      t.datetime :estimated_start_date
      t.datetime :estimated_arrival_date
      t.string :tracking_id_number
      t.text :tracking_digest
      t.integer :home_delivery_type
      t.datetime :home_delivery_date
      t.integer :third_party_account_number
      t.integer :third_party_postal_code
      t.integer :third_party_country_geo_code
      t.text :ups_high_value_report
      t.integer :billing_weight_id

      t.timestamps
    end
  end
end
