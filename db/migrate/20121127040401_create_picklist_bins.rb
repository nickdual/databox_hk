class CreatePicklistBins < ActiveRecord::Migration
  def change
    create_table :picklist_bins do |t|
      t.integer :picklist_id
      t.integer :bin_location_number
      t.integer :primary_order_id
      t.integer :primary_order_part_seq_id

      t.timestamps
    end
  end
end
