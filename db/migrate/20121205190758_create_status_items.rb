class CreateStatusItems < ActiveRecord::Migration
  def change
    create_table :status_items do |t|
      t.string :status_id
      t.string :description
      t.integer :sequence_num

      t.timestamps
    end
  end
end
