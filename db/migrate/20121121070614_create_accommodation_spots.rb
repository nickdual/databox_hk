class CreateAccommodationSpots < ActiveRecord::Migration
  def change
    create_table :accommodation_spots do |t|
      t.integer :accommodation_class_id
      t.integer :asset_id
      t.integer :number_of_spaces
      t.string :description

      t.timestamps
    end
  end
end
