class CreateProductCategoryMembers < ActiveRecord::Migration
  def change
    create_table :product_category_members do |t|
      t.integer :product_category_id
      t.integer :product_id
      t.datetime :from_date
      t.datetime :thru_date
      t.text :comments
      t.integer :sequence_num
      t.decimal :quantity

      t.timestamps
    end
    add_index :product_category_members, :product_category_id, :name => 'PRD_CMBR_PCT'
  end
end
