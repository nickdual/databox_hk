class CreateProductStoreEmails < ActiveRecord::Migration
  def change
    create_table :product_store_emails do |t|
      t.integer :product_store_id
      t.integer :email_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :email_template_id

      t.timestamps
    end
  end
end
