class CreateRoleTypes < ActiveRecord::Migration
  def change
    create_table :role_types do |t|
      t.integer :parent_type_id
      t.text :description
      t.string :name
      t.timestamps
    end
  end
end
