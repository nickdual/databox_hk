class CreateProductMaintenances < ActiveRecord::Migration
  def change
    create_table :product_maintenances do |t|
      t.integer :product_id
      t.integer :maintenance_type_id
      t.text :description
      t.integer :template_work_effort_id
      t.decimal :interval_quantity
      t.integer :interval_uom_id
      t.integer :interval_meter_type_id
      t.integer :repeat_count

      t.timestamps
    end
  end
end
