class CreateAcctgTransEntries < ActiveRecord::Migration
	def change
		create_table :acctg_trans_entries do |t|
			t.integer :acctg_trans_id
			t.integer :acctg_trans_entry_seq_id
			t.string :description
			t.string :voucher_ref
			t.integer :party_id
			t.integer :role_type_id
			t.integer :their_party_id
			t.integer :product_id
			t.integer :their_product_id
			t.integer :asset_id
			t.integer :gl_account_type_enum_id
			t.integer :gl_account_id
			t.integer :organization_party_id
			t.float :amount
			t.integer :amoun_uom_id
			t.float :orig_currency_amount
			t.integer :orig_currency_uom_id
			t.boolean :debit_credit_flag
			t.date :due_date
			t.integer :group_id
			t.integer :tax_id
			t.string :reconcile_status_id
			t.integer :settlement_term_id
			t.boolean :is_summary

			t.timestamps
		end
	end
end
