class CreateGlAccountGroupMembers < ActiveRecord::Migration
  def change
    create_table :gl_account_group_members do |t|
    	t.integer :gl_account_id
    	t.enum :gl_account_group_type_enum_id
    	t.integer :gl_account_group_id

      t.timestamps
    end
  end
end
