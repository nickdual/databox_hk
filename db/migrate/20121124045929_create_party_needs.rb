class CreatePartyNeeds < ActiveRecord::Migration
  def change
    create_table :party_needs do |t|
      t.integer :need_type_id
      t.integer :party_id
      t.integer :role_type_id
      t.integer :communication_event_id
      t.integer :product_id
      t.integer :product_category_id
      t.integer :visit_id
      t.datetime :datetime_recorded
      t.text :description

      t.timestamps
    end
  end
end
