class CreateProductCalculatedInfos < ActiveRecord::Migration
  def change
    create_table :product_calculated_infos do |t|
      t.integer :product_id
      t.decimal :total_quantity_ordered
      t.integer :total_times_viewed
      t.decimal :average_customer_rating

      t.timestamps
    end
  end
end
