class CreatePartyClassifications < ActiveRecord::Migration
  def change
    create_table :party_classifications do |t|
      t.integer :classification_type_id
      t.integer :parent_classification_id
      t.text :description

      t.timestamps
    end
  end
end
