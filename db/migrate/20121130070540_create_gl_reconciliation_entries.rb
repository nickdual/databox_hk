class CreateGlReconciliationEntries < ActiveRecord::Migration
  def change
    create_table :gl_reconciliation_entries do |t|
    	t.string :gl_reconciliation_id
    	t.integer :acctg_trans_id
    	t.integer :acctg_trans_entry_seq_id
    	t.float :reconciled_amount
    	
      t.timestamps
    end
  end
end
