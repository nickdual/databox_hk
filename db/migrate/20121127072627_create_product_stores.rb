class CreateProductStores < ActiveRecord::Migration
  def change
    create_table :product_stores do |t|
      t.string :store_name
      t.integer :organization_party_id
      t.integer :inventory_facility_id
      t.integer :reservation_order_id
      t.integer :requirement_method_id
      t.text :default_locale
      t.integer :default_currency_uom_id
      t.integer :default_sales_channel_id
      t.boolean :require_customer_role

      t.timestamps
    end
  end
end
