class CreateSalesOpportunityWorkEfforts < ActiveRecord::Migration
  def change
    create_table :sales_opportunity_work_efforts do |t|
      t.integer :sales_opportunity_id
      t.integer :work_effort_id

      t.timestamps
    end
  end
end
