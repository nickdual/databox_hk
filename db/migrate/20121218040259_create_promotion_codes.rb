class CreatePromotionCodes < ActiveRecord::Migration
  def change
    create_table :promotion_codes do |t|
      t.string :product_promotion_code_id
      t.string :product_promotion_id
      t.boolean :user_entered
      t.boolean :require_email_or_party
      t.integer :use_limit_per_code
      t.integer :use_limit_per_customer
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :created_by_user_login
      t.integer :last_modified_by_user_login
      t.timestamps
    end
  end
end
