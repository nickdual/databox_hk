class CreateOrderNotes < ActiveRecord::Migration
  def change
    create_table :order_notes do |t|
      t.integer :order_id
      t.datetime :note_date
      t.text :note_text
      t.boolean :internal_note

      t.timestamps
    end
  end
end
