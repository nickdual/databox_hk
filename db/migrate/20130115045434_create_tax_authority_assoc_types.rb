class CreateTaxAuthorityAssocTypes < ActiveRecord::Migration
  def change
    create_table :tax_authority_assoc_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
