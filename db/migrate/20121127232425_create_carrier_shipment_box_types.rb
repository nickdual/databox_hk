class CreateCarrierShipmentBoxTypes < ActiveRecord::Migration
  def change
    create_table :carrier_shipment_box_types do |t|
      t.integer :carrier_party_id
      t.integer :shipment_box_type_id
      t.integer :packaging_type_code
      t.string :oversize_code

      t.timestamps
    end
  end
end
