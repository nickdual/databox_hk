class CreateOrderAdjustmentBillings < ActiveRecord::Migration
  def change
    create_table :order_adjustment_billings do |t|
      t.integer :order_adjustment_id
      t.integer :invoice_id
      t.integer :invoice_item_seq_id
      t.decimal :amount

      t.timestamps
    end
  end
end
