class CreateTelecomNumbers < ActiveRecord::Migration
  def change
    create_table :telecom_numbers do |t|
      t.integer :contact_mech_id
      t.string :country_code
      t.string :area_code
      t.string :contact_number
      t.string :ask_for_name

      t.timestamps
    end
    add_index :telecom_numbers, [:area_code, :contact_number], :name => 'AREA_CONTACT_IDX'
  end
end
