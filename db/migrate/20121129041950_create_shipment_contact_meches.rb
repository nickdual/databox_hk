class CreateShipmentContactMeches < ActiveRecord::Migration
  def change
    create_table :shipment_contact_meches do |t|
      t.integer :shipment_id
      t.integer :contact_mech_purpose_id
      t.integer :contact_mech_id

      t.timestamps
    end
  end
end
