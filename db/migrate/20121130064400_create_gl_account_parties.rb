class CreateGlAccountParties < ActiveRecord::Migration
  def change
    create_table :gl_account_parties do |t|
    	t.integer :gl_account_id
    	t.integer :party_id
    	t.integer :role_type_id
    	t.date :from_date
    	t.date :thru_date

      t.timestamps
    end
  end
end
