class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :invoice_id
      t.integer :invoice_type_enum_id
      t.integer :from_party_id
      t.integer :to_party_id
      t.integer :status_id
      t.integer :billing_account_id
      t.datetime :invoice_date
      t.datetime :due_date
      t.datetime :paid_date
      t.text :invoice_message
      t.text :reference_number
      t.text :description
      t.integer :currency_uom_id

      t.timestamps
    end
  end
end
