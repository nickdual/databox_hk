class CreateTaxAuthorityRateTypes < ActiveRecord::Migration
  def change
    create_table :tax_authority_rate_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
