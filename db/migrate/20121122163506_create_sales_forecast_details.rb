class CreateSalesForecastDetails < ActiveRecord::Migration
  def change
    create_table :sales_forecast_details do |t|
      t.integer :sales_forecast_id
      t.integer :sales_forecast_detail_seq_id
      t.decimal :amount
      t.decimal :quantity
      t.integer :quantity_uom_id
      t.integer :product_id
      t.integer :product_category_id

      t.timestamps
    end
  end
end
