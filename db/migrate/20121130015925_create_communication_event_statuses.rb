class CreateCommunicationEventStatuses < ActiveRecord::Migration
  def change
    create_table :communication_event_statuses do |t|
      t.string :description
      t.string :name
      t.integer :sequence_num

      t.timestamps
    end
  end
end
