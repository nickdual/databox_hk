class CreatePromotionRuleActions < ActiveRecord::Migration
  def change
    create_table :promotion_rule_actions do |t|
      t.integer :promotion_rule_id
      t.enum :promotion_rule_action_name
      t.integer :quantity
      t.integer :amount
      t.integer :item_id
      t.string :party_id
      t.string :service_name
      t.boolean :use_cart_quantity

      t.timestamps
    end
  end
end
