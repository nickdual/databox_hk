class CreateProductStoreFacilities < ActiveRecord::Migration
  def change
    create_table :product_store_facilities do |t|
      t.integer :product_store_id
      t.integer :facility_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :sequence_num

      t.timestamps
    end
  end
end
