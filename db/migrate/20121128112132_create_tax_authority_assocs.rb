class CreateTaxAuthorityAssocs < ActiveRecord::Migration
	def change
		create_table :tax_authority_assocs do |t|
			t.integer :tax_auth_geo_id
			t.integer :tax_auth_party_id
			t.integer :to_tax_auth_geo_id
			t.integer :to_tax_auth_party_id
			t.date :from_date
			t.date :thru_date
			t.enum :assoc_type_enum_id

			t.timestamps
		end
	end
end
