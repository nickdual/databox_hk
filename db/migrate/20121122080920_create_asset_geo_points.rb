class CreateAssetGeoPoints < ActiveRecord::Migration
  def change
    create_table :asset_geo_points do |t|
      t.integer :asset_id
      t.integer :geo_point_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
