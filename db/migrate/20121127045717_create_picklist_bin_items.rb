class CreatePicklistBinItems < ActiveRecord::Migration
  def change
    create_table :picklist_bin_items do |t|
      t.integer :picklist_bin_id
      t.integer :picklist_bin_item_seq_id
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :asset_id
      t.integer :status_id
      t.decimal :quantity

      t.timestamps
    end
  end
end
