class CreateFinancialAccountTypeGlAccounts < ActiveRecord::Migration
  def change
    create_table :financial_account_type_gl_accounts do |t|
    	t.integer :fin_account_type_id
    	t.integer :organization_party_id
    	t.integer :gl_account_id

      t.timestamps
    end
  end
end
