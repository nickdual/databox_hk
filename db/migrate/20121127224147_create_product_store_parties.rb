class CreateProductStoreParties < ActiveRecord::Migration
  def change
    create_table :product_store_parties do |t|
      t.integer :product_store_id
      t.integer :party_id
      t.integer :role_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :sequence_num

      t.timestamps
    end
  end
end
