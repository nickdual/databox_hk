class CreateGlAccountTypeDefaults < ActiveRecord::Migration
  def change
    create_table :gl_account_type_defaults do |t|
    	t.enum :gl_account_type_enum_id
    	t.integer :organization_party_id
    	t.integer :gl_accouny_id

      t.timestamps
    end
  end
end
