class CreateGlAccountTypePaymentTypes < ActiveRecord::Migration
  def change
    create_table :gl_account_type_payment_types do |t|
    	t.enum :payment_type_enum_id
    	t.integer :organization_party_id
    	t.enum :gl_account_type_enum_id

      t.timestamps
    end
  end
end
