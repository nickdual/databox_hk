class CreatePostalAddresses < ActiveRecord::Migration
  def change
    create_table :postal_addresses do |t|
      t.integer :contact_mech_id
      t.string :to_name
      t.string :attn_name
      t.string :address1
      t.string :address2
      t.string :unit_number
      t.text :directions
      t.string :city
      t.integer :county_geo_id
      t.integer :state_province_geo_id
      t.integer :country_geo_id
      t.string :postal_code
      t.string :postal_code_ext
      t.integer :postal_code_geo_id
      t.integer :geo_point_id
      t.boolean :commercial
      t.string :access_code
      t.integer :postal_code_id

      t.timestamps
    end
    add_index :postal_addresses, :city, :name => 'CITY_IDX'
    add_index :postal_addresses, :postal_code, :name => 'POSTAL_CODE_IDX'
  end
end
