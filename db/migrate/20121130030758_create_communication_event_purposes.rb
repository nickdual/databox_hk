class CreateCommunicationEventPurposes < ActiveRecord::Migration
  def change
    create_table :communication_event_purposes do |t|
      t.integer :communication_event_id
      t.integer :purpose_id
      t.text :description

      t.timestamps
    end
  end
end
