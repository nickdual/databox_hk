class CreateItemIssuances < ActiveRecord::Migration
  def change
    create_table :item_issuances do |t|
      t.integer :asset_id
      t.integer :asset_reservation_id
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :shipment_id
      t.integer :shipment_item_seq_id
      t.integer :asset_maintenance_id
      t.datetime :issued_datetime
      t.decimal :quantity
      t.decimal :cancel_quantity

      t.timestamps
    end
  end
end
