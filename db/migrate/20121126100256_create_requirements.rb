class CreateRequirements < ActiveRecord::Migration
  def change
    create_table :requirements do |t|
      t.integer :requirement_type_enum_id
      t.integer :status_id
      t.integer :facility_id
      t.integer :deliverable_id
      t.integer :asset_id
      t.integer :product_id
      t.text :description
      t.datetime :requirement_start_date
      t.decimal :estimated_budget, :precision => 10, :scale => 2, :null => false
      t.decimal :quantity, :precision => 10, :scale => 2, :null => false
      t.text :use_case
      t.text :reason

      t.timestamps
    end
  end
end
