class CreateShipmentReceiptParties < ActiveRecord::Migration
  def change
    create_table :shipment_receipt_parties do |t|
      t.integer :shipment_receipt_id
      t.integer :party_id
      t.integer :role_type_id

      t.timestamps
    end
  end
end
