class CreateQualificationTypes < ActiveRecord::Migration
  def change
    create_table :qualification_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
