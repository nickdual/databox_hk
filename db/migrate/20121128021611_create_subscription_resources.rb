class CreateSubscriptionResources < ActiveRecord::Migration
  def change
    create_table :subscription_resources do |t|
      t.integer :parent_resource_id
      t.text :description
      t.text :content_location

      t.timestamps
    end
  end
end
