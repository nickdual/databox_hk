class CreateShipmentReceipts < ActiveRecord::Migration
  def change
    create_table :shipment_receipts do |t|
      t.integer :asset_id
      t.integer :product_id
      t.integer :shipment_id
      t.integer :shipment_item_seq_id
      t.integer :shipment_package_seq_id
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :return_id
      t.integer :return_item_seq_id
      t.integer :rejection_reason_id
      t.integer :received_by_user_id
      t.datetime :datetime_received
      t.text :item_description
      t.decimal :quantity_accepted
      t.decimal :quantity_rejected

      t.timestamps
    end
  end
end
