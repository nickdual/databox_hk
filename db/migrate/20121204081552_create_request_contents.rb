class CreateRequestContents < ActiveRecord::Migration
  def change
    create_table :request_contents do |t|
      t.integer :request_id
      t.text :content_location
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
