class CreateEmplPositionClasses < ActiveRecord::Migration
  def change
    create_table :empl_position_classes do |t|
      t.text :title
      t.text :description
      t.string :name
      t.timestamps
    end
  end
end
