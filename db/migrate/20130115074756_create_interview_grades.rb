class CreateInterviewGrades < ActiveRecord::Migration
  def change
    create_table :interview_grades do |t|
      t.string :description
      t.string :name
      t.integer :sequence_num

      t.timestamps
    end
  end
end
