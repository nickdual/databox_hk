class CreatePaypalAccounts < ActiveRecord::Migration
  def change
    create_table :paypal_accounts do |t|
      t.integer :payment_method_id
      t.integer :payer_id
      t.text :expresss_checkout_token
      t.text :payer_status
      t.boolean :avs_addr
      t.boolean :avs_zip
      t.integer :correlation_id
      t.text :transaction_id

      t.timestamps
    end
  end
end
