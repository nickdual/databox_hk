class CreatePartyRoles < ActiveRecord::Migration
  def change
    create_table :party_roles do |t|
      t.integer :party_id
      t.integer :role_type_id

      t.timestamps
    end
  end
end
