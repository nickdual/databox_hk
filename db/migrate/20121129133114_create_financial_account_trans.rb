class CreateFinancialAccountTrans < ActiveRecord::Migration
  def change
    create_table :financial_account_trans do |t|
      t.integer :fin_account_trans_id
      t.integer :fin_account_id
      t.integer :status_id
      t.integer :party_id
      t.integer :gl_reconciliation_id
      t.datetime :transaction_date
      t.datetime :entry_date
      t.decimal :amount
      t.integer :payment_id
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :performed_by_party_id
      t.integer :reason_enum_id
      t.text :comments

      t.timestamps
    end
  end
end
