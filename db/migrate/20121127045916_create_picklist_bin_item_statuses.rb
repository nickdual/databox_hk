class CreatePicklistBinItemStatuses < ActiveRecord::Migration
  def change
    create_table :picklist_bin_item_statuses do |t|
      t.string :description
      t.integer :sequence_num
      t.string :name

      t.timestamps
    end
  end
end
