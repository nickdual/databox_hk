class CreateAcctgTrans < ActiveRecord::Migration
  def change
    create_table :acctg_trans do |t|
    	t.enum :acctg_trans_type_enum_id
    	t.string :description
    	t.date :transaction_date
    	t.boolean :is_posted
    	t.date :posted_date
    	t.date :scheduled_posting_date
    	t.integer :gl_journal_id
    	t.integer :gl_fiscal_type_enum_id
    	t.string :voucher_ref
    	t.date :voucher_date
    	t.integer :group_status_id
    	t.integer :asset_id
    	t.integer :physical_inventory_id
    	t.integer :party_id
    	t.integer :role_type_id
    	t.integer :invoice_id
    	t.integer :payment_id
    	t.integer :fin_account_trans_id
    	t.integer :shipment_id
    	t.integer :receipt_id
    	t.integer :work_effort_id
    	t.integer :their_acctg_trans_id

      t.timestamps
    end
  end
end
