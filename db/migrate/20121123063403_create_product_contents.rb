class CreateProductContents < ActiveRecord::Migration
  def change
    create_table :product_contents do |t|
      t.integer :product_id
      t.text :content_location
      t.integer :product_content_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :sequence_num

      t.timestamps
    end
  end
end
