class CreateShipmentItemBillings < ActiveRecord::Migration
  def change
    create_table :shipment_item_billings do |t|
      t.integer :shipment_id
      t.integer :shipment_item_seq_id
      t.integer :invoice_id
      t.integer :invoice_item_seq_id

      t.timestamps
    end
  end
end
