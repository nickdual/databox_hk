class CreateTaxAuthorityGlAccounts < ActiveRecord::Migration
  def change
    create_table :tax_authority_gl_accounts do |t|
    	t.integer :tax_auth_geo_id
    	t.integer :tax_auth_party_id
    	t.integer :organization_party_id
    	t.integer :gl_account_id
    	
      t.timestamps
    end
  end
end
