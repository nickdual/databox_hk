class CreateAssetStatuses < ActiveRecord::Migration
  def change
    create_table :asset_statuses do |t|
      t.string :status_id
      t.string :description
      t.integer :sequence_num

      t.timestamps
    end
  end
end
