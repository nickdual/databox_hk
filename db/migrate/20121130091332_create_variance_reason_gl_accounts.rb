class CreateVarianceReasonGlAccounts < ActiveRecord::Migration
  def change
    create_table :variance_reason_gl_accounts do |t|
    	t.enum :variance_reason_enum_id
    	t.integer :organization_party_id
    	t.integer :gl_account_id

      t.timestamps
    end
  end
end
