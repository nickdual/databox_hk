class CreateAssetMaintenanceMeters < ActiveRecord::Migration
  def change
    create_table :asset_maintenance_meters do |t|
      t.integer :asset_maintenance_id
      t.integer :product_meter_type_id
      t.decimal :meter_value

      t.timestamps
    end
  end
end
