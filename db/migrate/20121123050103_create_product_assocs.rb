class CreateProductAssocs < ActiveRecord::Migration
  def change
    create_table :product_assocs do |t|
      t.integer :product_id
      t.integer :to_product_id
      t.integer :product_assoc_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :sequence_num
      t.text :reason
      t.decimal :quantity
      t.decimal :scrap_factor
      t.text :instruction
      t.integer :routing_work_effort_id

      t.timestamps
    end
  end
end
