class CreateProductConfigItemAppls < ActiveRecord::Migration
  def change
    create_table :product_config_item_appls do |t|
      t.integer :product_id
      t.integer :config_item_id
      t.integer :sequence_num
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :config_type_id
      t.text :description
      t.integer :default_config_option_id
      t.boolean :is_mandatory

      t.timestamps
    end
  end
end
