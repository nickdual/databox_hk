class CreateOrderItemWorkEfforts < ActiveRecord::Migration
  def change
    create_table :order_item_work_efforts do |t|
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :work_effort_id

      t.timestamps
    end
  end
end
