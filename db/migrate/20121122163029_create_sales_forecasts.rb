class CreateSalesForecasts < ActiveRecord::Migration
  def change
    create_table :sales_forecasts do |t|
      t.integer :parent_sales_forecast_id
      t.integer :organization_party_id
      t.integer :internal_party_id
      t.integer :time_period_id
      t.integer :currency_uom_id
      t.decimal :quota_amount
      t.decimal :forecast_amount
      t.decimal :best_case_amount
      t.decimal :closed_amount
      t.decimal :percent_of_quota_forecast
      t.decimal :percent_of_quota_closed
      t.decimal :pipeline_amount

      t.timestamps
    end
  end
end
