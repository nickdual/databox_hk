class CreateBudgetRevisionImpacts < ActiveRecord::Migration
  def change
    create_table :budget_revision_impacts do |t|
    	t.integer :budget_id
    	t.integer :budget_item_seq_id
    	t.integer :revision_seq_id
    	t.float :revised_amount
    	t.boolean :add_delete_flag
    	t.string :revision_reason

      t.timestamps
    end
  end
end