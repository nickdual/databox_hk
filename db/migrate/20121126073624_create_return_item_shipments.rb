class CreateReturnItemShipments < ActiveRecord::Migration
  def change
    create_table :return_item_shipments do |t|
      t.integer :return_id
      t.integer :return_item_seq_id
      t.integer :shipment_id
      t.integer :shipment_item_seq_id
      t.decimal :quantity

      t.timestamps
    end
  end
end
