class CreateWorkEffortAssocTypes < ActiveRecord::Migration
  def change
    create_table :work_effort_assoc_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
