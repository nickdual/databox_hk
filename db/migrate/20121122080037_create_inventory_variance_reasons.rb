class CreateInventoryVarianceReasons < ActiveRecord::Migration
  def change
    create_table :inventory_variance_reasons do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
