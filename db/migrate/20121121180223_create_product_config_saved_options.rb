class CreateProductConfigSavedOptions < ActiveRecord::Migration
  def change
    create_table :product_config_saved_options do |t|
      t.integer :product_config_saved_id
      t.integer :config_item_id
      t.integer :config_option_seq_id
      t.integer :product_id
      t.decimal :quantity

      t.timestamps
    end
  end
end
