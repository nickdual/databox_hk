class CreateMarketSegments < ActiveRecord::Migration
  def change
    create_table :market_segments do |t|
      t.integer :market_segment_id
      t.integer :market_segment_type_enum_id
      t.text :description
      t.integer :product_store_id

      t.timestamps
    end
  end
end
