class CreatePartyContentTypes < ActiveRecord::Migration
  def change
    create_table :party_content_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
