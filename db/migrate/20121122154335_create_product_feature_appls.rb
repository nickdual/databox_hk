class CreateProductFeatureAppls < ActiveRecord::Migration
  def change
    create_table :product_feature_appls do |t|
      t.integer :product_id
      t.integer :product_feature_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :appl_type_id
      t.integer :sequence_num
      t.decimal :amount
      t.decimal :recurring_amount

      t.timestamps
    end
  end
end
