class CreateRequestCommEvents < ActiveRecord::Migration
  def change
    create_table :request_comm_events do |t|
      t.integer :request_id
      t.integer :communication_event_id

      t.timestamps
    end
  end
end
