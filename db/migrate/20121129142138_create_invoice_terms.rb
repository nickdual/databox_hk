class CreateInvoiceTerms < ActiveRecord::Migration
  def change
    create_table :invoice_terms do |t|
      t.integer :invoice_id
      t.integer :invoice_item_seq_id
      t.integer :settlement_term_id

      t.timestamps
    end
  end
end
