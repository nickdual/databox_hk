class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :payment_id
      t.integer :payment_type_enum_id
      t.integer :from_party_id
      t.integer :to_party_id
      t.integer :payment_method_type_enum_id
      t.integer :payment_method_id
      t.integer :order_id
      t.integer :status_id
      t.datetime :effective_data
      t.text :payment_auth_code
      t.text :payment_ref_num
      t.decimal :amount, :precision => 10, :scale => 2,:null => false
      t.integer :amount_uom_id
      t.text :comments
      t.integer :fin_account_auth_id
      t.integer :fin_account_trans_id
      t.integer :override_gl_account_id
      t.decimal :original_currency_amount, :precision => 10, :scale => 2,:null => false
      t.integer :original_currency_uom_id
      t.boolean :present_flag
      t.boolean :swiped_flag
      t.integer :process_attempt
      t.boolean :needs_nsf_retry

      t.timestamps
    end
  end
end
