class CreateAssetPartyAssignments < ActiveRecord::Migration
  def change
    create_table :asset_party_assignments do |t|
      t.integer :asset_id
      t.integer :party_id
      t.integer :role_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.datetime :allocated_date
      t.integer :status_id
      t.text :comments

      t.timestamps
    end
  end
end
