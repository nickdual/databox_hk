class CreatePicklistParties < ActiveRecord::Migration
  def change
    create_table :picklist_parties do |t|
      t.integer :picklist_id
      t.integer :party_id
      t.integer :role_type_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
