class CreateGlAccountGroups < ActiveRecord::Migration
  def change
    create_table :gl_account_groups do |t|
    	t.integer :gl_account_group_type_enum_id
    	t.string :description

      t.timestamps
    end
  end
end
