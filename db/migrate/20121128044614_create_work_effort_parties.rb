class CreateWorkEffortParties < ActiveRecord::Migration
  def change
    create_table :work_effort_parties do |t|
      t.integer :work_effort_id
      t.integer :party_id
      t.integer :role_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :status_id
      t.integer :availability_id
      t.integer :delegate_reason_id
      t.integer :expectation_id
      t.text :comments
      t.boolean :must_rsvp

      t.timestamps
    end
  end
end
