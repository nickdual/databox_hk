class CreateTerminationTypes < ActiveRecord::Migration
  def change
    create_table :termination_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
