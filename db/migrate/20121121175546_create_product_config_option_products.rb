class CreateProductConfigOptionProducts < ActiveRecord::Migration
  def change
    create_table :product_config_option_products do |t|
      t.integer :config_item_id
      t.integer :config_option_seq_id
      t.integer :product_id
      t.decimal :quantity
      t.integer :sequence_num

      t.timestamps
    end
  end
end
