class CreateWorkEfforts < ActiveRecord::Migration
  def change
    create_table :work_efforts do |t|
      t.string :universal_id
      t.integer :source_reference_id
      t.integer :parent_work_effort_id
      t.integer :work_effort_type_id
      t.integer :purpose_id
      t.integer :visibility_id
      t.integer :status_id
      t.integer :priority
      t.boolean :send_notification_email
      t.integer :percent_complete
      t.integer :revision_number
      t.string :work_effort_name
      t.text :description
      t.text :location
      t.integer :facility_id
      t.string :info_url
      t.datetime :estimated_start_date
      t.datetime :estimated_completion_date
      t.datetime :actual_start_date
      t.datetime :actual_completion_date
      t.float :estimated_work_time
      t.float :estimated_setup_time
      t.float :actual_work_time
      t.float :actual_setup_time
      t.float :total_time_allowed
      t.integer :time_uom_id
      t.integer :recurrence_info_id

      t.timestamps
    end
  end
end
