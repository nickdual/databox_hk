class CreateWorkEffortAssocs < ActiveRecord::Migration
  def change
    create_table :work_effort_assocs do |t|
      t.integer :work_effort_id
      t.integer :to_work_effort_id
      t.integer :work_effort_assoc_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :sequence_num

      t.timestamps
    end
  end
end
