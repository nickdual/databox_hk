class CreateGlAccountTypes < ActiveRecord::Migration
  def change
    create_table :gl_account_types do |t|
      t.string :name
      t.string :description
      t.integer :parent_id

      t.timestamps
    end
  end
end
