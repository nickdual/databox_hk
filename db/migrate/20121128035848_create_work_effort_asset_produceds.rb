class CreateWorkEffortAssetProduceds < ActiveRecord::Migration
  def change
    create_table :work_effort_asset_produceds do |t|
      t.integer :work_effort_id
      t.integer :asset_id

      t.timestamps
    end
  end
end
