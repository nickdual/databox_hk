class CreateOrderShipments < ActiveRecord::Migration
  def change
    create_table :order_shipments do |t|
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :shipment_id
      t.integer :shipment_item_seq_id
      t.decimal :quantity

      t.timestamps
    end
  end
end
