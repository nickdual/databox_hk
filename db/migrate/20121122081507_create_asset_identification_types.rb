class CreateAssetIdentificationTypes < ActiveRecord::Migration
  def change
    create_table :asset_identification_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
