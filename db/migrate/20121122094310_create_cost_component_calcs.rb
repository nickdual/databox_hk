class CreateCostComponentCalcs < ActiveRecord::Migration
  def change
    create_table :cost_component_calcs do |t|
      t.string :description
      t.integer :cost_gl_account_type_id
      t.integer :offset_gl_account_type_id
      t.decimal :fixed_cost
      t.decimal :variable_cost
      t.integer :per_milli_second
      t.integer :cost_uom_id

      t.timestamps
    end
  end
end
