class CreateInvoiceItemAssocs < ActiveRecord::Migration
  def change
    create_table :invoice_item_assocs do |t|
      t.integer :invoice_item_assoc_id
      t.integer :invoice_id
      t.integer :invoice_item_seq_id
      t.integer :to_invoice_id
      t.integer :to_invoice_item_seq_id
      t.integer :invoice_item_assoc_type_enum_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :from_party_id
      t.integer :to_party_id
      t.decimal :quantity
      t.decimal :amount, :precision => 10, :scale => 2, :null => false

      t.timestamps
    end
  end
end
