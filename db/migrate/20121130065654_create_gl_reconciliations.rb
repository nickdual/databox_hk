class CreateGlReconciliations < ActiveRecord::Migration
	def change
		create_table :gl_reconciliations do |t|
			t.string :gl_reconciliation_id
			t.integer :gl_reconciliation_name
			t.string :description
			t.integer :gl_account_id
			t.integer :status_id
			t.integer :organization_party_id
			t.float :reconciled_balance
			t.float :opening_balance
			t.date :reconciled_date

			t.timestamps
		end
	end
end
