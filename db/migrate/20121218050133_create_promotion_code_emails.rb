class CreatePromotionCodeEmails < ActiveRecord::Migration
  def change
    create_table :promotion_code_emails do |t|
      t.string :email
      t.string :product_promotion_code_id

      t.timestamps
    end
  end
end
