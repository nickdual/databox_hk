class CreateCommunicationEventProducts < ActiveRecord::Migration
  def change
    create_table :communication_event_products do |t|
      t.integer :product_id
      t.integer :communication_event_id

      t.timestamps
    end
  end
end
