class CreateContactListEmailTypes < ActiveRecord::Migration
  def change
    create_table :contact_list_email_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
