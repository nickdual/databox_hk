class CreateContactMechPurposes < ActiveRecord::Migration
  def change
    create_table :contact_mech_purposes do |t|
      t.integer :contact_mech_type_id
      t.text :description
      t.string :name
      t.timestamps
    end
  end
end
