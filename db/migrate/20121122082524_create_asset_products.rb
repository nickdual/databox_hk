class CreateAssetProducts < ActiveRecord::Migration
  def change
    create_table :asset_products do |t|
      t.integer :asset_id
      t.integer :product_id
      t.integer :asset_product_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.string :comments
      t.integer :sequence_num
      t.decimal :quantity
      t.integer :quantity_uom_id

      t.timestamps
    end
  end
end
