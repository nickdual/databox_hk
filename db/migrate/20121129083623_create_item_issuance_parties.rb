class CreateItemIssuanceParties < ActiveRecord::Migration
  def change
    create_table :item_issuance_parties do |t|
      t.integer :item_issuance_id
      t.integer :party_id
      t.integer :role_type_id

      t.timestamps
    end
  end
end
