class CreateProductSubscriptionResources < ActiveRecord::Migration
  def change
    create_table :product_subscription_resources do |t|
      t.integer :product_id
      t.integer :subscription_resource_id
      t.datetime :from_date
      t.datetime :thru_date
      t.datetime :purchase_from_date
      t.datetime :purchase_thru_date
      t.integer :available_time
      t.integer :available_time_uom_id
      t.integer :use_count_limit
      t.integer :use_time
      t.integer :use_time_uom_id
      t.integer :use_role_type_id

      t.timestamps
    end
  end
end
