class CreateOrderCommunicationEvents < ActiveRecord::Migration
  def change
    create_table :order_communication_events do |t|
      t.integer :order_id
      t.integer :communication_event_id

      t.timestamps
    end
  end
end
