class CreateProductFacilityLocation < ActiveRecord::Migration
  def create
	  create_table "product_facility_location", :force => true do |t|
	    t.integer   "product_id"
	    t.integer   "facility_id"
	    t.integer   "location_seq_id"
	    t.integer   "minimum_stock"
	    t.integer   "move_quantity"
	    t.datetime "created_at",                    :null => false
	    t.datetime "updated_at",                    :null => false
	  end
  end
end
