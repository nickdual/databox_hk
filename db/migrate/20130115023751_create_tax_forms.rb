class CreateTaxForms < ActiveRecord::Migration
  def change
    create_table :tax_forms do |t|
      t.string :name
      t.string :description
      t.integer :sequence_num

      t.timestamps
    end
  end
end
