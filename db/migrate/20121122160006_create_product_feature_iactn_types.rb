class CreateProductFeatureIactnTypes < ActiveRecord::Migration
  def change
    create_table :product_feature_iactn_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
