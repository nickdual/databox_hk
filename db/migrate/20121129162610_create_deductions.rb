class CreateDeductions < ActiveRecord::Migration
  def change
    create_table :deductions do |t|
      t.integer :deduction_id
      t.integer :deduction_type_enum_id
      t.integer :payment_id
      t.decimal :amount, :precision => 10, :scale => 2,:null => false

      t.timestamps
    end
  end
end
