class CreateAssetDetails < ActiveRecord::Migration
  def change
    create_table :asset_details do |t|
      t.integer :asset_id
      t.integer :asset_detail_seq_id
      t.datetime :effective_date
      t.decimal :quantity_on_hand_diff
      t.decimal :available_to_promise_diff
      t.decimal :unit_cost
      t.integer :asset_reservation_id
      t.integer :shipment_id
      t.integer :shipment_item_seq_id
      t.integer :return_id
      t.integer :return_item_seq_id
      t.integer :work_effort_id
      t.integer :asset_id
      t.integer :asset_maintenance_id
      t.integer :item_issuance_id
      t.integer :receipt_id
      t.integer :physical_inventory_id
      t.integer :variance_reason_id
      t.text :description

      t.timestamps
    end
  end
end
