class CreateReturnResponses < ActiveRecord::Migration
  def change
    create_table :return_responses do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
