class CreateWorkEffortProducts < ActiveRecord::Migration
  def change
    create_table :work_effort_products do |t|
      t.integer :work_effort_id
      t.integer :product_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :type_id
      t.integer :status_id
      t.float :estimated_quantity
      t.decimal :estimated_cost

      t.timestamps
    end
  end
end
