class CreateGlAccountCategoryMembers < ActiveRecord::Migration
  def change
    create_table :gl_account_category_members do |t|
    	t.integer :gl_account_id
    	t.integer :gl_account_category_id
    	t.date :from_date
    	t.date :thru_date
    	t.float :amount_percentage

      t.timestamps
    end
  end
end
