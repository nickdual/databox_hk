class CreateWorkEffortStatuses < ActiveRecord::Migration
  def change
    create_table :work_effort_statuses do |t|
      t.string :description
      t.integer :sequence_num
      t.string :name

      t.timestamps
    end
  end
end
