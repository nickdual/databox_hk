class CreatePartyIdentification < ActiveRecord::Migration
  def create
	  create_table "party_identification", :force => true do |t|
	    t.integer   "party_id"
	    t.integer   "party_id_type_id"
	    t.integer   "id_value"
	    t.date   "expire_date"
	    t.datetime "created_at",                    :null => false
	    t.datetime "updated_at",                    :null => false
	  end
  end
end
