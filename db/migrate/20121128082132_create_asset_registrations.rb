class CreateAssetRegistrations < ActiveRecord::Migration
  def change
    create_table :asset_registrations do |t|
      t.integer :asset_id
      t.datetime :from_date
      t.datetime :thru_date
      t.datetime :registration_date
      t.integer :gov_agency_party_id
      t.string :registration_number
      t.string :license_number

      t.timestamps
    end
  end
end
