class CreateBudgetParties < ActiveRecord::Migration
  def change
    create_table :budget_parties do |t|
    	t.integer :budget_id
    	t.integer :party_id
    	t.integer :role_type_id    	

      t.timestamps
    end
  end
end
