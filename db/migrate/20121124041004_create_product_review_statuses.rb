class CreateProductReviewStatuses < ActiveRecord::Migration
  def change
    create_table :product_review_statuses do |t|
      t.string :description
      t.integer :sequence_num
      t.string :status_id

      t.timestamps
    end
  end
end
