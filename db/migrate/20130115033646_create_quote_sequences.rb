class CreateQuoteSequences < ActiveRecord::Migration
  def change
    create_table :quote_sequences do |t|
      t.string :description
      t.string :name
      t.integer :sequence_num

      t.timestamps
    end
  end
end
