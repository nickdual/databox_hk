class CreateProductFeatures < ActiveRecord::Migration
  def change
    create_table :product_features do |t|
      t.integer :product_feature_type_id
      t.text :description
      t.decimal :number_specified
      t.integer :number_uom_id
      t.decimal :default_amount
      t.integer :default_sequence_num
      t.integer :abbrev
      t.integer :id_code

      t.timestamps
    end
  end
end
