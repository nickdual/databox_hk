class CreateAcctgTransTypes < ActiveRecord::Migration
  def change
    create_table :acctg_trans_types do |t|
      t.string :name
      t.string :description
      t.integer :parent_id

      t.timestamps
    end
  end
end
