class CreateFinancialAccountParties < ActiveRecord::Migration
  def change
    create_table :financial_account_parties do |t|
      t.integer :fin_account_id
      t.integer :party_id
      t.integer :role_type_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
