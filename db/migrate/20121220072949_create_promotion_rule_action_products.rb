class CreatePromotionRuleActionProducts < ActiveRecord::Migration
  def change
    create_table :promotion_rule_action_products do |t|
      t.integer :product_id
      t.enum :sub_category_status
      t.integer :promotion_rule_action_id

      t.timestamps
    end
  end
end
