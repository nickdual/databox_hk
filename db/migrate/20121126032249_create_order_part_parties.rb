class CreateOrderPartParties < ActiveRecord::Migration
  def change
    create_table :order_part_parties do |t|
      t.integer :order_id
      t.integer :order_part_seq_id
      t.integer :party_id
      t.integer :role_type_id

      t.timestamps
    end
  end
end
