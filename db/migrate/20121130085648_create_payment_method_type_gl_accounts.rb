class CreatePaymentMethodTypeGlAccounts < ActiveRecord::Migration
  def change
    create_table :payment_method_type_gl_accounts do |t|
    	t.integer :payment_method_type_enum_id
    	t.integer :organization_party_id
    	t.integer :gl_account_id

      t.timestamps
    end
  end
end
