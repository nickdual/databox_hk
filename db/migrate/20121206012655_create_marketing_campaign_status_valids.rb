class CreateMarketingCampaignStatusValids < ActiveRecord::Migration
  def change
    create_table :marketing_campaign_status_valids do |t|
      t.string :status_id
      t.string :to_status_id
      t.string :transition_name

      t.timestamps
    end
  end
end
