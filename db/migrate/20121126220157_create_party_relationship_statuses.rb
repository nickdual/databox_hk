class CreatePartyRelationshipStatuses < ActiveRecord::Migration
  def change
    create_table :party_relationship_statuses do |t|

      t.timestamps
    end
  end
end
