class CreateGlAccountHistories < ActiveRecord::Migration
  def change
    create_table :gl_account_histories do |t|
    	t.integer :gl_account_id
    	t.integer :organization_party_id
    	t.integer :time_period_id
    	t.float :posted_debits
    	t.float :posted_credits
    	t.float :ending_balance

      t.timestamps
    end
  end
end
