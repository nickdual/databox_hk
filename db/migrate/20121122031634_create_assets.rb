class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.integer :asset_type_id
      t.integer :parent_asset_id
      t.integer :instance_of_product_id
      t.integer :class_id
      t.integer :status_id
      t.boolean :has_quantity
      t.decimal :quantity_on_hand_total
      t.decimal :available_to_promise_total
      t.text :asset_name
      t.text :comments
      t.integer :owner_party_id
      t.integer :acquire_order_id
      t.integer :acquire_order_item_seq_id
      t.text :serial_number
      t.text :soft_identifier
      t.text :activation_number
      t.datetime :activation_valid_thru
      t.datetime :receive_date
      t.datetime :date_acquired
      t.datetime :date_manufactured
      t.datetime :date_last_serviced
      t.datetime :date_next_service
      t.date :expected_end_of_life
      t.date :actual_end_of_life
      t.decimal :production_capacity
      t.integer :production_capacity_uom_id
      t.integer :calendar_id
      t.integer :facility_id
      t.integer :location_seq_id
      t.integer :container_id
      t.integer :lot_id
      t.decimal :purchase_cost
      t.integer :purchase_cost_uom_id
      t.decimal :salvage_value
      t.decimal :depreciation
      t.integer :depreciation_type_id

      t.timestamps
    end
    add_index :assets, :serial_number, :name => 'INVITEM_SN'
    add_index :assets, :soft_identifier, :name => 'INVITEM_SOFID', :unique => true
    add_index :assets, :activation_number, :name => 'INVITEM_ACTNM', :unique => true
  end
end
