class CreateTimesheets < ActiveRecord::Migration
  def change
    create_table :timesheets do |t|
      t.integer :party_id
      t.integer :client_party_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :status_id
      t.string :comments

      t.timestamps
    end
  end
end
