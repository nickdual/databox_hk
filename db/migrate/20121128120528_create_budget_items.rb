class CreateBudgetItems < ActiveRecord::Migration
  def change
    create_table :budget_items do |t|
    	t.integer :budget_id
    	t.integer :budget_item_seq_id
    	t.enum :budget_item_type_enum_id
    	t.float :amount
    	t.string :purpose
    	t.string :justification

      t.timestamps
    end
  end
end
