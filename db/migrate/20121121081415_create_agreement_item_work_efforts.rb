class CreateAgreementItemWorkEfforts < ActiveRecord::Migration
  def change
    create_table :agreement_item_work_efforts do |t|
      t.integer :agreement_id
      t.integer :agreement_item_seq_id
      t.integer :work_effort_id

      t.timestamps
    end
  end
end
