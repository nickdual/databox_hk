class CreateOrganization < ActiveRecord::Migration
  def create
	  create_table "organization", :force => true do |t|
	    t.integer   "party_id" 
	    t.text   "organization_name"
	    t.text   "office_site_name" 
	    t.decimal   "annual_revenue", :precision => 10, :scale => 2, :null => false  
	    t.decimal   "num_employees", :precision => 10, :scale => 2, :null => false 
	    t.text   "comments"    
	    t.datetime "created_at",                    :null => false
	    t.datetime "updated_at",                    :null => false
	  end
  end
end
