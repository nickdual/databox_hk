class CreateCommunicationEventParties < ActiveRecord::Migration
  def change
    create_table :communication_event_parties do |t|
      t.integer :communication_event_id
      t.integer :party_id
      t.integer :role_type_id
      t.integer :contact_mech_id
      t.integer :status_id

      t.timestamps
    end
  end
end
