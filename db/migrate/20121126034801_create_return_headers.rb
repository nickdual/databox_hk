class CreateReturnHeaders < ActiveRecord::Migration
  def change
    create_table :return_headers do |t|
      t.integer :return_id
      t.integer :status_id
      t.integer :from_party_id
      t.integer :to_party_id
      t.integer :payment_method_id
      t.integer :fin_account_id
      t.integer :billing_account_id
      t.datetime :entry_date
      t.integer :origin_contact_mech_id
      t.integer :destination_facility_id
      t.boolean :needs_inventory_receive
      t.integer :currency_uom_id
      t.integer :supplier_rma_id

      t.timestamps
    end
  end
end
