class CreateGiftCardFulfillments < ActiveRecord::Migration
  def change
    create_table :gift_card_fulfillments do |t|
      t.integer :fulfillment_id
      t.integer :type_enum_id
      t.integer :merchant_id
      t.integer :party_id
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :survey_response_id
      t.text :card_number
      t.text :pin_number
      t.decimal :amount, :precision => 10, :scale => 2,:null => false
      t.text :response_code
      t.text :reference_num
      t.text :auth_code
      t.datetime :fulfillment_date

      t.timestamps
    end
  end
end
