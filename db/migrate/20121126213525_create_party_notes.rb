class CreatePartyNotes < ActiveRecord::Migration
  def change
    create_table :party_notes do |t|
      t.integer :party_id
      t.datetime :note_date
      t.text :note_text

      t.timestamps
    end
  end
end
