class CreateAssetReservations < ActiveRecord::Migration
  def change
    create_table :asset_reservations do |t|
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :asset_id
      t.integer :reserve_order_id
      t.decimal :quantity
      t.decimal :quantity_not_available
      t.datetime :reserved_datetime
      t.datetime :promised_datetime
      t.datetime :current_promised_date
      t.integer :priority
      t.integer :sequence_num

      t.timestamps
    end
  end
end
