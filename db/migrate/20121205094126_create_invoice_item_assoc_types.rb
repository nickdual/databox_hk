class CreateInvoiceItemAssocTypes < ActiveRecord::Migration
  def change
    create_table :invoice_item_assoc_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
