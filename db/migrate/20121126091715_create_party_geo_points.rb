class CreatePartyGeoPoints < ActiveRecord::Migration
  def change
    create_table :party_geo_points do |t|
      t.integer :party_id
      t.integer :geo_point_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
