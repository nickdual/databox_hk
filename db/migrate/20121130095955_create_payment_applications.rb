class CreatePaymentApplications < ActiveRecord::Migration
  def change
    create_table :payment_applications do |t|
      t.integer :payment_application_id
      t.integer :payment_id
      t.integer :invoice_id
      t.integer :invoice_item_seq_id
      t.integer :billing_account_id
      t.integer :override_gl_account_id
      t.integer :to_payment_id
      t.integer :tax_auth_geo_id
      t.decimal :amount_applied, :precision => 10, :scale => 2,:null => false

      t.timestamps
    end
  end
end
