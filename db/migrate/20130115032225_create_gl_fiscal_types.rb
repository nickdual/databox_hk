class CreateGlFiscalTypes < ActiveRecord::Migration
  def change
    create_table :gl_fiscal_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
