class CreateTrackingCodeOrders < ActiveRecord::Migration
  def change
    create_table :tracking_code_orders do |t|
      t.integer :order_id
      t.integer :tracking_code_type_enum_id
      t.integer :tracking_code_id
      t.boolean :is_billable
      t.text :site_id
      t.boolean :has_exported
      t.datetime :affiliate_referred_time_stamp

      t.timestamps
    end
  end
end
