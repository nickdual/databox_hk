class CreateTaxAuthorities < ActiveRecord::Migration
  def change
    create_table :tax_authorities do |t|
    	t.integer :tax_auth_geo_id
    	t.integer :tax_auth_party_id
    	t.boolean :require_tax_id_for_exemption
    	t.string :tax_id_format_pattern
    	t.boolean :include_tax_in_price

      t.timestamps
    end
  end
end
