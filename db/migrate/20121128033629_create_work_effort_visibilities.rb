class CreateWorkEffortVisibilities < ActiveRecord::Migration
  def change
    create_table :work_effort_visibilities do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
