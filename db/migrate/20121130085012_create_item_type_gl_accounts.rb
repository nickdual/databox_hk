class CreateItemTypeGlAccounts < ActiveRecord::Migration
  def change
    create_table :item_type_gl_accounts do |t|
    	t.enum :item_type_enum_id
    	t.integer :organization_party_id
    	t.integer :gl_accouny_id

      t.timestamps
    end
  end
end
