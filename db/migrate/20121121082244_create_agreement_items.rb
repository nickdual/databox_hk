class CreateAgreementItems < ActiveRecord::Migration
  def change
    create_table :agreement_items do |t|
      t.integer :agreement_id
      t.integer :agreement_item_seq_id
      t.integer :agreement_item_type_id
      t.integer :currency_uom_id
      t.datetime :from_date
      t.datetime :thru_date
      t.text :item_text

      t.timestamps
    end
  end
end
