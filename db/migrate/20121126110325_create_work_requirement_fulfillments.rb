class CreateWorkRequirementFulfillments < ActiveRecord::Migration
  def change
    create_table :work_requirement_fulfillments do |t|
      t.integer :requirement_id
      t.integer :work_effort_id
      t.integer :fulfillment_type_enum_id

      t.timestamps
    end
  end
end
