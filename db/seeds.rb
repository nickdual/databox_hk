
# Add an admin user with credentials u: admin p: adminowm

User.create(email:'admin@owm.com',
            password:'adminowm',
            sign_in_count:"0",
            created_at:"2012-11-21 10:24:28.586343",
            confirmed_at:'2012-11-22 10:01:19.653129',
            approval:true)
#Make first user Admin
user = User.first
user.add_role('admin')
#Creating Roles

Role.create([
                {name:'account_lead'},
                {name:'admin'},
                {name:'agent'},
                {name:'automated_agent_role'},
                {name:'calendar_role'},
                {name:'client'},
                {name:'comm_participant'},
                {name:'consumer'},
                {name:'contractor'},
                {name:'customer'},
                {name:'distribution_channel'},
                {name:'ISP'},
                {name:'hosting_server'},
                {name:'manufacturer'},
                {name:'na'},
                {name:'organization_role'},
                {name:'owner'},
                {name:'prospect'},
                {name:'person_role'},
                {name:'referrer'},
                {name:'req_manager'},
                {name:'req_requester'},
                {name:'req_taker'},
                {name:'sfa_role'},
                {name:'shareholder'},
                {name:'subscriber'},
                {name:'vendor'},
                {name:'visitor'},
                {name:'web_master'},
                {name:'workflow_role'},
                {name:'accountant'}
            ])
#99.times do |n|
#  billback_factor = Faker::Name.name
#  override_org_party_id = rand(1..10)
#  promotion_id = SecureRandom.uuid
#  promotion_name = Faker::Name.name
#  promotion_show_to_customer = Faker::Name.name
#  promotion_text = Faker::Name.name
#  require_code = rand(1..10)
#  use_limit_per_customer = rand(1..10)
#  use_limit_per_order = rand(1..10)
#  use_limit_per_promotion = rand(1..10)
#  user_entered = Faker::Name.name
#  ProductPromotion.create!(
#      billback_factor: billback_factor,
#      override_org_party_id: override_org_party_id,
#      promotion_id: promotion_id,
#      promotion_name: promotion_name,
#      promotion_show_to_customer: promotion_show_to_customer,
#      promotion_text: promotion_text,
#      require_code: require_code,
#      use_limit_per_customer: use_limit_per_customer,
#      use_limit_per_order: use_limit_per_order,
#      use_limit_per_promotion: use_limit_per_promotion,
#      user_id: 1,
#      user_entered: user_entered,
#      created_by_user_login: 1,
#      last_modified_by_user_login: 1
#  )
#end

10.times do |n|
  ProductStore.create(
      store_name: Faker::Name.name,
      default_locale: 'en'
  )
end

#99.times do |n|
#  product_store_id = rand(0..1)
#  sequence_num = rand(1..10)
#  thru_date = Time.now
#  product_promotion_id = 1
#  PromotionStore.create!(
#      product_store_id: product_store_id,
#      sequence_num: sequence_num,
#      thru_date: thru_date,
#      product_promotion_id: product_promotion_id
#  )
#end

#PromotionRule.create(id: 1, name: '54444')

Operator.create!(name: 'Is')
Operator.create!(name: 'Is Not')
Operator.create!(name: 'Is Less Than')
Operator.create!(name: 'Is Less Than or Equal To')
Operator.create!(name: 'Is Greater Than')
Operator.create!(name: 'Is Greater Than or Equal To')

ShipmentMethod.create!(name: 'UPS Air')
ShipmentMethod.create!(name: 'FEDEX Express')
ShipmentMethod.create!(name: 'DHL Express')
ShipmentMethod.create!(name: 'USPS Express')
ShipmentMethod.create!(name: 'FEDEX First Overnight')
ShipmentMethod.create!(name: 'DHL Ground')
ShipmentMethod.create!(name: 'UPS Ground')
ShipmentMethod.create!(name: 'FEDEX Ground')
ShipmentMethod.create!(name: 'FEDEX Ground Home Delivery')
ShipmentMethod.create!(name: 'FEDEX International Economy')
ShipmentMethod.create!(name: 'FEDEX International Priority')
ShipmentMethod.create!(name: 'FEDEX Next Morning')
ShipmentMethod.create!(name: 'UPS Guaranteed Next Day')
ShipmentMethod.create!(name: 'FEDEX Guaranteed Next Day')
ShipmentMethod.create!(name: 'FEDEX Next Afternoon')
ShipmentMethod.create!(name: 'DHL Next Afternoon')
ShipmentMethod.create!(name: 'NA_ No Shipping')
ShipmentMethod.create!(name: 'FEDEX Priority Overnight')
ShipmentMethod.create!(name: 'FEDEX Same Day')
ShipmentMethod.create!(name: '_NA_ Sea')
ShipmentMethod.create!(name: 'UPS Second Day')
ShipmentMethod.create!(name: 'DHL Second Day')
ShipmentMethod.create!(name: 'FEDEX Second Day')
ShipmentMethod.create!(name: '_NA_ Standard')
ShipmentMethod.create!(name: 'USPS Standard')
ShipmentMethod.create!(name: 'FEDEX Standard Overnight')

ProductCategory.create(:id => 1,:category_name => "A1")
ProductCategory.create(:id => 2,:category_name => "A2")
ProductCategory.create(:id => 3,:category_name => "A3")
ProductCategory.create(:id => 4,:category_name => "A4")
ProductCategory.create(:id => 5,:category_name => "A5")
ProductCategory.create(:id => 6,:category_name => "A6")
PromotionRuleCondition.create(promotion_rule_id: 1, promotion_rule_condition_name: 'PPIP_ORDER_TOTAL', operator_id: 1, value: 22, other: 222, shipment_method_id: 1)
PromotionRuleCondition.create(promotion_rule_id: 1, promotion_rule_condition_name: 'PPIP_PRODUCT_TOTAL', operator_id: 2, value: 22, other: 222, shipment_method_id: 2)
PromotionRuleConditionCategory.create( product_category_id: "1", sub_category_status: "include", group_id: 1, promotion_rule_condition_id: 1  )
10.times do |n|
  PromotionRuleAction.create(
      amount: 10,
      item_id: 1,
      party_id: 1,
      promotion_rule_action_name: "PPRA_GIFT_WITH_PURCHASE",
      promotion_rule_id: 1,
      quantity: 10,
      service_name: Faker::Name.name,
      use_cart_quantity: 10
  )
end

Rake::Task['geonames_rails:load_data'].invoke


#PromotionRule.create(id: 1, name: '54444')
#
#Operator.create!(name: 'Is')
#Operator.create!(name: 'Is Not')
#Operator.create!(name: 'Is Less Than')
#Operator.create!(name: 'Is Less Than or Equal To')
#Operator.create!(name: 'Is Greater Than')
#Operator.create!(name: 'Is Greater Than or Equal To')
#
#ShipmentMethod.create!(name: 'UPS Air')
#ShipmentMethod.create!(name: 'FEDEX Express')
#ShipmentMethod.create!(name: 'DHL Express')
#ShipmentMethod.create!(name: 'USPS Express')
#ShipmentMethod.create!(name: 'FEDEX First Overnight')
#ShipmentMethod.create!(name: 'DHL Ground')
#ShipmentMethod.create!(name: 'UPS Ground')
#ShipmentMethod.create!(name: 'FEDEX Ground')
#ShipmentMethod.create!(name: 'FEDEX Ground Home Delivery')
#ShipmentMethod.create!(name: 'FEDEX International Economy')
#ShipmentMethod.create!(name: 'FEDEX International Priority')
#ShipmentMethod.create!(name: 'FEDEX Next Morning')
#ShipmentMethod.create!(name: 'UPS Guaranteed Next Day')
#ShipmentMethod.create!(name: 'FEDEX Guaranteed Next Day')
#ShipmentMethod.create!(name: 'FEDEX Next Afternoon')
#ShipmentMethod.create!(name: 'DHL Next Afternoon')
#ShipmentMethod.create!(name: 'NA_ No Shipping')
#ShipmentMethod.create!(name: 'FEDEX Priority Overnight')
#ShipmentMethod.create!(name: 'FEDEX Same Day')
#ShipmentMethod.create!(name: '_NA_ Sea')
#ShipmentMethod.create!(name: 'UPS Second Day')
#ShipmentMethod.create!(name: 'DHL Second Day')
#ShipmentMethod.create!(name: 'FEDEX Second Day')
#ShipmentMethod.create!(name: '_NA_ Standard')
#ShipmentMethod.create!(name: 'USPS Standard')
#ShipmentMethod.create!(name: 'FEDEX Standard Overnight')
#
#ProductCategory.create(:id => 1,:category_name => "A1")
#ProductCategory.create(:id => 2,:category_name => "A2")
#ProductCategory.create(:id => 3,:category_name => "A3")
#ProductCategory.create(:id => 4,:category_name => "A4")
#ProductCategory.create(:id => 5,:category_name => "A5")
#ProductCategory.create(:id => 6,:category_name => "A6")
#PromotionRuleCondition.create(promotion_rule_id: 1, promotion_rule_condition_name: 'PPIP_ORDER_TOTAL', operator_id: 1, value: 22, other: 222, shipment_method_id: 1)
#PromotionRuleCondition.create(promotion_rule_id: 1, promotion_rule_condition_name: 'PPIP_PRODUCT_TOTAL', operator_id: 2, value: 22, other: 222, shipment_method_id: 2)
#PromotionRuleConditionCategory.create( product_category_id: "1", sub_category_status: "include", group_id: 1, promotion_rule_condition_id: 1  )
#10.times do |n|
#  PromotionRuleAction.create(
#      amount: 10,
#      item_id: 1,
#      party_id: 1,
#      promotion_rule_action_name: "PPRA_GIFT_WITH_PURCHASE",
#      promotion_rule_id: 1,
#      quantity: 10,
#      service_name: Faker::Name.name,
#      use_cart_quantity: 10
#  )
#end
#Rake::Task['geonames_rails:load'].invoke

