PartyQualificationStatus.seed do |s|
  s.id = 1
  s.status_id = 'HR_DS_COMPLETE'
  s.description = 'Incomplete'
  s.sequence_num = 11
end
PartyQualificationStatus.seed do |s|
  s.id = 2
  s.status_id = 'HR_DS_INCOMPLETE'
  s.description = 'Available'
  s.sequence_num = 12
end
PartyQualificationStatus.seed do |s|
  s.id = 3
  s.status_id = 'HR_DS_DEFERRED'
  s.description = 'Deferred'
  s.sequence_num = 13
end
PartyQualificationStatus.seed do |s|
  s.id = 4
  s.status_id = 'HR_JS_FULLTIME'
  s.description = 'Full time'
  s.sequence_num = 21
end
PartyQualificationStatus.seed do |s|
  s.id = 5
  s.status_id = 'HR_JS_PARTTIME'
  s.description = 'Part time'
  s.sequence_num = 22
end
PartyQualificationStatus.seed do |s|
  s.id = 6
  s.status_id = 'HR_JS_CONTRACTOR'
  s.description = 'Contractor'
  s.sequence_num = 23
end
