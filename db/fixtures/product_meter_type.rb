ProductMeterType.seed do |s|
  s.id = 1
  s.description = 'Speedometer'
  s.name = 'SPEEDOMETER'
end
ProductMeterType.seed do |s|
  s.id = 2
  s.description = 'Tachometer'
  s.name = 'TACHOMETER'
end
ProductMeterType.seed do |s|
  s.id = 3
  s.description = 'Distance Meter'
  s.name = 'DISTANCE'
end
ProductMeterType.seed do |s|
  s.id = 4
  s.description = 'Motor Time Meter'
  s.name = 'MOTOR_TIME'
end
ProductMeterType.seed do |s|
  s.id = 5
  s.description = 'Use Count Meter'
  s.name = 'USE_COUNT'
end
ProductMeterType.seed do |s|
  s.id = 6
  s.description = 'Copy Count Meter'
  s.name = 'COPY_COUNT'
end
ProductMeterType.seed do |s|
  s.id = 7
  s.description = 'Trip meter'
  s.name = 'TRIP_METER'
end
ProductMeterType.seed do |s|
  s.id = 8
  s.description = 'Tachograph'
  s.name = 'TACHOGRAPH'
end
ProductMeterType.seed do |s|
  s.id = 9
  s.description = 'Taximeter'
  s.name = 'TAXIMETER'
end
ProductMeterType.seed do |s|
  s.id = 10
  s.description = 'Event Data Recorder'
  s.name = 'EVENT_DATA_RECORDER'
end
ProductMeterType.seed do |s|
  s.id = 11
  s.description = 'Pedometer'
  s.name = 'PEDOMETER'
end
ProductMeterType.seed do |s|
  s.id = 12
  s.description = 'Odometer'
  s.name = 'ODOMETER'
end