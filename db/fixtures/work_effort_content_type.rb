WorkEffortContentType.seed do |s|
  s.id = 1
  s.name = 'PROJECT_UPDATE'
  s.description = 'Project Update'
end
WorkEffortContentType.seed do |s|
  s.id = 2
  s.name = 'DISCUSSION'
  s.description = 'Discussion'
end
WorkEffortContentType.seed do |s|
  s.id = 3
  s.name = 'REQUIREMENT'
  s.description = 'Requirement'
end
WorkEffortContentType.seed do |s|
  s.id = 4
  s.name = 'DESIGN'
  s.description = 'Design'
end
WorkEffortContentType.seed do |s|
  s.id = 5
  s.name = 'PROPOSAL_MEDIA'
  s.description = 'Proposal Media'
end
WorkEffortContentType.seed do |s|
  s.id = 6
  s.name = 'SUPPORTING_MEDIA'
  s.description = 'Supporting Media'
end
WorkEffortContentType.seed do |s|
  s.id = 7
  s.name = 'CREATED_MEDIA'
  s.description = 'Created Media'
end