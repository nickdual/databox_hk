CommunicationEventStatus.seed do |s|
  s.id = 1
  s.description = 'Pending'
  s.name = 'COM_PENDING'
  s.sequence_num = 1
end
CommunicationEventStatus.seed do |s|
  s.id = 2
  s.description = 'Entered'
  s.name = 'COM_ENTERED'
  s.sequence_num = 2
end
CommunicationEventStatus.seed do |s|
  s.id = 3
  s.description = 'Read'
  s.name = 'COM_READ'
  s.sequence_num = 3
end
CommunicationEventStatus.seed do |s|
  s.id = 4
  s.description = 'In-Progress'
  s.name = 'COM_IN_PROGRESS'
  s.sequence_num = 5
end
CommunicationEventStatus.seed do |s|
  s.id = 5
  s.description = 'Unknown Party'
  s.name = 'COM_UNKNOWN_PARTY'
  s.sequence_num = 7
end
CommunicationEventStatus.seed do |s|
  s.id = 6
  s.description = 'Closed'
  s.name = 'COM_COMPLETE'
  s.sequence_num = 20
end
CommunicationEventStatus.seed do |s|
  s.id = 7
  s.description = 'Resolved'
  s.name = 'COM_RESOLVED'
  s.sequence_num = 21
end
CommunicationEventStatus.seed do |s|
  s.id = 8
  s.description = 'Referred'
  s.name = 'COM_REFERRED'
  s.sequence_num = 22
end
CommunicationEventStatus.seed do |s|
  s.id = 9
  s.description = 'Bounced'
  s.name = 'COM_BOUNCED'
  s.sequence_num = 50
end
CommunicationEventStatus.seed do |s|
  s.id = 10
  s.description = 'Cancelled'
  s.name = 'COM_CANCELLED'
  s.sequence_num = 99
end