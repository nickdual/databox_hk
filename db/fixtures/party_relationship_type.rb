PartyRelationshipType.seed do |s|
  s.id = 1
  s.name = 'AGENT'
  s.description = 'Agent'
end
PartyRelationshipType.seed do |s|
  s.id = 2
  s.name = 'CHILD'
  s.description = 'Child'
end
PartyRelationshipType.seed do |s|
  s.id = 3
  s.name = 'CONTACT_REL'
  s.description = 'Contact Relationship'
end
PartyRelationshipType.seed do |s|
  s.id = 4
  s.name = 'LEAD_REL'
  s.description = 'Lead Relationship'
end
PartyRelationshipType.seed do |s|
  s.id = 5
  s.name = 'LEAD_OWNER'
  s.description = 'Lead Owned by'
end
PartyRelationshipType.seed do |s|
  s.id = 6
  s.name = 'CUSTOMER_REL'
  s.description = 'Customer'
end
PartyRelationshipType.seed do |s|
  s.id = 7
  s.name = 'DISTRIBUTION_CHANNEL'
  s.description = 'Distributor'
end
PartyRelationshipType.seed do |s|
  s.id = 8
  s.name = 'EMPLOYMENT'
  s.description = 'Employee'
end
PartyRelationshipType.seed do |s|
  s.id = 9
  s.name = 'FRIEND'
  s.description = 'Friend'
end
PartyRelationshipType.seed do |s|
  s.id = 10
  s.name = 'GROUP_ROLLUP'
  s.description = 'Group Member'
end
PartyRelationshipType.seed do |s|
  s.id = 11
  s.name = 'HOST_SERVER_VISITOR'
  s.description = 'Host Server Visitor'
end
PartyRelationshipType.seed do |s|
  s.id = 12
  s.name = 'VISITOR_ISP'
  s.description = 'ISP Visitor'
end
PartyRelationshipType.seed do |s|
  s.id = 13
  s.name = 'MANAGER'
  s.description = 'Manager'
end
PartyRelationshipType.seed do |s|
  s.id = 14
  s.name = 'PARENT'
  s.description = 'Parent'
end
PartyRelationshipType.seed do |s|
  s.id = 15
  s.name = 'PARTNERSHIP'
  s.description = 'Partner'
end
PartyRelationshipType.seed do |s|
  s.id = 16
  s.name = 'SALES_AFFILIATE'
  s.description = 'Sales Affiliate'
end
PartyRelationshipType.seed do |s|
  s.id = 17
  s.name = 'SPOUSE'
  s.description = 'Spouse'
end
PartyRelationshipType.seed do |s|
  s.id = 18
  s.name = 'SUPPLIER_REL'
  s.description = 'Supplier'
end
PartyRelationshipType.seed do |s|
  s.id = 19
  s.name = 'WEB_MASTER_ASSIGNMEN'
  s.description = 'Web Master'
end
PartyRelationshipType.seed do |s|
  s.id = 20
  s.name = 'ACCOUNT'
  s.description = 'Account owned by'
end
PartyRelationshipType.seed do |s|
  s.id = 21
  s.name = 'ASSISTANT'
  s.description = 'Assistant'
end
PartyRelationshipType.seed do |s|
  s.id = 22
  s.name = 'OWNER'
  s.description = 'Owned by'
end
PartyRelationshipType.seed do |s|
  s.id = 23
  s.name = 'PARENT_ACCOUNT'
  s.description = 'Parent Account'
end
PartyRelationshipType.seed do |s|
  s.id = 24
  s.name = 'REPORTS_TO'
  s.description = 'Reports To'
end
PartyRelationshipType.seed do |s|
  s.id = 25
  s.name = 'LEAD_OWN_GRP_MEMBER'
  s.description = 'Lead Owners/Managers'
end