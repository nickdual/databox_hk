AssetIdentificationType.seed do |s|
  s.id = 1
  s.name = 'AIT_MFG_SERIAL'
  s.description = 'Mfg Serial Number'
end
AssetIdentificationType.seed do |s|
  s.id = 2
  s.name = 'AIT_TRACKING_VENDOR'
  s.description = 'Vendor Tracking/Inventory Number'
end
AssetIdentificationType.seed do |s|
  s.id = 3
  s.name = 'AIT_TRACKING_LABEL'
  s.description = 'Tracking Label Number'
end
AssetIdentificationType.seed do |s|
  s.id = 4
  s.name = 'AIT_VIN'
  s.description = 'Vehicle Identification Number (VIN)'
end
AssetIdentificationType.seed do |s|
  s.id = 5
  s.name = 'AIT_GAS_CARD'
  s.description = 'Gas Card Number'
end