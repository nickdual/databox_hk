AssetReservationOrder.seed do |s|
  s.id = 1
  s.name = 'INVRO_FIFO_REC'
  s.description = 'FIFO Receive'
end
AssetReservationOrder.seed do |s|
  s.id = 2
  s.name = 'INVRO_LIFO_REC'
  s.description = 'LIFO Receive'
end
AssetReservationOrder.seed do |s|
  s.id = 3
  s.name = 'INVRO_FIFO_EXP'
  s.description = 'FIFO Expire'
end
AssetReservationOrder.seed do |s|
  s.id = 4
  s.name = 'INVRO_LIFO_EXP'
  s.description = 'LIFO Expire'
end
AssetReservationOrder.seed do |s|
  s.id = 5
  s.name = 'INVRO_GUNIT_COST'
  s.description = 'Greatest Unit Cost'
end
AssetReservationOrder.seed do |s|
  s.id = 6
  s.name = 'INVRO_LUNIT_COST'
  s.description = 'Lowest Unit Cost'
end
AssetReservationOrder.seed do |s|
  s.id = 7
  s.name = 'INVRO_NO_RES'
  s.description = 'No Reservation'
end