EmplPositionClass.seed do |s|
  s.id = 1
  s.title = 'Programmer'
  s.description = nil
  s.name = 'PROGRAMMER'
end
EmplPositionClass.seed do |s|
  s.id = 2
  s.title = 'System Administrator'
  s.description = nil
  s.name = 'SYS_ADMIN'
end
EmplPositionClass.seed do |s|
  s.id = 3
  s.title = 'Business Analyst'
  s.description = nil
  s.name = 'BIZ_ANALYST'
end