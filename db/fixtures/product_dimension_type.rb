ProductDimensionType.seed do |s|
  s.id = 1
  s.name = 'PRODDIM_QUANTITY'
  s.description = 'Quantity Included'
end
ProductDimensionType.seed do |s|
  s.id = 2
  s.name = 'PRODDIM_PIECES'
  s.description = 'Pieces Included'
end
ProductDimensionType.seed do |s|
  s.id = 3
  s.name = 'PRODDIM_WEIGHT'
  s.description = 'Weight'
end
ProductDimensionType.seed do |s|
  s.id = 4
  s.name = 'PRODDIM_HEIGHT'
  s.description = 'Height'
end
ProductDimensionType.seed do |s|
  s.id = 5
  s.name = 'PRODDIM_WIDTH'
  s.description = 'Width'
end
ProductDimensionType.seed do |s|
  s.id = 6
  s.name = 'PRODDIM_DEPTH'
  s.description = 'Depth'
end
ProductDimensionType.seed do |s|
  s.id = 7
  s.name = 'PRODDIM_SH_WEIGHT'
  s.description = 'Shipping Weight'
end
ProductDimensionType.seed do |s|
  s.id = 8
  s.name = 'PRODDIM_SH_HEIGHT'
  s.description = 'Shipping Height'
end
ProductDimensionType.seed do |s|
  s.id = 9
  s.name = 'PRODDIM_SH_WIDTH'
  s.description = 'Shipping Width'
end
ProductDimensionType.seed do |s|
  s.id = 10
  s.name = 'PRODDIM_SH_DEPTH'
  s.description = 'Shipping Depth'
end
ProductDimensionType.seed do |s|
  s.id = 11
  s.name = 'PRODDIM_DIAMETER'
  s.description = 'Diameter'
end