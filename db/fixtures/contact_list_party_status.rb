ContactListPartyStatus.seed do |s|
   s.status_id = 'CLPT_PENDING'
   s.description = 'Pending Acceptance'
   s.sequence_num =  1
end
ContactListPartyStatus.seed do |s|
   s.status_id = 'CLPT_ACCEPTED'
   s.description = 'Accepted'
   s.sequence_num = 2
end
ContactListPartyStatus.seed do |s|
   s.status_id = 'CLPT_REJECTED'
   s.description = 'Rejected'
   s.sequence_num = 3
end
ContactListPartyStatus.seed do |s|
   s.status_id = 'CLPT_IN_USE'
   s.description = 'In Use'
   s.sequence_num = 4
end
ContactListPartyStatus.seed do |s|
   s.status_id = 'CLPT_INVALID'
   s.description = 'Invalid'
   s.sequence_num = 5
end
ContactListPartyStatus.seed do |s|
   s.status_id = 'CLPT_UNSUBS_PENDING'
   s.description = 'Unsubscription pending'
   s.sequence_num = 6
end
ContactListPartyStatus.seed do |s|
   s.status_id = 'CLPT_UNSUBSCRIBED'
   s.description = 'Unsubscribed'
   s.sequence_num = 7
end