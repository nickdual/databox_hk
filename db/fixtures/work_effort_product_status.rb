WorkEffortProductStatus.seed do |s|
  s.id = 1
  s.description = 'Created'
  s.name = 'WEPAS_CREATED'
  s.sequence_num = 1
end
WorkEffortProductStatus.seed do |s|
  s.id = 2
  s.description = 'Completed'
  s.name = 'WEPAS_COMPLETED'
  s.sequence_num = 2
end
WorkEffortProductStatus.seed do |s|
  s.id = 3
  s.description = 'Cancelled'
  s.name = 'WEPAS_CANCELLED'
  s.sequence_num = 3
end