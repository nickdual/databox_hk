ContactMechanismType.seed do |s|
  s.id = 1
  s.description = 'Postal Address'
  s.name = 'POSTAL_ADDRESS'
  s.parent_id = nil
end
ContactMechanismType.seed do |s|
  s.id = 2
  s.description = 'Phone Number'
  s.name = 'TELECOM_NUMBER'
  s.parent_id = nil
end
ContactMechanismType.seed do |s|
  s.id = 3
  s.description = 'Electronic Address'
  s.name = 'ELECTRONIC_ADDRESS'
  s.parent_id = nil
end
ContactMechanismType.seed do |s|
  s.id = 4
  s.description = 'Email Address'
  s.name = 'EMAIL_ADDRESS'
  s.parent_id = 3
end
ContactMechanismType.seed do |s|
  s.id = 5
  s.description = 'Internet IP Address'
  s.name = 'IP_ADDRESS'
  s.parent_id = 3
end
ContactMechanismType.seed do |s|
  s.id = 6
  s.description = 'Internet Domain Name'
  s.name = 'DOMAIN_NAME'
  s.parent_id = 3
end
ContactMechanismType.seed do |s|
  s.id = 7
  s.description = 'Web URL/Address'
  s.name = 'WEB_ADDRESS'
  s.parent_id = 3
end