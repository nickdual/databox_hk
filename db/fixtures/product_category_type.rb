ProductCategoryType.seed do |s|
  s.id = 1
  s.name = 'CATALOG_CATEGORY'
  s.description = 'Catalog'
end
ProductCategoryType.seed do |s|
  s.id = 2
  s.name = 'INDUSTRY_CATEGORY'
  s.description = 'Industry'
end
ProductCategoryType.seed do |s|
  s.id = 3
  s.name = 'INTERNAL_CATEGORY'
  s.description = 'Internal'
end
ProductCategoryType.seed do |s|
  s.id = 4
  s.name = 'MATERIALS_CATEGORY'
  s.description = 'Materials'
end
ProductCategoryType.seed do |s|
  s.id = 5
  s.name = 'QUICKADD_CATEGORY'
  s.description = 'Quick Add'
end
ProductCategoryType.seed do |s|
  s.id = 6
  s.name = 'SEARCH_CATEGORY'
  s.description = 'Search'
end
ProductCategoryType.seed do |s|
  s.id = 7
  s.name = 'USAGE_CATEGORY'
  s.description = 'Usage'
end
ProductCategoryType.seed do |s|
  s.id = 8
  s.name = 'MIXMATCH_CATEGORY'
  s.description = 'Mix and Match'
end
ProductCategoryType.seed do |s|
  s.id = 9
  s.name = 'CROSS_SELL_CATEGORY'
  s.description = 'Cross Sell'
end
ProductCategoryType.seed do |s|
  s.id = 10
  s.name = 'TAX_CATEGORY'
  s.description = 'Tax'
end
ProductCategoryType.seed do |s|
  s.id = 11
  s.name = 'GOOGLE_BASE_CATEGORY'
  s.description = 'Google Base'
end
ProductCategoryType.seed do |s|
  s.id = 12
  s.name = 'GIFT_CARD_CATEGORY'
  s.description = 'Gift Cards'
end
ProductCategoryType.seed do |s|
  s.id = 13
  s.name = 'BEST_SELL_CATEGORY'
  s.description = 'Best Selling'
end