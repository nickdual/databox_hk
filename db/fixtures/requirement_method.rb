RequirementMethod.seed do |s|
  s.id = 1
  s.name = 'PRODRQM_NONE'
  s.description = 'No Requirement Created'
end
RequirementMethod.seed do |s|
  s.id = 2
  s.name = 'PRODRQM_AUTO'
  s.description = 'Automatic For Every Sales Order'
end
RequirementMethod.seed do |s|
  s.id = 3
  s.name = 'PRODRQM_STOCK_QOH'
  s.description = 'When QOH Reaches Minimum Stock for Product-Facility'
end
RequirementMethod.seed do |s|
  s.id = 4
  s.name = 'PRODRQM_STOCK_ATP'
  s.description = 'When ATP Reaches Minimum Stock for Product-Facility'
end
RequirementMethod.seed do |s|
  s.id = 5
  s.name = 'PRODRQM_DS'
  s.description = 'Drop-ship only'
end
RequirementMethod.seed do |s|
  s.id = 6
  s.name = 'PRODRQM_DSATP'
  s.description = 'Auto drop-ship on low quantity'
end