SalesChannel.seed do |s|
  s.id = 1
  s.name = 'WEB_SALES_CHANNEL'
  s.description = 'Web Channel'
end
SalesChannel.seed do |s|
  s.id = 2
  s.name = 'POS_SALES_CHANNEL'
  s.description = 'POS Channel'
end
SalesChannel.seed do |s|
  s.id = 3
  s.name = 'FAX_SALES_CHANNEL'
  s.description = 'Fax Channel'
end
SalesChannel.seed do |s|
  s.id = 4
  s.name = 'PHONE_SALES_CHANNEL'
  s.description = 'Phone Channel'
end
SalesChannel.seed do |s|
  s.id = 5
  s.name = 'EMAIL_SALES_CHANNEL'
  s.description = 'E-Mail Channel'
end
SalesChannel.seed do |s|
  s.id = 6
  s.name = 'SNAIL_SALES_CHANNEL'
  s.description = 'Snail Mail Channel'
end
SalesChannel.seed do |s|
  s.id = 7
  s.name = 'AFFIL_SALES_CHANNEL'
  s.description = 'Affiliate Channel'
end
SalesChannel.seed do |s|
  s.id = 8
  s.name = 'EBAY_SALES_CHANNEL'
  s.description = 'eBay Channel'
end
SalesChannel.seed do |s|
  s.id = 9
  s.name = 'UNKNWN_SALES_CHANNEL'
  s.description = 'Unknown Channel'
end



