ProductStoreEmailType.seed do |s|
  s.id = 1
  s.name = 'PSET_CUST_REGISTER'
  s.description = 'Registration'
end
ProductStoreEmailType.seed do |s|
  s.id = 2
  s.name = 'PSET_ODR_CONFIRM'
  s.description = 'Confirmation'
end
ProductStoreEmailType.seed do |s|
  s.id = 3
  s.name = 'PSET_ODR_COMPLETE'
  s.description = 'Complete'
end
ProductStoreEmailType.seed do |s|
  s.id = 4
  s.name = 'PSET_ODR_BACKORDER'
  s.description = 'Back-Order'
end
ProductStoreEmailType.seed do |s|
  s.id = 5
  s.name = 'PSET_ODR_CHANGE'
  s.description = 'Order Change'
end
ProductStoreEmailType.seed do |s|
  s.id = 6
  s.name = 'PSET_ODR_PAYRETRY'
  s.description = 'Payment Retry'
end
ProductStoreEmailType.seed do |s|
  s.id = 7
  s.name = 'PSET_RTN_ACCEPT'
  s.description = 'Return Accepted'
end
ProductStoreEmailType.seed do |s|
  s.id = 8
  s.name = 'PSET_RTN_COMPLETE'
  s.description = 'Return Completed'
end
ProductStoreEmailType.seed do |s|
  s.id = 9
  s.name = 'PSET_RTN_CANCEL'
  s.description = 'Return Cancelled'
end
ProductStoreEmailType.seed do |s|
  s.id = 10
  s.name = 'PSET_PWD_RETRIEVE'
  s.description = 'Retrieve Password'
end
ProductStoreEmailType.seed do |s|
  s.id = 11
  s.name = 'PSET_TELL_FRIEND'
  s.description = 'Tell-A-Friend'
end
ProductStoreEmailType.seed do |s|
  s.id = 12
  s.name = 'PSET_GC_PURCHASE'
  s.description = 'Gift-Card Purchase'
end
ProductStoreEmailType.seed do |s|
  s.id = 13
  s.name = 'PSET_GC_RELOAD'
  s.description = 'Gift-Card Reload'
end
ProductStoreEmailType.seed do |s|
  s.id = 14
  s.name = 'PSET_QUO_CREATED'
  s.description = 'Quote Created'
end
ProductStoreEmailType.seed do |s|
  s.id = 15
  s.name = 'PSET_QUO_CONFIRM'
  s.description = 'Quote Confirmation'
end
ProductStoreEmailType.seed do |s|
  s.id = 16
  s.name = 'PSET_ODR_SHIP_COMPLT'
  s.description = 'Shipment Complete'
end


