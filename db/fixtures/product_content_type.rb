ProductContentType.seed do |s|
  s.id = 1
  s.name = 'ONLINE_ACCESS'
  s.description = 'Online Access'
end
ProductContentType.seed do |s|
  s.id = 2
  s.name = 'DIGITAL_DOWNLOAD'
  s.description = 'Digital Download'
end
ProductContentType.seed do |s|
  s.id = 3
  s.name = 'FULFILLMENT_EMAIL'
  s.description = 'Fulfillment Email'
end
ProductContentType.seed do |s|
  s.id = 4
  s.name = 'PRODUCT_NAME'
  s.description = 'Product Name'
end
ProductContentType.seed do |s|
  s.id = 5
  s.name = 'DESCRIPTION'
  s.description = 'Description'
end
ProductContentType.seed do |s|
  s.id = 6
  s.name = 'LONG_DESCRIPTION'
  s.description = 'Description'
end
ProductContentType.seed do |s|
  s.id = 7
  s.name = 'PRICE_DETAIL_TEXT'
  s.description = 'Price Detail Text'
end
ProductContentType.seed do |s|
  s.id = 8
  s.name = 'INGREDIENTS'
  s.description = 'Ingredients'
end
ProductContentType.seed do |s|
  s.id = 9
  s.name = 'UNIQUE_INGREDIENTS'
  s.description = 'Unique Ingredients'
end
ProductContentType.seed do |s|
  s.id = 10
  s.name = 'SPECIALINSTRUCTIONS'
  s.description = 'Special Instructions'
end
ProductContentType.seed do |s|
  s.id = 11
  s.name = 'WARNINGS'
  s.description = 'Warnings'
end
ProductContentType.seed do |s|
  s.id = 12
  s.name = 'DIRECTIONS'
  s.description = 'Directions'
end
ProductContentType.seed do |s|
  s.id = 13
  s.name = 'TERMS_AND_CONDS'
  s.description = 'Terms and Conditions'
end
ProductContentType.seed do |s|
  s.id = 14
  s.name = 'DELIVERY_INFO'
  s.description = 'Delivery Info'
end
ProductContentType.seed do |s|
  s.id = 15
  s.name = 'SMALL_IMAGE_URL'
  s.description = 'Image - Small'
end
ProductContentType.seed do |s|
  s.id = 16
  s.name = 'MEDIUM_IMAGE_URL'
  s.description = 'Image - Medium'
end
ProductContentType.seed do |s|
  s.id = 17
  s.name = 'LARGE_IMAGE_URL'
  s.description = 'Image - Large'
end
ProductContentType.seed do |s|
  s.id = 18
  s.name = 'DETAIL_IMAGE_URL'
  s.description = 'Image - Detail'
end
ProductContentType.seed do |s|
  s.id = 19
  s.name = 'ORIGINAL_IMAGE_URL'
  s.description = 'Image - Original'
end
ProductContentType.seed do |s|
  s.id = 20
  s.name = 'SMALL_IMAGE_ALT'
  s.description = 'Image Alt Text - Small'
end
ProductContentType.seed do |s|
  s.id = 21
  s.name = 'MEDIUM_IMAGE_ALT'
  s.description = 'Image Alt Text - Medium'
end
ProductContentType.seed do |s|
  s.id = 22
  s.name = 'LARGE_IMAGE_ALT'
  s.description = 'Image Alt Text - Large'
end
ProductContentType.seed do |s|
  s.id = 23
  s.name = 'DETAIL_IMAGE_ALT'
  s.description = 'Image Alt Text - Detail'
end
ProductContentType.seed do |s|
  s.id = 24
  s.name = 'ADDTOCART_LABEL'
  s.description = 'Add To Cart Label'
end
ProductContentType.seed do |s|
  s.id = 25
  s.name = 'ADDTOCART_IMAGE'
  s.description = 'Add To Cart Image'
end
ProductContentType.seed do |s|
  s.id = 26
  s.name = 'SHORT_SALES_PITCH'
  s.description = 'Short Sales Pitch'
end
ProductContentType.seed do |s|
  s.id = 27
  s.name = 'INSTALLATION'
  s.description = 'Installation'
end
ProductContentType.seed do |s|
  s.id = 28
  s.name = 'SPECIFICATION'
  s.description = 'Specification'
end
ProductContentType.seed do |s|
  s.id = 29
  s.name = 'WARRANTY'
  s.description = 'Warranty'
end
ProductContentType.seed do |s|
  s.id = 30
  s.name = 'META_KEYWORDS'
  s.description = 'Meta-Keywords'
end
ProductContentType.seed do |s|
  s.id = 31
  s.name = 'META_DESCRIPTION'
  s.description = 'Meta-Description'
end