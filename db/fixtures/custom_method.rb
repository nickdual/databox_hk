CustomMethod.seed do |s|
  s.id = 1
  s.custom_method_id = 'STR_LINE_DEP_FORMULA'
  s.custom_method_type_id = 1
  s.custom_method_name = 'straightLineDepreciation'
  s.description = 'Straight Line depreciatiion algorithm for fixed asset((purchaseCost - salvageCost)/expectedLifeInYears)'
end
CustomMethod.seed do |s|
  s.id = 2
  s.custom_method_id = 'DBL_DECL_DEP_FORMULA'
  s.custom_method_type_id = 1
  s.custom_method_name = 'doubleDecliningBalanceDepreciation'
  s.description = 'Double decline depreciatiion algorithm for fixed asset((NetBookValue - salvageCost)*2/remainingLifeInYears)'
end
