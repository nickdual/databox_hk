ContactListEmailType.seed do |s|
   s.name = 'CONT_UNSUB_NOT'
   s.description = 'Unsubscribe Contact List Notification'
end
ContactListEmailType.seed do |s|
   s.name = 'CONT_SUB_NOT'
   s.description = 'Subscribe Contact List Notification'
end
ContactListEmailType.seed do |s|
   s.name = 'CONT_UNSUB_VER'
   s.description = 'Unsubscribe Contact List Verify'
end
ContactListEmailType.seed do |s|
   s.name = 'CONT_EMAIL_TEMPLATE'
   s.description = 'Contact List E-mail Template'
end