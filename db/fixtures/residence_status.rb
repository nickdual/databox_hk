ResidenceStatus.seed do |s|
  s.id = 1
  s.name = 'PRESS_OWN'
  s.description = 'Own Home'
end
ResidenceStatus.seed do |s|
  s.id = 2
  s.name = 'PRESS_PVT_TENANT'
  s.description = 'Private Tenant'
end
ResidenceStatus.seed do |s|
  s.id = 3
  s.name = 'PRESS_PUB_TENANT'
  s.description = 'Public Tenant'
end
ResidenceStatus.seed do |s|
  s.id = 4
  s.name = 'PRESS_PARENTS'
  s.description = 'With Parents'
end
