TimePeriod.seed do |s|
  s.id = 1
  s.parent_period_id = nil
  s.time_period_type_id = 'FISCAL_YEAR'
  s.party_id = 1
  s.period_num = 1
  s.period_name = 'Time Period Name 1'
  s.from_date = Time.now
  s.thru_date = 2.weeks.from_now
  s.is_closed = false
end

TimePeriod.seed do |s|
  s.id = 2
  s.parent_period_id = 1
  s.time_period_type_id = 'FISCAL_MONTH'
  s.party_id = 2
  s.period_num = 2
  s.period_name = 'Time Period Name 2'
  s.from_date = 1.day.ago
  s.thru_date = 1.month.from_now
  s.is_closed = false
end
