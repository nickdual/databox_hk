AssetType.seed do |s|
  s.id = 1
  s.name = 'INTERNAL'
  s.description = 'Internal Content'
end
AssetType.seed do |s|
  s.id = 2
  s.name = 'USERDEF'
  s.description = 'User Defined Content'
end
AssetType.seed do |s|
  s.id = 3
  s.name = 'LGOIMGURL'
  s.description = 'Logo Image URL'
end
AssetType.seed do |s|
  s.id = 4
  s.name = 'VNDSHPINF'
  s.description = 'Vendor Shipping Info'
end
