JobPostingType.seed do |s|
  s.id = 1
  s.name = 'JobPostingType_INTR'
  s.description = 'Internal Job Posting'
end
JobPostingType.seed do |s|
  s.id = 2
  s.name = 'JobPostingType_EXTR'
  s.description = 'External Job Posting'
end
