PerformanceReviewItemType.seed do |s|
  s.id = 1
  s.name = 'PRIT_TECH'
  s.description = 'Technical skills'
end
PerformanceReviewItemType.seed do |s|
  s.id = 2
  s.name = 'PRIT_RESP'
  s.description = 'Responsibility'
end
PerformanceReviewItemType.seed do |s|
  s.id = 2
  s.name = 'PRIT_RESP'
  s.description = 'Responsibility'
end
PerformanceReviewItemType.seed do |s|
  s.id = 3
  s.name = 'PRIT_ATT'
  s.description = 'Attitude'
end
PerformanceReviewItemType.seed do |s|
  s.id = 4
  s.name = 'PRIT_COMM'
  s.description = 'Communication skills'
end
PerformanceReviewItemType.seed do |s|
  s.id = 5
  s.name = 'PRIT_JOBSAT'
  s.description = 'Job Satisfaction'
end