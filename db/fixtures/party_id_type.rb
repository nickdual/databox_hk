PartyIdType.seed do |s|
  s.id = 1
  s.name = 'PTYID_PASSPORT'
  s.description = 'Passport Number'
end
PartyIdType.seed do |s|
  s.id = 2
  s.name = 'PTYID_DRLIC'
  s.description = 'Driver License'
end
PartyIdType.seed do |s|
  s.id = 3
  s.name = 'PTYID_SSN'
  s.description = 'Social Security Number'
end
PartyIdType.seed do |s|
  s.id = 4
  s.name = 'PTYID_MCID'
  s.description = 'Matricula Consular ID'
end