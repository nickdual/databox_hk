InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_IN_PROCESS'
   to_status_id = 'INVOICE_READY'
   transition_name = 'Mark Ready'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_IN_PROCESS'
   to_status_id = 'INVOICE_CANCELLED'
   transition_name = 'Cancel'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_IN_PROCESS'
   to_status_id = 'INVOICE_SENT'
   transition_name = 'Send'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_IN_PROCESS'
   to_status_id = 'INVOICE_RECEIVED'
   transition_name = 'Receive'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_SENT'
   to_status_id = 'INVOICE_APPROVED'
   transition_name = 'Mark Approved'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_SENT'
   to_status_id = 'INVOICE_READY'
   transition_name = 'Mark Ready'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_SENT'
   to_status_id = 'INVOICE_RECEIVED'
   transition_name = 'Receive'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_RECEIVED'
   to_status_id = 'INVOICE_READY'
   transition_name = 'Mark Receive'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_SENT'
   to_status_id = 'INVOICE_CANCELLED'
   transition_name = 'Cancel'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_RECEIVED'
   to_status_id = 'INVOICE_CANCELLED'
   transition_name = 'Cancel'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_SENT'
   to_status_id = 'INVOICE_IN_PROCESS'
   transition_name = 'Enable sales invoice update'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_RECEIVED'
   to_status_id = 'INVOICE_IN_PROCESS'
   transition_name = 'Enable purch.invoice update'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_IN_PROCESS'
   to_status_id = 'INVOICE_APPROVED'
   transition_name = 'Approve'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_APPROVED'
   to_status_id = 'INVOICE_SENT'
   transition_name = 'Send'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_IN_PROCESS'
   to_status_id = 'INVOICE_RECEIVED'
   transition_name = 'Receive'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_RECEIVED'
   to_status_id = 'INVOICE_APPROVED'
   transition_name = 'Approve'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_APPROVED'
   to_status_id = 'INVOICE_READY'
   transition_name = 'Mark Ready'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_READY'
   to_status_id = 'INVOICE_PAID'
   transition_name = 'Pay'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_READY'
   to_status_id = 'INVOICE_WRITEOFF'
   transition_name = 'Write Off'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_PAID'
   to_status_id = 'INVOICE_READY'
   transition_name = 'Unpay'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_READY'
   to_status_id = 'INVOICE_CANCELLE'
   transition_name = 'Cancel'
end
InvoiceStatusValid.seed do |s|
   status_id = 'INVOICE_PAID'
   to_status_id = 'INVOICE_CANCELLE'
   transition_name = 'Cancel'
end