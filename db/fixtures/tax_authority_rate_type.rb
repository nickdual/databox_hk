TaxAuthorityRateType.seed do |s|
  s.id = 1
  s.name = 'SALES_TAX'
  s.description = 'Sales Tax'
end
TaxAuthorityRateType.seed do |s|
  s.id = 2
  s.name = 'USE_TAX'
  s.description = 'Use Tax'
end
TaxAuthorityRateType.seed do |s|
  s.id = 3
  s.name = 'VAT_TAX'
  s.description = 'Value Added Tax'
end
TaxAuthorityRateType.seed do |s|
  s.id = 4
  s.name = 'INCOME_TAX'
  s.description = 'Income Tax'
end
TaxAuthorityRateType.seed do |s|
  s.id = 5
  s.name = 'EXPORT_TAX'
  s.description = 'Export Tax'
end
TaxAuthorityRateType.seed do |s|
  s.id = 6
  s.name = 'IMPORT_TAX'
  s.description = 'Import Tax'
end