EmplPositionStatus.seed do |s|
  s.id = 1
  s.description = 'Planned For'
  s.status_id = 'EMPL_POS_PLANNEDFOR'
end
EmplPositionStatus.seed do |s|
  s.id = 2
  s.status_id = 'EMPL_POS_ACTIVE'
  s.description = 'Active/Open'
end
EmplPositionStatus.seed do |s|
  s.id = 3
  s.status_id = 'EMPL_POS_INACTIVE'
  s.description = 'Inactive/Closed'
end
