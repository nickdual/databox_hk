CogsMethod.seed do |s|
  s.id = 1
  s.name = 'COGS_LIFO'
  s.description = 'LIFO'
  s.sequence_num =  1
end
CogsMethod.seed do |s|
  s.id = 2
  s.name = 'COGS_FIFO'
  s.description = 'FIFO'
  s.sequence_num =  2
end
CogsMethod.seed do |s|
  s.id = 3
  s.name = 'COGS_AVG_COST'
  s.description = 'Average Cost'
  s.sequence_num =  3
end
CogsMethod.seed do |s|
  s.id = 4
  s.name = 'COGS_INV_COST'
  s.description = 'Inventory Item Cost'
  s.sequence_num =  4
end
