MarketingCampaignStatus.seed do |s|
  s.status_id = 'MKTG_CAMP_PLANNED'
  s.description = 'Planned'
  s.sequence_num = 1
end
MarketingCampaignStatus.seed do |s|
  s.status_id = 'MKTG_CAMP_APPROVED'
  s.description = 'Approved'
  s.sequence_num = 2
end
MarketingCampaignStatus.seed do |s|
  s.status_id = 'MKTG_CAMP_INPROGRESS'
  s.description = 'In Progress'
  s.sequence_num = 3
end
MarketingCampaignStatus.seed do |s|
  s.status_id = 'MKTG_CAMP_COMPLETED'
  s.description = 'Completed'
  s.sequence_num = 4
end
MarketingCampaignStatus.seed do |s|
  s.status_id = 'MKTG_CAMP_CANCELLED'
  s.description = 'Cancelled'
  s.sequence_num = 5
end