ProductConfigItemContentType.seed do |s|
  s.id = 1
  s.name = 'IMAGE_URL'
  s.description = 'Image'
end
ProductConfigItemContentType.seed do |s|
  s.id = 2
  s.name = 'DESCRIPTION'
  s.description = 'Description'
end
ProductConfigItemContentType.seed do |s|
  s.id = 3
  s.name = 'LONG_DESCRIPTION'
  s.description = 'Description - Long'
end
ProductConfigItemContentType.seed do |s|
  s.id = 4
  s.name = 'INSTRUCTIONS'
  s.description = 'Instructions'
end
