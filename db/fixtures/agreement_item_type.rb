AgreementItemType.seed do |s|
  s.id = 1
  s.name = 'SUBAGREEMENT'
  s.description = 'Sub-Agreement'
end
AgreementItemType.seed do |s|
  s.id = 2
  s.name = 'AGREEMENT_EXHIBIT'
  s.description = 'Exhibit'
end
AgreementItemType.seed do |s|
  s.id = 3
  s.name = 'AGREEMENT_PRICING_PR'
  s.description = 'Pricing'
end
AgreementItemType.seed do |s|
  s.id = 4
  s.name = 'AGREEMENT_SECTION'
  s.description = 'Section'
end
AgreementItemType.seed do |s|
  s.id = 5
  s.name = 'AGREEMENT_COMMISSION'
  s.description = 'Commission Rate'
end