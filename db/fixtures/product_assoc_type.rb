ProductAssocType.seed do |s|
  s.id = 1
  s.name = 'ALSO_BOUGHT'
  s.description = 'Also Bought'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 2
  s.name = 'PRODUCT_UPGRADE'
  s.description = 'Upgrade or Up-Sell'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 3
  s.name = 'PRODUCT_COMPLEMENT'
  s.description = 'Complementary or Cross-Sell'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 4
  s.name = 'PRODUCT_INCOMPATABLE'
  s.description = 'Incompatible'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 5
  s.name = 'PRODUCT_OBSOLESCENCE'
  s.description = 'New Version, Replacement'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 6
  s.name = 'PRODUCT_COMPONENT'
  s.description = 'Actual Product Component'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 7
  s.name = 'PRODUCT_SUBSTITUTE'
  s.description = 'Equivalent or Substitute'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 8
  s.name = 'PRODUCT_VARIANT'
  s.description = 'Product Variant'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 9
  s.name = 'UNIQUE_ITEM'
  s.description = 'Unique Item'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 10
  s.name = 'PRODUCT_ACCESSORY'
  s.description = 'Accessory'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 11
  s.name = 'PRODUCT_REFURB'
  s.description = 'Refurbished Equivalent'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 12
  s.name = 'PRODUCT_REPAIR_SRV'
  s.description = 'Repair Service'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 13
  s.name = 'PRODUCT_AUTORO'
  s.description = 'Auto Reorder (needs recurrenceInfoId)'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 14
  s.name = 'PRODUCT_REVISION'
  s.description = 'Revision'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 15
  s.name = 'MANUF_COMPONENT'
  s.description = 'Revision'
  s.parent_id = 6
end
ProductAssocType.seed do |s|
  s.id = 16
  s.name = 'ENGINEER_COMPONENT'
  s.description = 'Engineering Bill of Materials'
  s.parent_id = 6
end
ProductAssocType.seed do |s|
  s.id = 17
  s.name = 'PRODUCT_MANUFACTURED'
  s.description = 'Product Manufactured As'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 18
  s.name = 'PRODUCT_CONF'
  s.description = 'Configurable product instance'
  s.parent_id = nil
end
ProductAssocType.seed do |s|
  s.id = 19
  s.name = 'ALTERNATIVE_PACKAGE'
  s.description = 'Alternative Packaging'
  s.parent_id = nil
end