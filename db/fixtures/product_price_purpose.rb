ProductPricePurpose.seed do |s|
  s.id = 1
  s.name = 'PPP_PURCHASE'
  s.description = 'Purchase/Initial'
end
ProductPricePurpose.seed do |s|
  s.id = 2
  s.name = 'PPP_RECURRING_CHARGE'
  s.description = 'Recurring Charge'
end
ProductPricePurpose.seed do |s|
  s.id = 3
  s.name = 'PPP_USAGE_CHARGE'
  s.description = 'Usage Charge'
end
ProductPricePurpose.seed do |s|
  s.id = 4
  s.name = 'PPP_COMPONENT_PRICE'
  s.description = 'Component Price'
end
ProductPricePurpose.seed do |s|
  s.id = 5
  s.name = 'PPP_SALE'
  s.description = 'Sale'
end