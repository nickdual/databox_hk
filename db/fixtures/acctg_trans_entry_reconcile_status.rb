AcctgTransEntryReconcileStatus.seed do |s|
  s.id = 1
  s.description = 'Not Reconciled'
  s.status_id = 'AES_NOT_RECONCILED'
  s.sequence_num = 1
end
AcctgTransEntryReconcileStatus.seed do |s|
  s.id = 2
  s.description = 'Partly Reconciled'
  s.status_id = 'AES_PARTLY_RECON'
  s.sequence_num = 2
end
AcctgTransEntryReconcileStatus.seed do |s|
  s.id = 3
  s.description = 'Reconciled'
  s.status_id = 'AES_RECONCILED'
  s.sequence_num = 3
end