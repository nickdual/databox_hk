OrderItemStatus.seed do |s|
  s.id = 1
  s.status_id = 'ITEM_CREATED'
  s.description = 'Created'
  s.sequence_num = 1
end
OrderItemStatus.seed do |s|
  s.id = 2
  s.status_id = 'ITEM_APPROVED'
  s.description = 'Approved'
  s.sequence_num = 5
end
OrderItemStatus.seed do |s|
  s.id = 3
  s.status_id = 'ITEM_COMPLETED'
  s.description = 'Completed'
  s.sequence_num = 10
end
OrderItemStatus.seed do |s|
  s.id = 4
  s.status_id = 'ITEM_REJECTED'
  s.description = 'Rejected'
  s.sequence_num = 98
end
OrderItemStatus.seed do |s|
  s.id = 5
  s.status_id = 'ITEM_CANCELLED'
  s.description = 'Cancelled'
  s.sequence_num = 99
end


