ProductType.seed do |s|
  s.id = 1
  s.name = 'ISBN'
  s.description = 'ISBN'
end
ProductType.seed do |s|
  s.id = 2
  s.name = 'MANUFACTURER_ID_NO'
  s.description = 'Manufacturer Model Number'
end
ProductType.seed do |s|
  s.id = 3
  s.name = 'OTHER_ID'
  s.description = 'Other'
end
ProductType.seed do |s|
  s.id = 4
  s.name = 'SKU'
  s.description = 'SKU'
end
ProductType.seed do |s|
  s.id = 5
  s.name = 'UPCA'
  s.description = 'UPCA'
end
ProductType.seed do |s|
  s.id = 6
  s.name = 'UPCE'
  s.description = 'UPCE'
end
ProductType.seed do |s|
  s.id = 7
  s.name = 'EAN'
  s.description = 'EAN'
end
ProductType.seed do |s|
  s.id = 8
  s.name = 'LOC'
  s.description = 'Library of Congress'
end