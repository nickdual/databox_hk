TrackingCodeType.seed do |s|
   s.id = 1
   s.status_id = 'ACCESS'
   s.description = 'Access Code'
end
TrackingCodeType.seed do |s|
   s.id = 2
   s.status_id = 'INTERNAL'
   s.description = 'Internal'
end
TrackingCodeType.seed do |s|
   s.id = 3
   s.status_id = 'EXTERNAL'
   s.description = 'External'
end
TrackingCodeType.seed do |s|
   s.id = 4
   s.status_id = 'PARTNER_MGD'
   s.description = 'Partner Managed'
end