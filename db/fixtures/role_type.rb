RoleType.seed do |s|
  s.id = 1
  s.name = 'ACCOUNT_LEAD'
  s.description = 'Account Lead'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 2
  s.name = 'ADMIN'
  s.description = 'Administrator'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 3
  s.name = 'AGENT'
  s.description = 'Agent'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 4
  s.name = 'AUTOMATED_AGENT_ROLE'
  s.description = 'Automated Agent'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 5
  s.name = 'CALENDAR_ROLE'
  s.description = 'Calendar'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 6
  s.name = 'CLIENT'
  s.description = 'Client'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 7
  s.name = 'COMM_PARTICIPANT'
  s.description = 'Communication Participant'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 8
  s.name = 'CONSUMER'
  s.description = 'Consumer'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 9
  s.name = 'CONTRACTOR'
  s.description = 'Contractor'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 10
  s.name = 'CUSTOMER'
  s.description = 'Customer'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 11
  s.name = 'DISTRIBUTION_CHANNEL'
  s.description = 'Distribution Channel'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 12
  s.name = 'ISP'
  s.description = 'ISP'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 13
  s.name = 'HOSTING_SERVER'
  s.description = 'Hosting Server'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 14
  s.name = 'MANUFACTURER'
  s.description = 'Manufacturer'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 15
  s.name = '_NA_'
  s.description = 'Not Applicable'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 16
  s.name = 'ORGANIZATION_ROLE'
  s.description = 'Organization'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 17
  s.name = 'OWNER'
  s.description = 'Owner'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 18
  s.name = 'PROSPECT'
  s.description = 'Prospect'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 19
  s.name = 'PERSON_ROLE'
  s.description = 'Person'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 20
  s.name = 'REFERRER'
  s.description = 'Referrer'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 21
  s.name = 'REQ_MANAGER'
  s.description = 'Request Manager'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 22
  s.name = 'REQ_REQUESTER'
  s.description = 'Requesting Party'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 23
  s.name = 'REQ_TAKER'
  s.description = 'Request Taker'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 24
  s.name = 'SFA_ROLE'
  s.description = 'Sales Force Automation'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 25
  s.name = 'SHAREHOLDER'
  s.description = 'Shareholder'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 26
  s.name = 'SUBSCRIBER'
  s.description = 'Subscriber'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 27
  s.name = 'VENDOR'
  s.description = 'Vendor'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 28
  s.name = 'VISITOR'
  s.description = 'Visitor'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 29
  s.name = 'WEB_MASTER'
  s.description = 'Web Master'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 30
  s.name = 'WORKFLOW_ROLE'
  s.description = 'Workflow'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 31
  s.name = 'ACCOUNTANT'
  s.description = 'Accountant'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 32
  s.name = 'ACCOUNT'
  s.description = 'Account'
  s.parent_type_id = 24
end
RoleType.seed do |s|
  s.id = 33
  s.name = 'ASSOCIATION'
  s.description = 'Association'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 34
  s.name = 'BILL_FROM_VENDOR'
  s.description = 'Bill-From Vendor'
  s.parent_type_id = 27
end
RoleType.seed do |s|
  s.id = 35
  s.name = 'BILL_TO_CUSTOMER'
  s.description = 'Bill-To Customer'
  s.parent_type_id = 10
end
RoleType.seed do |s|
  s.id = 36
  s.name = 'BULK_CUSTOMER'
  s.description = 'Bulk Customer'
  s.parent_type_id = 10
end
RoleType.seed do |s|
  s.id = 37
  s.name = 'CAL_ATTENDEE'
  s.description = 'Calendar Attendee'
  s.parent_type_id = 5
end
RoleType.seed do |s|
  s.id = 38
  s.name = 'CAL_DELEGATE'
  s.description = 'Calendar Delegate'
  s.parent_type_id = 5
end
RoleType.seed do |s|
  s.id = 39
  s.name = 'CAL_HOST'
  s.description = 'Calendar Host'
  s.parent_type_id = 5
end
RoleType.seed do |s|
  s.id = 40
  s.name = 'CAL_ORGANIZER'
  s.description = 'Calendar Organizer'
  s.parent_type_id = 5
end
RoleType.seed do |s|
  s.id = 41
  s.name = 'CAL_OWNER'
  s.description = 'Calendar Owner'
  s.parent_type_id = 5
end
RoleType.seed do |s|
  s.id = 42
  s.name = 'CARRIER'
  s.description = 'Carrier'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 43
  s.name = 'COMPETITOR'
  s.description = 'Competitor'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 44
  s.name = 'CONTACT'
  s.description = 'Contact'
  s.parent_type_id = 24
end
RoleType.seed do |s|
  s.id = 45
  s.name = 'DISTRIBUTOR'
  s.description = 'Distributor'
  s.parent_type_id = 11
end
RoleType.seed do |s|
  s.id = 46
  s.name = 'EMAIL_ADMIN'
  s.description = 'Email Administrator'
  s.parent_type_id = 19
end
RoleType.seed do |s|
  s.id = 47
  s.name = 'EMPLOYEE'
  s.description = 'Employee'
  s.parent_type_id = 19
end
RoleType.seed do |s|
  s.id = 48
  s.name = 'END_USER_CUSTOMER'
  s.description = 'End-User Customer'
  s.parent_type_id = 10
end
RoleType.seed do |s|
  s.id = 49
  s.name = 'HOUSEHOLD'
  s.description = 'Household'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 50
  s.name = 'INTERNAL_ORGANIZATIO'
  s.description = 'Internal Organization'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 51
  s.name = 'CORPORATION'
  s.description = 'Corporation'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 52
  s.name = 'GOVERNMENT_AGENCY'
  s.description = 'Government Agency'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 53
  s.name = 'FAMILY'
  s.description = 'Family'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 54
  s.name = 'TEAM'
  s.description = 'Team'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 55
  s.name = 'LEAD'
  s.description = 'Lead'
  s.parent_type_id = 24
end
RoleType.seed do |s|
  s.id = 56
  s.name = 'LTD_ADMIN'
  s.description = 'Limited Administrator'
  s.parent_type_id = 2
end
RoleType.seed do |s|
  s.id = 57
  s.name = 'ORGANIZATION_UNIT'
  s.description = 'Organization Unit'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 58
  s.name = 'PARTNER'
  s.description = 'Partner'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 59
  s.name = 'PLACING_CUSTOMER'
  s.description = 'Placing Customer'
  s.parent_type_id = 10
end
RoleType.seed do |s|
  s.id = 60
  s.name = 'REGULATORY_AGENCY'
  s.description = 'Regulatory Agency'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 61
  s.name = 'SALES_REP'
  s.description = 'Sales Representative'
  s.parent_type_id = 19
end
RoleType.seed do |s|
  s.id = 62
  s.name = 'SHIP_FROM_VENDOR'
  s.description = 'Ship-From Vendor'
  s.parent_type_id = 27
end
RoleType.seed do |s|
  s.id = 63
  s.name = 'SHIP_TO_CUSTOMER'
  s.description = 'Ship-To Customer'
  s.parent_type_id = 10
end
RoleType.seed do |s|
  s.id = 64
  s.name = 'SPONSOR'
  s.description = 'Ship-To Customer'
  s.parent_type_id = 19
end
RoleType.seed do |s|
  s.id = 65
  s.name = 'SPOUSE'
  s.description = 'Spouse'
  s.parent_type_id = 19
end
RoleType.seed do |s|
  s.id = 66
  s.name = 'SUPPLIER_AGENT'
  s.description = 'Supplier Agent'
  s.parent_type_id = 19
end
RoleType.seed do |s|
  s.id = 67
  s.name = 'SUPPLIER'
  s.description = 'Supplier'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 68
  s.name = 'TAX_AUTHORITY'
  s.description = 'Tax Authority'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 69
  s.name = 'UNION'
  s.description = 'Union'
  s.parent_type_id = 16
end
RoleType.seed do |s|
  s.id = 70
  s.name = 'WF_OWNER'
  s.description = 'Workflow Owner'
  s.parent_type_id = 30
end
RoleType.seed do |s|
  s.id = 71
  s.name = 'AFFILIATE'
  s.description = 'Affiliate'
  s.parent_type_id = 61
end
RoleType.seed do |s|
  s.id = 72
  s.name = 'BUYER'
  s.description = 'Buyer'
  s.parent_type_id = 47
end
RoleType.seed do |s|
  s.id = 73
  s.name = 'CASHIER'
  s.description = 'Cashier'
  s.parent_type_id = 47
end
RoleType.seed do |s|
  s.id = 74
  s.name = 'DEPARTMENT'
  s.description = 'Department'
  s.parent_type_id = 57
end
RoleType.seed do |s|
  s.id = 75
  s.name = 'DIVISION'
  s.description = 'Division'
  s.parent_type_id = 57
end
RoleType.seed do |s|
  s.id = 76
  s.name = 'FAMILY_MEMBER'
  s.description = 'Family Member'
  s.parent_type_id = 19
end
RoleType.seed do |s|
  s.id = 77
  s.name = 'MANAGER'
  s.description = 'Manager'
  s.parent_type_id = 47
end
RoleType.seed do |s|
  s.id = 78
  s.name = 'MANAGER'
  s.description = 'Manager'
  s.parent_type_id = 47
end
RoleType.seed do |s|
  s.id = 79
  s.name = 'ORDER_CLERK'
  s.description = 'Order Clerk'
  s.parent_type_id = 47
end
RoleType.seed do |s|
  s.id = 80
  s.name = 'OTHER_INTERNAL_ORGAN'
  s.description = 'Other Internal'
  s.parent_type_id = 50
end
RoleType.seed do |s|
  s.id = 81
  s.name = 'OTHER_ORGANIZATION_U'
  s.description = 'Other Organization Unit'
  s.parent_type_id = 57
end
RoleType.seed do |s|
  s.id = 82
  s.name = 'PARENT_ORGANIZATION'
  s.description = 'Parent Organization'
  s.parent_type_id = 57
end
RoleType.seed do |s|
  s.id = 83
  s.name = 'PACKER'
  s.description = 'Packer'
  s.parent_type_id = 47
end
RoleType.seed do |s|
  s.id = 84
  s.name = 'PICKER'
  s.description = 'Picker'
  s.parent_type_id = 47
end
RoleType.seed do |s|
  s.id = 85
  s.name = 'RECEIVER'
  s.description = 'Receiver'
  s.parent_type_id = 47
end
RoleType.seed do |s|
  s.id = 86
  s.name = 'SHIPMENT_CLERK'
  s.description = 'Shipment Clerk'
  s.parent_type_id = 47
end
RoleType.seed do |s|
  s.id = 87
  s.name = 'STOCKER'
  s.description = 'Stocker'
  s.parent_type_id = 47
end
RoleType.seed do |s|
  s.id = 88
  s.name = 'SUBSIDIARY'
  s.description = 'Subsidiary'
  s.parent_type_id = 57
end
RoleType.seed do |s|
  s.id = 89
  s.name = 'WORKER'
  s.description = 'Worker'
  s.parent_type_id = 47
end
RoleType.seed do |s|
  s.id = 90
  s.name = 'FAM_ASSIGNEE'
  s.description = 'Fixed Asset Maint Assignee'
  s.parent_type_id = nil
end
RoleType.seed do |s|
  s.id = 91
  s.name = 'FAM_SUPPLIER'
  s.description = 'Maintenance Supplier or Service'
  s.parent_type_id = 90
end
RoleType.seed do |s|
  s.id = 92
  s.name = 'FAM_MANAGER'
  s.description = 'Maintenance Manager or Supervisor'
  s.parent_type_id = 90
end
RoleType.seed do |s|
  s.id = 93
  s.name = 'FAM_WORKER'
  s.description = 'Maintenance Worker'
  s.parent_type_id = 90
end