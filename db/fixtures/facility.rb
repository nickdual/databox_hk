Facility.seed do |s|
  
    s.facility_type_enum_id = 1
  
    s.parent_facility_id = 1
  
    s.owner_party_id = 1
  
    s.facility_name = 'text'
  
    s.facility_size = 1.0
  
    s.facility_size_uom_id = 1
  
    s.opened_date = Time.now
  
    s.closed_date = Time.now
  
    s.description = 'text'
  
    s.geo_point_id = 1
  
    s.default_days_to_ship = 1
  
end

