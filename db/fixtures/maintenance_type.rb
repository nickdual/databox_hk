MaintenanceType.seed do |s|
  s.id = 1
  s.description = 'Vehicle Maintenance'
  s.name = 'VEHICLE_MAINT'
  s.parent_id = nil
end
MaintenanceType.seed do |s|
  s.id = 2
  s.description = 'Oil Change'
  s.name = 'OIL_CHANGE'
  s.parent_id = 1
end
MaintenanceType.seed do |s|
  s.id = 3
  s.description = 'Serpentine Belt Replacement'
  s.name = 'SERP_BELT'
  s.parent_id = 1
end
MaintenanceType.seed do |s|
  s.id = 4
  s.description = 'Re-Fuel'
  s.name = 'REFUEL'
  s.parent_id = 1
end
MaintenanceType.seed do |s|
  s.id = 5
  s.description = 'Replace Battery'
  s.name = 'REPLACE_BATTERY'
  s.parent_id = 1
end
MaintenanceType.seed do |s|
  s.id = 6
  s.description = 'Tune Up'
  s.name = 'TUNE_UP'
  s.parent_id = 1
end
MaintenanceType.seed do |s|
  s.id = 7
  s.description = 'Check Battery'
  s.name = 'CHECK_BATTERY'
  s.parent_id = 1
end
MaintenanceType.seed do |s|
  s.id = 8
  s.description = 'Chassis Lubrication'
  s.name = 'CHASSIS_LUBE'
  s.parent_id = 1
end
MaintenanceType.seed do |s|
  s.id = 9
  s.description = 'Rotate Tires'
  s.name = 'ROTATE_TIRES'
  s.parent_id = 1
end
MaintenanceType.seed do |s|
  s.id = 10
  s.description = 'HVAC Maintenance'
  s.name = 'HVAC_MAINT'
  s.parent_id = nil
end
MaintenanceType.seed do |s|
  s.id = 11
  s.description = 'Replace Air Filter'
  s.name = 'HVAC_REPLACE_FILTER'
  s.parent_id = 10
end
MaintenanceType.seed do |s|
  s.id = 12
  s.description = 'Check/Recharge Refrigerant'
  s.name = 'HVAC_CHECK_REFR'
  s.parent_id = 10
end
MaintenanceType.seed do |s|
  s.id = 13
  s.description = 'Wash'
  s.name = 'WASH'
  s.parent_id = nil
end