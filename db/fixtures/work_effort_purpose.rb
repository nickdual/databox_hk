WorkEffortPurpose.seed do |s|
  s.id = 1
  s.description = 'Project'
  s.name = 'WEP_PROJECT'
end
WorkEffortPurpose.seed do |s|
  s.id = 2
  s.description = 'Phase'
  s.name = 'WEP_PHASE'
end
WorkEffortPurpose.seed do |s|
  s.id = 3
  s.description = 'Task'
  s.name = 'WEP_TASK'
end
WorkEffortPurpose.seed do |s|
  s.id = 4
  s.description = 'Manufacturing'
  s.name = 'ROU_MANUFACTURING'
end
WorkEffortPurpose.seed do |s|
  s.id = 5
  s.description = 'Assembling'
  s.name = 'ROU_ASSEMBLING'
end
WorkEffortPurpose.seed do |s|
  s.id = 6
  s.description = 'Routing'
  s.name = 'WEP_ROUTING'
end
WorkEffortPurpose.seed do |s|
  s.id = 7
  s.description = 'Routing Task'
  s.name = 'WEP_ROUTING_TASK'
end
WorkEffortPurpose.seed do |s|
  s.id = 8
  s.description = 'Production Run'
  s.name = 'WEP_PRODUCTION_RUN'
end
WorkEffortPurpose.seed do |s|
  s.id = 9
  s.description = 'Fixed Asset Usage (rental)'
  s.name = 'WEP_ASSET_USAGE'
end
WorkEffortPurpose.seed do |s|
  s.id = 10
  s.description = 'Business Travel'
  s.name = 'WEP_BUSINESS_TRAVEL'
end
WorkEffortPurpose.seed do |s|
  s.id = 11
  s.description = 'Meeting'
  s.name = 'WEP_MEETING'
end
WorkEffortPurpose.seed do |s|
  s.id = 12
  s.description = 'Training'
  s.name = 'WEP_TRAINING'
end
WorkEffortPurpose.seed do |s|
  s.id = 13
  s.description = 'Maintenance'
  s.name = 'WEP_MAINTENANCE'
end
WorkEffortPurpose.seed do |s|
  s.id = 14
  s.description = 'Research'
  s.name = 'WEP_RESEARCH'
end
WorkEffortPurpose.seed do |s|
  s.id = 15
  s.description = 'Development'
  s.name = 'WEP_DEVELOPMENT'
end
WorkEffortPurpose.seed do |s|
  s.id = 16
  s.description = 'Support'
  s.name = 'WEP_SUPPORT'
end
WorkEffortPurpose.seed do |s|
  s.id = 17
  s.description = 'Deployment'
  s.name = 'WEP_DEPLOYMENT'
end
WorkEffortPurpose.seed do |s|
  s.id = 18
  s.description = 'Sub-contracting'
  s.name = 'ROU_SUBCONTRACTING'
end
WorkEffortPurpose.seed do |s|
  s.id = 19
  s.description = 'Phone Call'
  s.name = 'WEP_PHONE_CALL'
end
WorkEffortPurpose.seed do |s|
  s.id = 20
  s.description = 'Email'
  s.name = 'WEP_EMAIL'
end