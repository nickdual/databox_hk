ReturnHeaderStatus.seed do |s|
  s.id = 1
  s.status_id = 'RETURN_CREATED'
  s.description = 'Created'
  s.sequence_num = 0
end
ReturnHeaderStatus.seed do |s|
  s.id = 2
  s.status_id = 'RETURN_REQUESTED'
  s.description = 'Requested'
  s.sequence_num = 1
end
ReturnHeaderStatus.seed do |s|
  s.id = 3
  s.status_id = 'RETURN_ACCEPTED'
  s.description = 'Accepted'
  s.sequence_num = 2
end
ReturnHeaderStatus.seed do |s|
  s.id = 4
  s.status_id = 'RETURN_SHIPPED'
  s.description = 'Shipped'
  s.sequence_num = 3
end
ReturnHeaderStatus.seed do |s|
  s.id = 5
  s.status_id = 'RETURN_RECEIVED'
  s.description = 'Received'
  s.sequence_num = 4
end
ReturnHeaderStatus.seed do |s|
  s.id = 6
  s.status_id = 'RETURN_COMPLETED'
  s.description = 'Completed'
  s.sequence_num = 10
end
ReturnHeaderStatus.seed do |s|
  s.id = 7
  s.status_id = 'RETURN_MAN_REFUND'
  s.description = 'Manual Refund Required'
  s.sequence_num = 11
end
ReturnHeaderStatus.seed do |s|
  s.id = 8
  s.status_id = 'RETURN_CANCELLED'
  s.description = 'Cancelled'
  s.sequence_num = 99
end