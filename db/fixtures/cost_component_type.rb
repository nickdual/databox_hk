CostComponentType.seed do |s|
  s.id = 1
  s.name = 'MAT_COST'
  s.description = 'Materials Cost'
  s.parent_id = nil
end
CostComponentType.seed do |s|
  s.id = 2
  s.name = 'ROUTE_COST'
  s.description = 'Route (fixed asset usage) Cost'
  s.parent_id = nil
end
CostComponentType.seed do |s|
  s.id = 3
  s.name = 'LABOR_COST'
  s.description = 'Labor Cost'
  s.parent_id = nil
end
CostComponentType.seed do |s|
  s.id = 4
  s.name = 'GEN_COST'
  s.description = 'General Cost'
  s.parent_id = nil
end
CostComponentType.seed do |s|
  s.id = 5
  s.name = 'IND_COST'
  s.description = 'Indirect Cost'
  s.parent_id = nil
end
CostComponentType.seed do |s|
  s.id = 6
  s.name = 'OTHER_COST'
  s.description = 'Other Cost'
  s.parent_id = nil
end
CostComponentType.seed do |s|
  s.id = 7
  s.name = 'EST_STD_MAT_COST'
  s.description = 'Estimated Standard Materials Cost'
  s.parent_id = 1
end
CostComponentType.seed do |s|
  s.id = 8
  s.name = 'EST_STD_ROUTE_COST'
  s.description = 'Estimated Standard Route (fixed asset usage) Cost'
  s.parent_id = 2
end
CostComponentType.seed do |s|
  s.id = 9
  s.name = 'EST_STD_LABOR_COST'
  s.description = 'Estimated Standard Labor Cost'
  s.parent_id = 3
end
CostComponentType.seed do |s|
  s.id = 10
  s.name = 'EST_STD_GEN_COST'
  s.description = 'Estimated Standard General Cost'
  s.parent_id = 4
end
CostComponentType.seed do |s|
  s.id = 11
  s.name = 'EST_STD_IND_COST'
  s.description = 'Estimated Standard Indirect Cost'
  s.parent_id = 5
end
CostComponentType.seed do |s|
  s.id = 12
  s.name = 'EST_STD_OTHER_COST'
  s.description = 'Estimated Standard Other Cost'
  s.parent_id = 6
end
CostComponentType.seed do |s|
  s.id = 13
  s.name = 'ACTUAL_MAT_COST'
  s.description = 'Actual Materials Cost'
  s.parent_id = 1
end
CostComponentType.seed do |s|
  s.id = 14
  s.name = 'ACTUAL_ROUTE_COST'
  s.description = 'Actual Route (fixed asset usage) Cost'
  s.parent_id = 2
end
CostComponentType.seed do |s|
  s.id = 15
  s.name = 'ACTUAL_LABOR_COST'
  s.description = 'Actual Labor Cost'
  s.parent_id = 3
end
CostComponentType.seed do |s|
  s.id = 16
  s.name = 'ACTUAL_OTHER_COST'
  s.description = 'Actual Other Cost'
  s.parent_id = 6
end
CostComponentType.seed do |s|
  s.id = 17
  s.name = 'ACTUAL_GEN_COST'
  s.description = 'Actual General Cost'
  s.parent_id = 4
end
CostComponentType.seed do |s|
  s.id = 18
  s.name = 'ACTUAL_IND_COST'
  s.description = 'Actual Indirect Cost'
  s.parent_id = 5
end
CostComponentType.seed do |s|
  s.id = 19
  s.name = 'ACTUAL_IND_COST'
  s.description = 'Actual Indirect Cost'
  s.parent_id = 5
end