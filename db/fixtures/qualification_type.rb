QualificationType.seed do |s|
  s.id = 1
  s.name = 'DEGREE'
  s.description = 'Degree'
  s.parent_id = nil
end
QualificationType.seed do |s|
  s.id = 2
  s.name = 'CERTIFICATION'
  s.description = 'Certification'
  s.parent_id = nil
end
QualificationType.seed do |s|
  s.id = 3
  s.name = 'EXPERIENCE'
  s.description = 'Work experience'
  s.parent_id = nil
end
QualificationType.seed do |s|
  s.id = 4
  s.name = 'B.Tech'
  s.description = 'Bachelor of Technology'
  s.parent_id = 1
end

QualificationType.seed do |s|
  s.id = 5
  s.name = 'MBA'
  s.description = 'Masters of business administration'
  s.parent_id = 1
end
QualificationType.seed do |s|
  s.id = 6
  s.name = 'MSC'
  s.description = 'Masters of Science'
  s.parent_id = 1
end
QualificationType.seed do |s|
  s.id = 7
  s.name = 'BSC'
  s.description = 'Bachelor of Science'
  s.parent_id = 1
end

