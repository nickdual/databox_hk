PartyQualificationVerification.seed do |s|
  s.id = 1
  s.status_id = 'PQV_NOT_VERIFIED'
  s.description = 'Not verified'
  s.sequence_num = 1
end
PartyQualificationVerification.seed do |s|
  s.id = 2
  s.status_id = 'PQV_VERIFIED'
  s.description = 'Verified'
  s.sequence_num = 2
end
