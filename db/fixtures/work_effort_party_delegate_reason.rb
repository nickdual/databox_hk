WorkEffortPartyDelegateReason.seed do |s|
  s.id = 1
  s.description = 'Need Support or Help'
  s.name = 'WEDR_NEED_HELP'
end
WorkEffortPartyDelegateReason.seed do |s|
  s.id = 2
  s.description = 'My Part Finished'
  s.name = 'WEDR_PART_FINISHED'
end
WorkEffortPartyDelegateReason.seed do |s|
  s.id = 3
  s.description = 'Completely Finished'
  s.name = 'WEDR_WHOLE_FINISHED'
end