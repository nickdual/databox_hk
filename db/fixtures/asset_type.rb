AssetType.seed do |s|
  s.id = 1
  s.name = 'EQUIPMENT'
  s.description = 'Equipment'
end
AssetType.seed do |s|
  s.id = 2
  s.name = 'PRODUCTION_EQUIPMENT'
  s.description = 'The fixed asset used in the operation-routing definition'
end
AssetType.seed do |s|
  s.id = 3
  s.name = 'GROUP_EQUIPMENT'
  s.description = 'Group of machines, used for task and routing definition'
end
AssetType.seed do |s|
  s.id = 4
  s.name = 'OTHER_FIXED_ASSET'
  s.description = 'Other Fixed Asset'
end
AssetType.seed do |s|
  s.id = 5
  s.name = 'PROPERTY'
  s.description = 'Property'
end
AssetType.seed do |s|
  s.id = 6
  s.name = 'VEHICLE'
  s.description = 'Vehicle'
end
AssetType.seed do |s|
  s.id = 7
  s.name = 'REAL_ESTATE'
  s.description = 'Real Estate'
end
AssetType.seed do |s|
  s.id = 8
  s.name = 'COMPUTER_SOFTWARE'
  s.description = 'Computer Software'
end
AssetType.seed do |s|
  s.id = 9
  s.name = 'COMPUTER_HARDWARE'
  s.description = 'Computer Hardware'
end