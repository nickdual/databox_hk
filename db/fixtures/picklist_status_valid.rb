PicklistStatusValid.seed do |s|
  s.id = 1
  s.status_id = 1
  s.to_status_id = 2
  s.transition_name = 'Assign'
end
PicklistStatusValid.seed do |s|
  s.id = 2
  s.status_id = 1
  s.to_status_id = 3
  s.transition_name = 'Print'
end
PicklistStatusValid.seed do |s|
  s.id = 3
  s.status_id = 1
  s.to_status_id = 4
  s.transition_name = 'Pick'
end
PicklistStatusValid.seed do |s|
  s.id = 4
  s.status_id = 1
  s.to_status_id = 5
  s.transition_name = 'Cancel'
end
PicklistStatusValid.seed do |s|
  s.id = 5
  s.status_id = 2
  s.to_status_id = 4
  s.transition_name = 'Pick'
end
PicklistStatusValid.seed do |s|
  s.id = 6
  s.status_id = 2
  s.to_status_id = 3
  s.transition_name = 'Print'
end
PicklistStatusValid.seed do |s|
  s.id = 7
  s.status_id = 2
  s.to_status_id = 5
  s.transition_name = 'Cancel'
end
PicklistStatusValid.seed do |s|
  s.id = 8
  s.status_id = 3
  s.to_status_id = 4
  s.transition_name = 'Pick'
end
PicklistStatusValid.seed do |s|
  s.id = 9
  s.status_id = 3
  s.to_status_id = 5
  s.transition_name = 'Cancel'
end
