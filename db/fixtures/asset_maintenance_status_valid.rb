AssetMaintenanceStatusValid.seed do |s|
  s.id = 1
  s.status_id = 1
  s.to_status_id = 2
  s.transition_name = 'Created Maintenance Scheduled'
end
AssetMaintenanceStatusValid.seed do |s|
  s.id = 2
  s.status_id = 1
  s.to_status_id = 3
  s.transition_name = 'Created Maintenance In-Process'
end
AssetMaintenanceStatusValid.seed do |s|
  s.id = 3
  s.status_id = 1
  s.to_status_id = 5
  s.transition_name = 'Created Maintenace Cancelled'
end
AssetMaintenanceStatusValid.seed do |s|
  s.id = 4
  s.status_id = 2
  s.to_status_id = 3
  s.transition_name = 'Scheduled Maintenace In-Process'
end
AssetMaintenanceStatusValid.seed do |s|
  s.id = 5
  s.status_id = 2
  s.to_status_id = 5
  s.transition_name = 'Scheduled Maintenace Cancelled'
end
AssetMaintenanceStatusValid.seed do |s|
  s.id = 6
  s.status_id = 3
  s.to_status_id = 4
  s.transition_name = 'In-Process Maintenance Completed'
end
