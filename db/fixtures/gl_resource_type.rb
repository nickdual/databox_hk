GlResourceType.seed do |s|
  s.id = 1
  s.name = 'GLRT_MONEY'
  s.description = 'Money'
end
GlResourceType.seed do |s|
  s.id = 2
  s.name = 'GLRT_RAW_MATERIALS'
  s.description = 'Raw Materials'
end
GlResourceType.seed do |s|
  s.id = 3
  s.name = 'GLRT_LABOR'
  s.description = 'Labor'
end
GlResourceType.seed do |s|
  s.id = 4
  s.name = 'GLRT_SERVICES'
  s.description = 'Services'
end
GlResourceType.seed do |s|
  s.id = 5
  s.name = 'GLRT_FINISHED_GOODS'
  s.description = 'Finished Goods'
end
GlResourceType.seed do |s|
  s.id = 6
  s.name = 'GLRT_DELIVERED_GOODS'
  s.description = 'Delivered Goods'
end