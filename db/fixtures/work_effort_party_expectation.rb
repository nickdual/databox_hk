WorkEffortPartyExpectation.seed do |s|
  s.id = 1
  s.description = 'For Your Information'
  s.name = 'WEE_FYI'
end
WorkEffortPartyExpectation.seed do |s|
  s.id = 2
  s.description = 'Involvement Required'
  s.name = 'WEE_REQUIRE'
end
WorkEffortPartyExpectation.seed do |s|
  s.id = 3
  s.description = 'Involvement Requested'
  s.name = 'WEE_REQUEST'
end
WorkEffortPartyExpectation.seed do |s|
  s.id = 4
  s.description = 'Immediate Response Requested'
  s.name = 'WEE_IMMEDIATE'
end