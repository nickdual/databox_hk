PerformanceReviewRating.seed do |s|
  s.id = 1
  s.name = 'PRR_1'
  s.description = 'Fails to Meet Expectations'
  s.sequence_num = 1
end
PerformanceReviewRating.seed do |s|
  s.id = 2
  s.name = 'PRR_2'
  s.description = 'Inconsistently Fulfills'
  s.sequence_num = 2
end
PerformanceReviewRating.seed do |s|
  s.id = 3
  s.name = 'PRR_3'
  s.description = 'Fulfilled Expectations'
  s.sequence_num = 3
end
PerformanceReviewRating.seed do |s|
  s.id = 4
  s.name = 'PRR_4'
  s.description = 'Frequently Exceeds'
  s.sequence_num = 4
end
PerformanceReviewRating.seed do |s|
  s.id = 5
  s.name = 'PRR_5'
  s.description = 'Consistently Exceeds'
  s.sequence_num = 5
end
