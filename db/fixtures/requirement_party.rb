RequirementParty.seed do |s|
  s.requirement_id = 1
  s.part_id = 1
  s.role_type_id = 1
  s.from_date = Time.zone.now
  s.thru_date = Time.zone.now.advance(:months => 1)
end