WorkEffortProductType.seed do |s|
  s.id = 1
  s.description = 'Product and Routing Association'
  s.name = 'ROU_PROD_TEMPLATE'
end
WorkEffortProductType.seed do |s|
  s.id = 2
  s.description = 'Production Run and Product to Deliver Association'
  s.name = 'PRUN_PROD_DELIV'
end
WorkEffortProductType.seed do |s|
  s.id = 3
  s.description = 'Production Run Task and Needed Product Association'
  s.name = 'PRUNT_PROD_NEEDED'
end
WorkEffortProductType.seed do |s|
  s.id = 4
  s.description = 'Production Run Task and Deliverable Product Association'
  s.name = 'PRUNT_PROD_DELIV'
end
WorkEffortProductType.seed do |s|
  s.id = 5
  s.description = 'Product to Represent General Sales of the WorkEffort'
  s.name = 'GENERAL_SALES'
end