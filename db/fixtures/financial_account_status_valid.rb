FinancialAccountStatusValid.seed do |s|
   s.status_id = 'FNACT_ACTIVE'
   s.to_status_id = 'FNACT_MANFROZEN'
   s.transition_name = 'Set Manually Frozen'
end
FinancialAccountStatusValid.seed do |s|
   s.status_id = 'FNACT_ACTIVE'
   s.to_status_id = 'FNACT_CANCELLED'
   s.transition_name = 'Cancel'
end
FinancialAccountStatusValid.seed do |s|
   s.status_id = 'FNACT_ACTIVE'
   s.to_status_id = 'FNACT_ACTIVE'
   s.transition_name = 'Re-activate'
end
FinancialAccountStatusValid.seed do |s|
   s.status_id = 'FNACT_NEGPENDREPL'
   s.to_status_id = 'FNACT_MANFROZEN'
   s.transition_name = 'Manually Freeze'
end
FinancialAccountStatusValid.seed do |s|
   s.status_id = 'FNACT_NEGPENDREPL'
   s.to_status_id = 'FNACT_CANCELLED'
   s.transition_name = 'Cancel'
end
FinancialAccountStatusValid.seed do |s|
   s.status_id = 'FNACT_NEGPENDREPL'
   s.to_status_id = 'FNACT_ACTIVE'
   s.transition_name = 'Re-activate'
end
FinancialAccountStatusValid.seed do |s|
   s.status_id = 'FNACT_MANFROZEN'
   s.to_status_id = 'AAA'
   s.transition_name = 'AAA'
end
FinancialAccountStatusValid.seed do |s|
   s.status_id = 'FNACT_MANFROZEN'
   s.to_status_id = 'FNACT_CANCELLED'
   s.transition_name = 'Cancel'
end
