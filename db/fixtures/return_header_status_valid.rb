ReturnHeaderStatusValid.seed do |s|
  s.id = 1
  s.status_id = 2
  s.to_status_id = 3
  s.transition_name = 'Accept'
end
ReturnHeaderStatusValid.seed do |s|
  s.id = 2
  s.status_id = 2
  s.to_status_id = 8
  s.transition_name = 'Cancel'
end
ReturnHeaderStatusValid.seed do |s|
  s.id = 3 
  s.status_id = 3
  s.to_status_id = 5
  s.transition_name = 'Receive'
end
ReturnHeaderStatusValid.seed do |s|
  s.id = 4
  s.status_id = 3
  s.to_status_id = 4
  s.transition_name = 'Ship'
end
ReturnHeaderStatusValid.seed do |s|
  s.id = 5
  s.status_id = 3
  s.to_status_id = 8
  s.transition_name = 'Cancel'
end
ReturnHeaderStatusValid.seed do |s|
  s.id = 6
  s.status_id = 4
  s.to_status_id = 5
  s.transition_name = 'Receive'
end
ReturnHeaderStatusValid.seed do |s|
  s.id = 7
  s.status_id = 4
  s.to_status_id = 6
  s.transition_name = 'Complete'
end
ReturnHeaderStatusValid.seed do |s|
  s.id = 8
  s.status_id = 4
  s.to_status_id = 8
  s.transition_name = 'Cancel'
end
ReturnHeaderStatusValid.seed do |s|
  s.id = 9
  s.status_id = 5
  s.to_status_id = 6
  s.transition_name = 'Complete'
end
ReturnHeaderStatusValid.seed do |s|
  s.id = 10
  s.status_id = 5
  s.to_status_id = 8
  s.transition_name = 'Cancel'
end
ReturnHeaderStatusValid.seed do |s|
  s.id = 11
  s.status_id = 5
  s.to_status_id = 7
  s.transition_name = 'Requires Manual Refund'
end