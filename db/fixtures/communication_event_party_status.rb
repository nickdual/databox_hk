CommunicationEventPartyStatus.seed do |s|
  s.id = 1
  s.description = 'Created'
  s.name = 'COM_ROLE_CREATED'
  s.sequence_num = 1
end
CommunicationEventPartyStatus.seed do |s|
  s.id = 2
  s.description = 'Read'
  s.name = 'COM_ROLE_READ'
  s.sequence_num = 2
end
CommunicationEventPartyStatus.seed do |s|
  s.id = 3
  s.description = 'Closed'
  s.name = 'COM_ROLE_COMPLETED'
  s.sequence_num = 3
end
