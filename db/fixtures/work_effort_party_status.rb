WorkEffortPartyStatus.seed do |s|
  s.id = 1
  s.description = 'Offered'
  s.name = 'PRTYASGN_OFFERED'
  s.sequence_num = 1
end
WorkEffortPartyStatus.seed do |s|
  s.id = 2
  s.description = 'Assigned'
  s.name = 'PRTYASGN_ASSIGNED'
  s.sequence_num = 2
end
WorkEffortPartyStatus.seed do |s|
  s.id = 3
  s.description = 'Declined'
  s.name = 'PRTYASGN_DECLINED'
  s.sequence_num = 3
end
WorkEffortPartyStatus.seed do |s|
  s.id = 4
  s.description = 'Unassigned'
  s.name = 'PRTYASGN_UNASSIGNED'
  s.sequence_num = 4
end