InterviewGrade.seed do |s|
  s.id = 1
  s.name = 'InterviewGrade_A'
  s.description = 'A (above 75%)'
  s.sequence_num = 1
end
InterviewGrade.seed do |s|
  s.id = 2
  s.name = 'InterviewGrade_B'
  s.description = 'B (60-75%)'
  s.sequence_num = 2
end
InterviewGrade.seed do |s|
  s.id = 3
  s.name = 'InterviewGrade_C'
  s.description = 'C (45-60%)'
  s.sequence_num = 3
end
InterviewGrade.seed do |s|
  s.id = 4
  s.name = 'InterviewGrade_D'
  s.description = 'D (below 40%)'
  s.sequence_num = 4
end
