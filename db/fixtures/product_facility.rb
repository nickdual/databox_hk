ProductFacility.seed do |s|
  
    s.product_id = 1
  
    s.facility_id = 1
  
    s.minimum_stock = 1.0
  
    s.reorder_quantity = 1.0
  
    s.days_to_ship = 1
  
end

