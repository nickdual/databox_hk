CreditCardType.seed do |s|
   s.name = 'CCT_VISA'
   s.description = 'Visa'
end
CreditCardType.seed do |s|
   s.name = 'CCT_MASTERCARD'
   s.description = 'Master Card'
end
CreditCardType.seed do |s|
   s.name = 'American Express'
   s.description = 'CCT_AMERICANEXPRESS'
end
CreditCardType.seed do |s|
   s.name = 'Diners Club'
   s.description = 'CCT_DINERSCLUB'
end
CreditCardType.seed do |s|
   s.name = 'CCT_DISCOVER'
   s.description = 'Discover'
end
CreditCardType.seed do |s|
   s.name = 'CCT_ENROUTE'
   s.description = 'EnRoute'
end
CreditCardType.seed do |s|
   s.name = 'CCT_CARTEBLANCHE'
   s.description = 'Carte Blanche'
end
CreditCardType.seed do |s|
   s.name = 'CCT_JCB'
   s.description = 'JCB'
end
CreditCardType.seed do |s|
   s.name = 'CCT_SOLO'
   s.description = 'Solo'
end
CreditCardType.seed do |s|
   s.name = 'CCT_SWITCH'
   s.description = 'Switch'
end
CreditCardType.seed do |s|
   s.name = 'CCT_VISAELECTRON'
   s.description = 'Visa Electron'
end
CreditCardType.seed do |s|
   s.name = 'CCT_UATP'
   s.description = 'Universal Air Travel Plan'
end
