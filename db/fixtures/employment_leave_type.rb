EmploymentLeaveType.seed do |s|
  s.id = 1
  s.name = 'ELT_EARNED'
  s.description = 'Earned Leave '

end
EmploymentLeaveType.seed do |s|
  s.id = 2
  s.name = 'ELT_LOSS_OF_PAY'
  s.description = 'Loss of Pay'

end
EmploymentLeaveType.seed do |s|
  s.id = 3
  s.name = 'ELT_HOLIDAY'
  s.description = 'Holiday'

end
EmploymentLeaveType.seed do |s|
  s.id = 4
  s.name = 'ELT_SPECIAL_DAY_OFF'
  s.description = 'Special Day Off'

end
