InvoiceSequence.seed do |s|
  s.id = 1
  s.name = 'INVSQ_STANDARD'
  s.description = 'Standard (faster, may have gaps, per system)'
  s.sequence_num =  1
end
InvoiceSequence.seed do |s|
  s.id = 2
  s.name = 'INVSQ_ENF_SEQ'
  s.description = 'Enforced Sequence (no gaps, per organization)'
  s.sequence_num =  2
end
InvoiceSequence.seed do |s|
  s.id = 3
  s.name = 'INVSQ_RESTARTYR'
  s.description = 'Restart on Fiscal Year (no gaps, per org, reset to 1 each year)'
  s.sequence_num =  3
end

