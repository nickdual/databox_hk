JobInterviewType.seed do |s|
  s.id = 1
  s.name = 'JOB_INTERVW'
  s.description = 'Hire - It is a process in which an employee is evaluated by an employer for prospective employment in their company'
end
JobInterviewType.seed do |s|
  s.id = 2
  s.name = 'EXIT_INTERVW'
  s.description = 'Exit - It is conducted by an employer of a departing employee'
end
JobInterviewType.seed do |s|
  s.id = 3
  s.name = 'INFORMAL_INTERVW'
  s.description = 'Informal - It is a meeting in which a job seeker asks for advice rather than employment'
end
JobInterviewType.seed do |s|
  s.id = 4
  s.name = 'CASE_INTERVW'
  s.description = 'Case - It is an interview in which the applicant is given a question/situation and asked to resolve it'
end
