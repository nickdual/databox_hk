RateTimePeriodType.seed do |s|
  s.id = 1
  s.description = 'Rate per Hour'
  s.period_length = 1
  s.length_uom_id =  'TF_hr'
  s.time_period_type_id =  'RATE_HOUR'
end
RateTimePeriodType.seed do |s|
  s.id = 2
  s.description = 'Rate per Week'
  s.period_length = 1
  s.length_uom_id =  'TF_wk'
  s.time_period_type_id =  'RATE_WEEK'
end
RateTimePeriodType.seed do |s|
  s.id = 3
  s.description = 'Rate per Month'
  s.period_length = 1
  s.length_uom_id =  'TF_mon'
  s.time_period_type_id =  'RATE_MONTH'
end
RateTimePeriodType.seed do |s|
  s.id = 4
  s.description = 'Rate per Quarter'
  s.period_length = 3
  s.length_uom_id =  'TF_mon'
  s.time_period_type_id =  'RATE_QUARTER'
end
