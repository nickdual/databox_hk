PayrollPreference.seed do |s|
  s.employment_id = 1
  s.from_date = Time.now
  s.thru_date = Time.now
  s.percentage = 1.0
  s.flat_amount = 1.0
  s.deduction_type_enum_id = 1
  s.time_period_type_id = 1
  s.payment_method_type_enum_id = 1
  s.payment_method_id = 1
end

