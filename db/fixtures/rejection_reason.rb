RejectionReason.seed do |s|
  s.id = 1
  s.name = 'SRJ_DAMAGED'
  s.description = 'Damaged'
end
RejectionReason.seed do |s|
  s.id = 2
  s.name = 'SRJ_NOT_ORDERED'
  s.description = 'Not Ordered'
end
RejectionReason.seed do |s|
  s.id = 3
  s.name = 'SRJ_OVER_SHIPPED'
  s.description = 'Over Shipped'
end
