AssetClass.seed do |s|
  s.id = 1
  s.name = 'ACLS_BOAT'
  s.description = 'Boat'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 2
  s.name = 'ACLS_FORKLIFT'
  s.description = 'Forklift'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 3
  s.name = 'ACLS_DIGGER'
  s.description = 'Digger'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 4
  s.name = 'ACLS_MANLIFT'
  s.description = 'Manlift'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 5
  s.name = 'ACLS_SERVICE_BODY'
  s.description = 'Service Body'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 6
  s.name = 'ACLS_TRACTOR'
  s.description = 'Tractor'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 7
  s.name = 'ACLS_PASSENGER'
  s.description = 'Passenger Vehicle'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 8
  s.name = 'ACLS_COMPRESSOR'
  s.description = 'Compressor'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 9
  s.name = 'ACLS_TRENCHER'
  s.description = 'Trencher'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 10
  s.name = 'ACLS_PULLER'
  s.description = 'Puller'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 11
  s.name = 'ACLS_TANKER'
  s.description = 'Tanker'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 12
  s.name = 'ACLS_REEL_STAND'
  s.description = 'Reel Stand'
  s.parent_id = nil
end

AssetClass.seed do |s|
  s.id = 13
  s.name = 'ACLS_HARDWARE'
  s.description = 'Computer Hardware'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 14
  s.name = 'ACLS_DESKTOP'
  s.description = 'Desktop'
  s.parent_id = 13
end
AssetClass.seed do |s|
  s.id = 15
  s.name = 'ACLS_LAPTOP'
  s.description = 'Laptop'
  s.parent_id = 13
end
AssetClass.seed do |s|
  s.id = 16
  s.name = 'ACLS_MONITOR'
  s.description = 'Monitor'
  s.parent_id = 13
end

AssetClass.seed do |s|
  s.id = 17
  s.name = 'ACLS_SOFTWARE'
  s.description = 'Computer Software'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 18
  s.name = 'ACLS_PRG_SOFTWARE'
  s.description = 'Computer Software'
  s.parent_id = 17
end
AssetClass.seed do |s|
  s.id = 19
  s.name = 'ACLS_PRG_SOFTWARE'
  s.description = 'Programming Software'
  s.parent_id = 17
end
AssetClass.seed do |s|
  s.id = 20
  s.name = 'ACLS_SYS_SOFTWARE'
  s.description = 'System Software'
  s.parent_id = 17
end
AssetClass.seed do |s|
  s.id = 21
  s.name = 'ACLS_APP_SOFTWARE'
  s.description = 'Application Software'
  s.parent_id = 17
end
AssetClass.seed do |s|
  s.id = 22
  s.name = 'ACLS_UTIL_SOFTWARE'
  s.description = 'Utility Software'
  s.parent_id = 17
end