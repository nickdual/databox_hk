ProductGeoPurpose.seed do |s|
  s.id = 1
  s.name = 'PG_PURCH_INCLUDE'
  s.description = 'Purchase Include Geo'
end
ProductGeoPurpose.seed do |s|
  s.id = 2
  s.name = 'PG_PURCH_EXCLUDE'
  s.description = 'Purchase Exclude Geo'
end
ProductGeoPurpose.seed do |s|
  s.id = 3
  s.name = 'PG_SHIP_INCLUDE'
  s.description = 'Shipment Include Geo'
end
ProductGeoPurpose.seed do |s|
  s.id = 4
  s.name = 'PG_SHIP_EXCLUDE'
  s.description = 'Shipment Exclude Geo'
end