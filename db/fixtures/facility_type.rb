FacilityType.seed do |s|
  s.name = 'BUILDING'
  s.description = 'Building'
end
FacilityType.seed do |s|
  s.name = 'FLOOR'
  s.description = 'Floor'
end
FacilityType.seed do |s|
  s.name = 'OFFICE'
  s.description = 'Office'
end
FacilityType.seed do |s|
  s.name = 'CALL_CENTER'
  s.description = 'Call Center'
end
FacilityType.seed do |s|
  s.name = 'PLANT'
  s.description = 'Plant'
end
FacilityType.seed do |s|
  s.name = 'ROOM'
  s.description = 'Room'
end
FacilityType.seed do |s|
  s.name = 'RETAIL_STORE'
  s.description = 'Retail Store'
end
FacilityType.seed do |s|
  s.name = 'WAREHOUSE'
  s.description = 'Warehouse'
end
