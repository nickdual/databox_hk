ShipmentType.seed do |s|
  s.id = 1
  s.description = 'Incoming'
  s.name = 'INCOMING_SHIPMENT'
  s.parent_id = nil
end
ShipmentType.seed do |s|
  s.id = 2
  s.description = 'Outgoing'
  s.name = 'OUTGOING_SHIPMENT'
  s.parent_id = nil
end
ShipmentType.seed do |s|
  s.id = 3
  s.description = 'Sales Return'
  s.name = 'SALES_RETURN'
  s.parent_id = 1
end
ShipmentType.seed do |s|
  s.id = 4
  s.description = 'Sales Shipment'
  s.name = 'SALES_SHIPMENT'
  s.parent_id = 2
end
ShipmentType.seed do |s|
  s.id = 5
  s.description = 'Purchase Shipment'
  s.name = 'PURCHASE_SHIPMENT'
  s.parent_id = 1
end
ShipmentType.seed do |s|
  s.id = 6
  s.description = 'Purchase Return'
  s.name = 'PURCHASE_RETURN'
  s.parent_id = 2
end
ShipmentType.seed do |s|
  s.id = 7
  s.description = 'Drop Shipment'
  s.name = 'DROP_SHIPMENT'
  s.parent_id = nil
end
ShipmentType.seed do |s|
  s.id = 8
  s.description = 'Transfer'
  s.name = 'TRANSFER'
  s.parent_id = nil
end