BenefitType.seed do |s|
  s.id = 1   
  s.name = 'HEALTH'
  s.description = 'Health' 
  s.employer_paid_percentage = nil
  s.parent_id = nil  
end
BenefitType.seed do |s|
  s.id = 2
  s.name = 'VACATION'
  s.description = 'Vacation' 
  s.employer_paid_percentage = nil
  s.parent_id = nil  
end
BenefitType.seed do |s|
  s.id = 3
  s.name = 'SICK_LEAVE'
  s.description = 'Sick Leave' 
  s.employer_paid_percentage = nil
  s.parent_id = nil  
end

