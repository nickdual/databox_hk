FinancialAccountTransType.seed do |s|
   s.name = 'FATT_DEPOSIT'
   s.description = 'Deposit'
end
FinancialAccountTransType.seed do |s|
   s.name = 'FATT_WITHDRAWAL'
   s.description = 'Withdraw'
end
FinancialAccountTransType.seed do |s|
   s.name = 'FATT_ADJUSTMENT'
   s.description = 'Adjustment'
end
