MarketInterest.seed do |s|
  s.product_category_id = 1
  s.market_segment_id = 1
  s.from_date = Time.zone.now
  s.thru_date = Time.zone.now.advance(:months => 1)
end