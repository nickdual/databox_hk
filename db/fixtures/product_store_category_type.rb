ProductStoreCategoryType.seed do |s|
  s.id = 1
  s.name = 'PSCT_HOME_PAGE'
  s.description = 'Home Page (One)'
end
ProductStoreCategoryType.seed do |s|
  s.id = 2
  s.name = 'PSCT_BROWSE_ROOT'
  s.description = 'Browse Root (One)'
end
ProductStoreCategoryType.seed do |s|
  s.id = 3
  s.name = 'PSCT_SEARCH'
  s.description = 'Default Search (One)'
end
ProductStoreCategoryType.seed do |s|
  s.id = 4
  s.name = 'PSCT_QUICK_ADD'
  s.description = 'Quick Add (Many)'
end
ProductStoreCategoryType.seed do |s|
  s.id = 5
  s.name = 'PSCT_PROMOTIONS'
  s.description = 'Promotions (One)'
end
ProductStoreCategoryType.seed do |s|
  s.id = 6
  s.name = 'PSCT_VIEW_ALLW'
  s.description = 'View Allow (One)'
end
ProductStoreCategoryType.seed do |s|
  s.id = 7
  s.name = 'PSCT_PURCH_ALLW'
  s.description = 'Purchase Allow (One)'
end
ProductStoreCategoryType.seed do |s|
  s.id = 8
  s.name = 'PSCT_ADMIN_ALLW'
  s.description = 'Admin Allow (One)'
end
ProductStoreCategoryType.seed do |s|
  s.id = 9
  s.name = 'PSCT_EBAY_ROOT'
  s.description = 'Ebay Root (One)'
end



