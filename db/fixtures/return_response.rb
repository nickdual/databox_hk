ReturnResponse.seed do |s|
  s.id = 1
  s.name = 'RTN_CREDIT'
  s.description = 'Store Credit'
end
ReturnResponse.seed do |s|
  s.id = 2
  s.name = 'RTN_REFUND'
  s.description = 'Refund'
end
ReturnResponse.seed do |s|
  s.id = 3
  s.name = 'RTN_REPLACE'
  s.description = 'Wait Replacement'
end
ReturnResponse.seed do |s|
  s.id = 4
  s.name = 'RTN_CSREPLACE'
  s.description = 'Cross-Ship Replacement'
end
ReturnResponse.seed do |s|
  s.id = 5
  s.name = 'RTN_REPAIR_REPLACE'
  s.description = 'Repair Replacement'
end
ReturnResponse.seed do |s|
  s.id = 6
  s.name = 'RTN_WAIT_REPLACE_RES'
  s.description = 'Wait Replacement Reserved'
end
ReturnResponse.seed do |s|
  s.id = 7
  s.name = 'RTN_REPLACE_IMMEDIAT'
  s.description = 'Replace Immediately'
end
ReturnResponse.seed do |s|
  s.id = 8
  s.name = 'RTN_REFUND_IMMEDIATE'
  s.description = 'Refund immediately'
end