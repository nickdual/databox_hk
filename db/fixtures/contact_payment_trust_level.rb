ContactPaymentTrustLevel.seed do |s|
  s.id = 1
  s.description = 'New Contact Mechanism'
  s.name = 'CMTL_NEW'
  s.sequence_num = 1
end
ContactPaymentTrustLevel.seed do |s|
  s.id = 2
  s.description = 'Valid/Clean (through 3rd party service)'
  s.name = 'CMTL_VALID'
  s.sequence_num = 2
end
ContactPaymentTrustLevel.seed do |s|
  s.id = 3
  s.description = 'Verified (with outbound contact or authorization)'
  s.name = 'CMTL_VERIFIED'
  s.sequence_num = 3
end
ContactPaymentTrustLevel.seed do |s|
  s.id = 4
  s.description = 'Greylisted'
  s.name = 'CMTL_GREYLISTED'
  s.sequence_num = 8
end
ContactPaymentTrustLevel.seed do |s|
  s.id = 5
  s.description = 'Blacklisted'
  s.name = 'CMTL_BLACKLISTED'
  s.sequence_num = 9
end