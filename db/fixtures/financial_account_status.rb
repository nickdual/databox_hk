FinancialAccountStatus.seed do |s|
   s.description = 'Active'
   s.sequence_num = 1
   s.status_id = 'FNACT_ACTIVE'
end
FinancialAccountStatus.seed do |s|
   s.description = 'Negative Pending Replenishment'
   s.sequence_num = 2
   s.status_id = 'FNACT_NEGPENDREPL'
end
FinancialAccountStatus.seed do |s|
   s.description = 'Manually Frozen'
   s.sequence_num = 3
   s.status_id = 'FNACT_MANFROZEN'
end
FinancialAccountStatus.seed do |s|
   s.description = 'Cancelled'
   s.sequence_num = 9
   s.status_id = 'FNACT_CANCELLED'
end