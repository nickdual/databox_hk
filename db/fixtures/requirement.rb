Requirement.seed do |s|
  s.id = 1
  s.status_id = 1
  s.requirement_type_enum_id = 1
  s.facility_id = 1
  s.deliverable_id = 1
  s.asset_id = 1
  s.product_id = 1
  s.description = 'Requirement description'
  s.requirement_start_date = Time.zone.now
  s.estimated_budget = 10.0
  s.quantity = 10.0
  s.use_case = 'Requriement use case'
  s.reason = 'Requriement reason'
end