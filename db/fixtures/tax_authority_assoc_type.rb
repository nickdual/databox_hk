TaxAuthorityAssocType.seed do |s|
  s.id = 1
  s.name = 'EXEMPT_INHER'
  s.description = 'Exemption Inheritance'
end
TaxAuthorityAssocType.seed do |s|
  s.id = 2
  s.name = 'COLLECT_AGENT'
  s.description = 'Collection Agent'
end