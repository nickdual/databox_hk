WorkEffortAssetAssignStatus.seed do |s|
  s.id = 1
  s.name = 'FA_ASGN_REQUESTED'
  s.description = 'Requested'
  s.sequence_num = 1
end
WorkEffortAssetAssignStatus.seed do |s|
  s.id = 2
  s.name = 'FA_ASGN_ASSIGNED'
  s.description = 'Assigned'
  s.sequence_num = 2
end
WorkEffortAssetAssignStatus.seed do |s|
  s.id = 3
  s.name = 'FA_ASGN_DENIED'
  s.description = 'Request Denied'
  s.sequence_num = 3
end

