How we work with Git
--------------------

1) The master branch should be production deployable at all times, that means that tests should run successfully at all times.

2) New features should be done on a branch e.g. if you get a ticket OWM-3275: User Can Add A Comment To a Post. First, check out a feature branch named with the story id and a short, descriptive title: 
git checkout -b 3275-add-commenting

Rebase against the upstream frequently to prevent your branch from diverging significantly:
git fetch origin master
git rebase origin/master

Tests should also run successfully on the feature branch so please run tests before marking the ticket as resolved. 


Merging
-------

Checkout the branch you want the changes merged into
- git checkout master

Merge the changes while keeping all the commit history
- git merge --no-ff 1000-feature-branch-name

Delete the feature branch on the Github
- git push origin --delete 1000-feature-branch-name 

Delete the feature branch locally
- git branch -D 1000-feature-branch-name