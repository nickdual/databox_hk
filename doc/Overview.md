DataBox Overview
================

Overview
--------
Databox is a Ruby on Rails based ERP system.
The database model is based on Moqui which is an evolution of Apache OFBiz, a Java based ERP system.

Basic info
----------
- Ruby 1.9.3
- Rails 3.2
- jQuery
- CoffeeScript
- SASS

Web server for development
--------------------------
- WEBrick

Web server for production
-------------------------
- Passenger

Unit testing
------------
- RSpec

Integration testing
-------------------
- Cucumber with Capybara

Front-end framework
-------------------
- Existing Theme called Starlight
- Responsive - mobile targeted @ 1024 x 768, desktop targeted @ 1440 x 900

Email
-----
- Mandrill

Authentication
--------------
- Devise with Confirmable and Invitable modules + OmniAuth for Google

Authorization
-------------
- CanCan with Rolify

User action history
-------------------
- PaperTrail

Image management
----------------
- Paperclip

Attachment Storage
------------------
- AWS S3

Data Model
----------
- Moqui Mantle UDM revision: https://github.com/jonesde/mantle/tree/4810f3590beef52d6e835cadc712c4d5a57bd4d3