require 'faker'
namespace :db do
  desc "Fill database with sample data"
  task :populate => :environment do
    Rake::Task['db:reset'].invoke
    99.times do |n|
      user = User.create(
                          :email => Faker::Internet.email ,
                          :password => "password",
                          :password_confirmation => "password",
                          :sign_in_count=> "0",
                          :created_at => "2012-11-21 10:24:28.586343",
                          :confirmed_at=> '2012-11-22 10:01:19.653129',
                          :approval => true)
      user.add_role('consumer')

    end
    99.times do |n|
      product = Product.create(:product_type_id => 1,
                          :product_name =>  Faker::Name.name,
                          :description => Populator.sentences(2..10),
                          :comments =>  Populator.sentences(2..10),
                          :sales_introduction_date => DateTime.now - 1.months,
                          :sales_disc_when_not_avail => true,
                          :sales_discontinuation_date => DateTime.now,
                          :default_shipment_box_type_id => 1,
                          :support_discontinuation_date =>  DateTime.now,
                          :require_inventory => true,
                          :requirement_method_id => true,
                          :charge_shipping => true,
                          :in_shipping_box => true ,
                          :returnable => true,
                          :require_amount => true ,
                          :amount_uom_type_id => rand(1..3),
                          :sku => n + 1
      )

    end

    99.times do
      cost_component = CostComponent.create(
          :cost_component_type_id => rand(1..10),
          :product_id => rand(1..99),
          :party_id => rand(1..10),
          :geo_id => rand(1..10),
          :work_effort_id => rand(1..10),
          :asset_id => rand(1..10) ,
          :cost_component_calc_id => rand(1..10),
          :from_date => DateTime.now - 1.months,
          :thru_date => DateTime.now ,
          :cost => 1.0 * rand(),
          :cost_uom_id => rand(1..10)
      )
    end

    # Party Data
    10.times do
      party = Party.create(
          :party_type_id => rand(1..4)
      )
    end

    # Party Product Fake Data
    99.times do |n|
      product_party = ProductParty.create(
          :product_id => n+1,
          :party_id => rand(1..10)
      )
    end

    # Supplier Product Fake Data
    99.times do |n|
      supplier_product =  SupplierProduct.create(
          :product_id => n+1,
          :comments => Populator.sentences(2..10),
          :standard_lead_time_days => rand(10..30)
      )
    end

    #Fake Asset Data for Products
    5.times do
      asset = Asset.create(
          :asset_type_id => rand(1..4),
          :quantity_on_hand_total => rand(243..9765)
      )
    end

    99.times do
      product_price = ProductPrice.create(
          :product_id=> rand(1..99),
          :product_store_id => rand(1..10) ,
          :vendor_party_id => rand(1..10),
          :customer_party_id =>rand(1..10),
          :price_type_id => rand(1..10),
          :price_purpose_id => rand(1..10),
          :from_date => DateTime.now - 2.months,
          :thru_date => DateTime.now,
          :min_quantity => 1.0 *rand(),
          :price => 2.0 *rand(),
          :price_uom_id => rand(1..10),
          :term_uom_id => rand(),
          :tax_in_price => true,
          :tax_amount => 1.0 * rand(),
          :tax_percentage => 2.0 * rand(),
          :tax_auth_party_id => rand(1..10),
          :tax_auth_geo_id => rand(),
          :agreement_id => rand(),
          :agreement_item_seq_id => rand()
      )
    end
    99.times do |n|
      order_header = OrderHeader.create(
          :order_id=> n + 1,
          :order_name => Populator.sentences(2..10) ,
          :entry_date =>  DateTime.now - 1.months,
          :status_id => 1,
          :currency_uom_id => rand(1..10),
          :billing_account_id => rand(1..10),
          :product_store_id => n + 1,
          :sales_channel_id => n + 1,
          :terminal_id => n + 1,
          :external_id => n + 1,
          :sync_status_id => n + 1,
          :visit_id => n + 1,
          :parent_order_id => n + 1,
          :recurrence_info_id => n + 1,
          :last_ordered_date => DateTime.now,
          :remaining_sub_total => 2.0 * rand(),
          :grand_total => 2.0 * rand(),

      )
    end
    99.times do |n|
      organization = Organization.create(
          party_id: n + 1,
          organization_name: Populator.sentences(2..10),
          office_site_name: Populator.sentences(2..10),
          annual_revenue: 2.0 * rand(),
          num_employees: 2.0 * rand(),
          comments: Populator.sentences(2..10)
      )
      end
    99.times do |n|
      order_item = OrderItem.create(
          order_id: n + 1,
          order_item_seq_id: n + 1,
          order_part_seq_id: n + 1,
          item_type_id: n + 1,
          status_id: n + 1,
          product_id: n + 1,
          product_config_saved_id: n + 1,
          item_description: Populator.sentences(2..10),
          comments: Populator.sentences(2..10),
          quantity: 2.0 * rand(),
          cancel_quantity: 2.0 * rand(),
          selected_amount: 2.0 * rand(),
          unit_price: 2.0 * rand(),
          unitList_price: 2.0 * rand(),
          is_modified_price: true,
          external_item_id: n + 1,
          from_asset_id: n + 1,
          budget_id: n + 1,
          budget_item_seq_id: n + 1,
          supplier_product_id: n + 1,
          product_category_id: n + 1,
          is_promo: true,
          shopping_list_id: n + 1,
          shopping_list_item_seq_id: n + 1,
          subscription_id: n + 1,
          override_gl_account_id: n + 1,
          sales_opportunity_id: n + 1
      )

    end

    99.times do |n|
      people = Person.create(
          party_id: n+1,
          first_name: Populator.sentences(2..3),
          last_name: Populator.sentences(4..5)
        )
    end

    99.times do |n|
      order_part = OrderPart.create(
          order_id: n+1,
          customer_party_id: n+1,
          order_part_seq_id: n+1,
          parent_part_seq_id: n+1,
          vendor_party_id: n+1,
          part_name: Populator.sentences(2..5)
        )
    end

    status = OrderHeaderStatus.create(:status_id => 1,:description=> 'Completed',:sequence_num => rand(1..10))
    status = OrderHeaderStatus.create(:status_id => 2,:description=> 'Finished',:sequence_num => rand(1..10))


  end
end