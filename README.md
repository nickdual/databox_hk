DataBox
=======

Overview
--------
Databox is a Ruby on Rails based ERP system. Please see the doc folder for more information.

Testing
-------
Build the test system

	rake db:migrate
	rake db:seed
	rake db:populate
	bundle exec rake test
	bundle exec cucumber .
	bundle exec rake spec
  
To login use the following admin account

	u: admin@owm.com
	p: adminowm
