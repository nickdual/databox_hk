When /the list of product promotions/  do
  visit product_promotions_path
end
When /the list of promotion codes/ do
  visit promotion_codes_path
end
When /the list of promotion stores/ do
  visit "/product_promotions/stores"
end
Then /the product promotion edit page/  do
  visit "/product_promotions/1/edit"
end
Then /the list of promotion rules/  do
  visit "/promotion_rules/manage/1"
end
Then /login page/ do
  visit "/users/login"
end
Then /new promotion code page/ do
  visit new_promotion_code_path
end
Given /new product promotion page/  do
  visit new_product_promotion_path
end
