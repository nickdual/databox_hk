
Given /^(?:|I )am ship_rate page$/ do
  visit '/ship_rates'
end

When /^(?:|I )have exits data$/ do
  @origin_contact_mech = PostalAddress.create( contact_mech_id:  1,city: "Beverly Hills",postal_code: "90210", access_code: "US")
  @destination_contact_mech = PostalAddress.create(contact_mech_id:  2, city: "Ottawa", postal_code: "K1P 1J1",access_code: "CA")
  @shipment = Shipment.create(origin_contact_mech_id: @origin_contact_mech.id ,destination_contact_mech_id: @destination_contact_mech.id)
  @shipment_package = ShipmentPackage.create(shipment_id: @shipment.id, box_length: 1,box_height: 1,box_width: 1,weight: 1)

  @origin_contact_mech2 = PostalAddress.create( contact_mech_id:  1,city: "NewYork",postal_code: "123456", access_code: "US")
  @destination_contact_mech2 = PostalAddress.create(contact_mech_id:  2, city: "Colla", postal_code: "56102",access_code: "KA")
  @shipment2 = Shipment.create(origin_contact_mech_id: @origin_contact_mech2.id ,destination_contact_mech_id: @destination_contact_mech2.id)
  @shipment_package2 = ShipmentPackage.create(shipment_id: @shipment2.id, box_length: 100,box_height: 100,box_width: 100,weight: 1)
end