@javascript
Feature: Account process
  Scenario: Sign in with valid account
    Given I have an user
    Then I go to login page
    And I give "user_email" with "abc@gmail.com" within "#ml-form"
    And I fill in "user_password" with "123456"
    When I press the link "#btn-login"
